# Programmer Serial D11C

D11C-based programmer/debugger with a serial FTDI socket.



The main features are:
- Minimal BOM.
- 5V-tolerant serial, 5V power to target.

## Pinout

![](img/Programmer-Serial-D11C_pinout.png)

## BOM

> Open [interactive BOM](http://pub.fabcloud.io/programmers/programmer-serial-d11c/) in your browser.

## Schematic

![](img/Programmer-Serial-D11C_schematic.png)

## Flashing bootloader

This board is meant to be flashed with the sam BA bootloader from the [Fab SAM core](https://github.com/qbolsee/ArduinoCore-fab-sam). You can download the binary for the bootloader [here](https://github.com/qbolsee/ArduinoCore-fab-sam/blob/master/bootloaders/zero/binaries/sam_ba_SAMD11C14A.bin?raw=true).

After connecting a SWD programmer to this board's programming pins, there are two methods to flash the bootloader:

- edbg:
```
edbg -ebpv -t samd11 -f sam_ba_SAMD11C14A.bin
```
- Arduino: select `SAMD11C14A` as the board in the Fab SAM core, then click on `Burn Bootloader`. The SWD programmer should be automatically detected.

## Loading USB-to-serial firmware

After flashing the bootloader, the firmware running on the chip can be uploaded from an Arduino sketch.

Our recommended implementation of USB-to-serial for the SAMD11C14 is provided [here](https://github.com/qbolsee/SAMD11C_serial). It supports baudrate, parity and stop bit changes on the fly, which are required for full UPDI support.

## Using as a Programmer

After the previous steps, you can program AVR targets that support UPDI by simply connecting the UPDI pin and uploading the binary through `pyupdi`, `pymcuprog` or directly through the Arduino IDE if using megaTinyCore or DxCore. In the latter case, the following settings must be selected in `Tools`:
- `port`: the SAMD11C14's serial port.
- `Programmer`: SerialUPDI - SLOW

Make sure that the `Board` setting matches your target chip. One common mistake is having `Board` left on `SAMD11C14A` from the Fab SAM Core, in which case the SAMD11 itself gets programmed.

## Using as a debugger

Just select the serial port of the programmer in your Arduino IDE to open the Serial Monitor. You can use any other serial monitor software for the purpose. You will be able to send and receive messages from your target board.
