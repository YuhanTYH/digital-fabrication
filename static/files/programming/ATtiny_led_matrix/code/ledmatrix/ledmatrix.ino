// anodes
int row[] = {0, 1, 2, 3};
// cathodes
int col[] = {6, 7, 8, 10};

int btn_count = 0;

void setup()
{
  Serial.begin(115200);
  pinMode(9, INPUT_PULLUP);
  for (int i = 0; i < 4; i++)
  {
    pinMode(row[i], OUTPUT);
    pinMode(col[i], OUTPUT);
  }
  allOff();
}


void loop()
{

  if (!digitalRead(9)) {
    btn_count++;
    delay(200);
  }

  if (btn_count == 1) {
    pattern1();
  }
  else if (btn_count == 2) {
    pattern2();
  }

  else if (btn_count > 2) {
    btn_count = 0;
  }

  delay(150);
  Serial.println(!digitalRead(9));
}


void allOff()
{
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(row[i], LOW);
    digitalWrite(col[i], HIGH);
  }
}


void pattern1() {
  for (int j = 0; j < 4; j++) {
    for (int i = 0; i < 4; i++)
    {
      digitalWrite(row[j], HIGH);
      digitalWrite(col[i], LOW);
    }
    delay(300);
  }
  allOff();
}

void pattern2() {
  for (int j = 0; j < 4; j++) {
    for (int i = 0; i < 4; i++) {
      digitalWrite(row[j], HIGH);
      digitalWrite(col[i], LOW);
      delay(100);
      allOff();
    }
  }
}
