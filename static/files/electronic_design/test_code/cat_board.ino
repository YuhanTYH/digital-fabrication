#define LED_1 3
#define LED_2 2

int pressCount = 0;

void setup() {
  Serial.begin(115200);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(4, INPUT_PULLUP);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

}

void loop() {
  if (!digitalRead(4)) {
    pressCount ++;
  }

  if (pressCount == 1) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, HIGH);
  }
  else if (pressCount == 2) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, LOW);
    delay(100);
    digitalWrite(LED_2, HIGH);
    digitalWrite(LED_1, LOW);
    delay(100);
  }

  else if (pressCount == 3) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, HIGH);
    delay(500);
    digitalWrite(LED_1, LOW);
    digitalWrite(LED_2, LOW);
    delay(100);
  }

  else {
    digitalWrite(LED_1, LOW);
    digitalWrite(LED_2, LOW);
    pressCount = 0;
  }
  
  Serial.println(pressCount);

  delay(150);
}
