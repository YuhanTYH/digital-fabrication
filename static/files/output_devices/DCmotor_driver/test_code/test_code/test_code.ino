#define IN1  1
#define IN2  7
#define PO  6

int rotate_speed = 0;
void setup() {
  pinMode(IN1,   OUTPUT);
  pinMode(IN2,    OUTPUT);

}

void loop() {
  rotate_speed = map(analogRead(PO), 0, 1023, 0, 255);

  analogWrite(IN1, rotate_speed);
  analogWrite(IN2, LOW);

  delay(1000);

  analogWrite(IN2, rotate_speed);
  analogWrite(IN1, LOW);

  delay(1000);

}
