---
title: "Machine Building"
foldername: "machine"
image: "machine-1.JPG"
date: 2022-04-23
author: "Yuhan Tseng"
tags: ["machine","motor","linear motion"]
categories: ["NOTE"]
draft: false
---
During these two weeks, we tried to design and make a machine with team members. We decided to make a **"Self-clean Cat Litter Box"** machine because one of our members, Anna, keeps 2 cats 😻 and she wants to build this machine for [her final project](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/final-project/). I mainly did about **electronics and user interface** parts and also luckly can participate other parts. The process was very interesting and also frustrating sometimes 😂 I learned a lot in this assignment, and this page is about what we did and some notes about the process.

This is our [**Machine Gitlab page**](https://gitlab.com/aaltofablab/machine-building-2022b)

{{< video src = "hero_clip.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

## Goal

:point_right: Design a machine that includes mechanism, actuation and automation.

:point_right: Build the mechanical parts and operate it manually.

:point_right: Actuate and automate your machine.

:point_right: Document the group project and your individual contribution.

## About this Machine

We found some products for our reference, and this one is closedt to what we wanted to do:
{{< youtube y9kRfRmG97E "16:9" "70">}}

The main function of the machine is **collecting cat's poop in the sand box automatically** so the keeper can easily to clean up the poops. More detailed functions are:

- The scoop can move forward automatically and push all the pooops to one side of the sand box.
- The scoop can move backward automatically and go back to the start position.
- The scoop can rotate (lift up) when it go backward, so it won't drag the poops back again.

We also discussed about many other functions and details of this machine, but we only have 2 weeks so we decided to focuse on the **scoop's movement** 


## Developing Process
Here are the making process. I have not find a good way to organized what we did so just wrote down anything that I experienced.

### Mechnics Part

The main mechanics part was designed and built by Karls, [**here is his documantation**](https://karlpalo.gitlab.io/fablab2022/weekly/week13/). Because we didn't have much experience in making a machine, his design gave us a concrete direction to start and work on 👍  He also made a design for adjusting the belt which was very clever! The main structures are:

- Linear movement of the scoop: a motor with smaller gear, a bigger gear, two lead screw connect with a belt
- Rotation of the scoop: a motor with smaller gear, a bigger gear, a belt

The reason that we used a smaller gear and a bigger gear was because we worried that **the pushing force and the rotation force** were too small, and actually this issue was true when we tested the machine.

To try out the idea, we tried to used the materials that already made in FabLab to make a rough prototype. This was the first mechincs test:

(the video was shot by Anna)

{{< video src = "mechanics-1.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

We found that building a stable structure for bigger gear was hard at this moment, and also **the rotation speed was decreased too much**, so we decided to remove the bigger gear, connecting to the belt and the motor directrly. The second verison:

{{< postimg url = "mechanics-2.JPG" alt ="second verions of mechanics part" imgw ="80%" >}}

Testing rotation part (the video was shot by Anna)
{{< video src = "mechanics-3.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

During the building process, we couldn't find a circular belt with the perfect length, and if we cut the belt and connected together by ourselves, the connecting part always interrupted the operation of the motor and the belt😥 Finally, Karls found that we can **sew the belt** so we can make a customized belt!

{{< postimg url = "combine_belt.jpg" alt ="a great idea from Karls" imgw ="80%" >}}

About the scoop rotation part, based on the Karls's design, Anna developed the few versions to get a more stable structure. [**Here is her documentation**](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment13/). The final version of the scoop box was really cool, which using worm gear set to increace the force with little space.

(the video was shot by Anna)
{{< video src = "mechanics-4.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

I felt that the mechanics part all work fine during the testing phase, but we found some issues when we assembled them all together 😂 I'll record this part in the latter section.

### Scoop

The scoop part looked not really complicated but actualy it was a tricky part 😂

This part was designed and built by Fiia, although she couldn't come to the lab at the first week because of Covid-19  😢  she made the scoop part efficiently at the second week when she came to the lab. [**Here is her great documentation**](https://digital-fabrication.fiikuna.fi/assignments/assignment-13/)

### Electronics and circuit

In this part, I chose to use **2 stepper motors, Arduino UNO, [gShield](https://www.adafruit.com/product/1750)**. Considering to control the movement more accurately, the stepper motor seems to be better than DC motor.

{{< postimg url = "main_components.jpg" alt ="the main electronic components we used" imgw ="90%" >}}

Because **gShield** is designed for 3-axis CNC machines, it simplifies the circuit of the stepper motor and you can just connect the stepper motor to it without another circuit. Matti remined me an important part is that **you need to adjust the out voltage for each motor by the potentionmeter.** The reason is to limit the current and protect the motor. [Here is the detail explaination](https://discuss.inventables.com/t/setting-the-stepper-motor-current-limit/14998).

{{< postimg url = "g-shield.jpeg" alt ="the ports we used in this project" imgw ="80%" >}}

About the power supply, the main power is 12V DC from a voltage transformer which can convert AC to DC. We also need 5V DC for Arduino. Because out control interface also need to connect to Arduino, we use a cable to connect the computer and Arduino, getting the power and the commands at the same time.

{{< postimg url = "power.JPG" alt ="connect to the power" imgw ="80%" >}}

### Programming

At the beginning, Kris told us we can try to use **g-code and [grbl](https://github.com/gnea/grbl/wiki)** to control the stepper motors easier. I followed [this website](https://diymachining.com/diy-cnc-controller-how-to-setup-your-arduino-gshield/) and the gitlab instruction, then controlled the motors with [**Universal G-code Sender(UGS)**](https://github.com/winder/Universal-G-Code-Sender). That's very cool and I felt that I can understand a little more about how the CNC machine work 😆

But also I found that it's a little tricky if we don't need the motors to act as a CNC machine. It's hard to do some simple but customized commands or connect other sensors or components if we use UGS interface. (but maybe because I haven't not fully understand how g-code and UGS work 😅 ) So I found the [schematic of the g-shield](https://github.com/synthetos/grblShield/blob/master/hardware/gshield_v5_schematic/gshield_v5_schematic_pg1.pdf) and use the pinouts to control the stepper motors in Arduino IDE directly.

Based on the experience from Output week, I mainly follow [this video and his example codes](https://www.youtube.com/watch?v=fHAO7SW-SZI) to control the stepper motors. The imporatant part is that **remember to add some delay between the change of the step pin**!!

[**Here is the code for Arduino**](https://gitlab.com/aaltofablab/machine-building-2022b/-/blob/main/Circuit_Program/control_code/control_code.ino), I wrote some explaination in the code commands.


### Communication and Interface

The communication between the machine and the computer was built by Samuli, [**here is his great documentation**](https://ambivalent.world/assignments/machine-building/). He built up the system and wrote a clear and simple instruction on our gitlab page, so it's easy for me to combine it with the Arduino coding part.

I have a little experience in designing websites, so I expanded the code from Samuli and made a simple interface for the machine. The main concept is that **when the user presses a button on the interface, the computer will send a character to Arduino by serial port, then Arduino will control the motor to move based on the different characters.**

[**Here are the code and the instruction**](https://gitlab.com/aaltofablab/machine-building-2022b/-/tree/main/ui)

I designed the two parts for the interface. One is **Manual Mode**, which is for testing and adjusting the machine; another is **Auto Mode**, which is the main function for the machine. However, the reset part haven't finished yet.

{{< postimg url = "ui.png" alt ="the control interface of the machine" imgw ="80%" >}}

Test with the motors:
{{< youtube v5zkKgVNIX4 "16:9" "80">}}



## Assembly

This part was the most exciting but also most confusing part for me 😂 😂 😂 The exciting part was because we could finally see our machine work! The confusing part was I didn't why all the parts looked work well seperately, but couldn't work so well when assemble them together 😂 

For the more stabel structure and a better looking, we made the base of the machine and some connectors for the motor and the lead screw. [Here are the files](https://gitlab.com/aaltofablab/machine-building-2022b/-/tree/main/DesignFiles/Base)

{{< postimg url = "base.jpg" alt ="the base of the machine" imgw ="80%" >}}

The structure for the stepper motor, we tried to design some space for adjusting the position of the motor:
{{< postimg url = "motor_base.jpg" alt ="the base of the stepper motor" imgw ="70%" >}}

The stucture for adjusting the tightness of the belt:
{{< postimg url = "adjust_belt.jpg" alt ="the height of the middle wheel can adjust the tightness of the belt" imgw ="90%" >}}


After some adjusting, I tested it with the power:
{{< youtube KtLMUhGZJMs "16:9" "80">}}

Also found this component was very helpful to organize the wires:
{{< postimg url = "organize_wire.JPG" alt ="organize the wires" imgw ="90%" >}}

About the connecting part of the scoop and the motor, we found that the point of the stick was hard to fix into the structure because it was rounded. After some trying, we kind of put the the stick inside it but it's not really stable:

{{< postimg url = "connect_motor.jpg" alt ="do some adjustment on the stick to connect" imgw ="90%" >}}


Finnal testing with the auto programme and the sand box, the one-round parocess is: 
1. The scoop goes forward to push all the poops together to one side.
2. The scoop lifts up.
3. The scoop moves to the starting point and back to the original state.

{{< youtube PA8HOkk6NwE "16:9" "80">}}



### Problems

Here are some issues that we found during the whole process and the final test:

- The thread of the lead screw is too tight so the moving speed is really slow, especially when we add a bigger gear to increase the force.

- We removed the bigger gear in the final structure, but the testing showed that we do need the bigger force for pushing because sometimes the scoop was blocked by the sand and the fake poops (and the fake poops are lighter than real ones 😂 )

- For the moving part, Kris also said we can try to use 2 motors for each sides which may be more stable and stronger.

- The shape (of the gap) of the scoop needs to be adjusted because the sizes of the sand is not average, some sand will be pushed together by the scoop.

- The connection between the motor and the scoop is not stable and I think is because we design this two parts seperately. Also, we usually forgot the connecting part when we were discussing the sturcture 😂 ).



## Reflection

This is my first time working with other people to do this kind of project. It's a very fresh and interesting experience for me and I am also very grateful to work with them 😊 On the other hand, I found that my communication skill is bad so I couldn't let other members fully understand my idea, and sometimes we spent a lot of time to discuss. Also, I found that the 3D modeling skill is very important in this kind of teamwork because if we can build a rough model everytime when we have some idea, it will be very helpful to let everyone has a common image to discuss. I need to improve more of my 3D modeling skill.

{{< content >}}
