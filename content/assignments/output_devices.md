---
title: "Output Devices"
foldername: "output_devices"
image: "stepper_circuit.jpg"
date: 2022-04-13
author: "Yuhan Tseng"
tags: ["PCB","motor"]
categories: ["NOTE"]
draft: false
---

This week we tried to connect ouput devices to a board we made, and controled the devices by programming. Because I planed to use **stepper motors** for my final project, I focused on **motor control** in this week.

We learned how to design a board that can connect one or more output devices with a microcontroller chip, and I am curious about if can we make a board modular so I can connect it with different main boards with different microcontrollers. So I planned to make **a main board with ATtiny1614**, **a motor driver modular with full-bridge**, and **a stepper motor driver modular**.

I feel I spent too much time on the main board part and haven't finished the output devices I'm interested for this week. I'll continue to make and try the ouput device boards during the next week.

## Goal

:point_right: Add an output device to a microcontroller board you've designed, and program it to do something.

:point_right: Measure the power consumption of an output device.

## Cat-paw Main Board

I tried to use pinheaders to connect the pinouts of **ATtiny1614** so I can connect other components in the futrue. I thought this would not be too difficult but I failed to make it at the end of this week 😂 Here are some note about the process:

### First try

In this version, I connected the pinouts and extended the 5V+ and GND to each pinout. Also, I thought that would be convenient if the components can get enough voltage from the main board without connecting external power, so I added the high voltage to each pinout.

{{< postimg url = "attiny1614_board.png" alt ="schematic of the main board" imgw ="90%" >}}

The reason that I chose the bigger regulator is because I thought I might connect to multiple motors in the futures that needs more voltag and currents, and I read [Russian's note](http://fabacademy.org/2021/labs/aalto/students/russianwu/assignments/week14-output-devices/week14-output-devices/) and it looked work well with this bigger regulator. I followed the recommanded circuit from the [regulator's datasheet](https://www.onsemi.com/pdf/datasheet/ncp1117-d.pdf), adding two capacitors to the circuit:

{{< postimg url = "bigger_reg.png" alt ="circuit from the datasheet" imgw ="40%" >}}

But actually, this is not a good choice, Kris showed me some motor modular which usually is the part where the higher voltage comes in. It's safer to **separate the high voltage and the main board power** so the high voltage won't go into the control circuit and break the chip.

##### Rotation problem in CopperCAM

I found that if we rotate the graphic of PCB with some special angle (not like 90, 180 degrees) in Kicad, CopperCAM will define the graphic as circle.

{{< postimg url = "coppermac-02.jpg" alt ="left: original design in KiCad, right: after inport the gerber file in CopperCAM " imgw ="90%" >}}

After finishing soldering, I could upload some code to the board, but then I found the part of the regulator was getting hot when the board connects to the computer by the UPDI programmer. Also, the power LED didn't work when the board got the power from the power jack. When I was thinking about how to debug, the board made some smoke and then dead with a little explotion 😂 😂 😂 😂

{{< postimg url = "attiny1614_board2.JPG" alt ="broken board" imgw ="50%" >}}

### Second try

After asking Kris if there are some problems with the circuit design, he told me I could **check the soldering part** first, maybe there are some parts connected together that they shouldn't. And yes, somewhre **my +5v and GND were connected** 😂 😂 😂 Because I couldn't find out where they were connected, I decided to design a new one.

For the new one, I increased the width of the copper wires and the space between the wires, Also, I removed the extended high voltage pins so basically the function of this board was only to provide the controlled signals to different output devices and get the signals or data from the input devices.

{{< postimg url = "attiny1614_board3.png" alt ="schematic of the main board" imgw ="90%" >}}

I checked the soldering part and it looked fine. I could upload the code to the board by UPDI programmer and the power LED worked when the board connected to the computer by UPDI. But when I tried to provide power from the power jack, the power LED didn' work, and the regulator became hotter again.

To find out the reason, I tried to measure the voltage by the multimeter in different positions (red circle):

{{< postimg url = "debug_voltage.png" alt ="measure the voltage of the circle part" imgw ="90%" >}}

And I found something wierd

- Only the power from UPDI connector can go to the extended 5v pin.
- The voltage after the regulator (position B) was not always 5v.

<div width="100%" height="auto" style="display:flex; justify-content:center">
    <table>
        <tr>
            <th>INPUT (v)</th>
            <th>A OUPUT</th>
            <th>B OUTPUT</th>
            <th>C OUTPUT</th>
            <th>D OUTPUT</th>
            <th>POWER LED</th>
        </tr>
        <tr>
            <td>5v from UPDI</td>
            <td>x</td>
            <td>X</td>
            <td>5.24</td>
            <td>5.24</td>
            <td>on</td>
        </tr>
        <tr>
            <td>7v</td>
            <td>7.06</td>
            <td>4.49</td>
            <td>4.49</td>
            <td>0.008</td>
            <td>off</td>
        </tr>
        <tr>
            <td>12v</td>
            <td>12.01</td>
            <td>9.3</td>
            <td>9.34</td>
            <td>0.015</td>
            <td>off</td>
        </tr>
    </table>
</div>

After some fixing, soldering, and disoldering, I couldn't find the reason and fix it, so I made a new one again 😂

### Third try

For this time, I tried to decrease the amount of the rivets because I found I usually made a bad connection between the riivets and the copper wires, I also increased the space between the wires and the width of the wires. 
Finally, although there's a little issue between the power jack and the circuit, I used an aditional wire to connect and it worked! 🎉 🎉 🎉 🎉

{{< postimg url = "mainboard.jpg" alt ="the 3rd version of the cat paw board" imgw ="60%" >}}


Now I can start to try the output device: DC motor and Stepper motor!


## Power converter

{{< postimg url = "power_convert.jpg" alt ="this board can convert high voltage to 5v" imgw ="60%" >}}

When I used the modular I made, I found that was not easy to deal with the power supply because the motor needed 12v and the cat-paw board neeed 5v. So I made a little board for power converting, which can connect up to 20v and also ouput 5v power.

From the failed experience of the cat-paw board when I tried to convert the voltage on that board, I searched some information about the voltage converting cirtuit. I found that many people mention [**1n4007 diode**](https://www.hwlibre.com/en/1n4007/) when they were making an Arduino. It's very helpful to protect the circuit when you use the external power to your board, so I added it to the regulator circuit and made this board.

{{< postimg url = "power_convert-2.png" alt ="the schematic of power converter board" imgw ="90%" >}}


So in this board, there're 5 ouputs of high voltage (the same value of the power supply), and 3 ouputs of 5v.


## Motor Driver with H-bridge

{{< postimg url = "dc_motor_board.jpg" alt ="motor driver modular" imgw ="60%" >}}

I made this modular with [A4953](https://www.allegromicro.com/en/products/motor-drivers/brush-dc-motor-drivers/a4952-3) and a potentiometer. I followed the example circuit from the datasheet and the value of [Russian's note](<http://fabacademy.org/2021/labs/aalto/students/russianwu/assignments/week14-output-devices/week14-output-devices/>). 

**Pay attention to the capacitor that some of them have direction!** usually the black part is negative.

{{< postimg url = "example_circuit.png" alt ="the example circuit from the datasheet" imgw ="60%" >}}

### Testing

- I connected the power supply to the power converter board
- connect the cat-paw board to 5v and GND
- connect the DC motor modular to 12v and GND
- and fianlly connect the 5v and GDN of cat-paw board and the motor modular.

I used the potentionmeter to change the rotation speed and the button to change the spinning direction.

{{< youtube cJm_jMPVCz0 "16:9" "70">}}

```c
int rotate_s = 0;
int btn = 0;
void setup() {
  pinMode(1, OUTPUT); //pin for DC motor
  pinMode(7, OUTPUT); //pin for DC motor
  pinMode(6, INPUT); //pin for potentionmeter 
  pinMode(3, INPUT_PULLUP); //pin for button
}

void loop() {
  //map the value of the potentionmeter to the pwm ouput
  rotate_s = map(analogRead(6), 0, 1023, 0, 255);

  if (!digitalRead(3)) {
    btn ++;
  }
  if (btn == 1) {
    r1(rotate_s);
  }
  else if (btn == 2) {
    r2(rotate_s);
  }
  else
  {
    btn = 0;
    stop_r();
  }
  delay(50);
}

//rotating in one direction
void r1(int s) {
  analogWrite(1, s);
  analogWrite(7, 0);
}

//rotating in another direction
void r2(int s) {
  analogWrite(7, s);
  analogWrite(1, 0);
}

void stop_r(){
  analogWrite(7, 0);
  analogWrite(1, 0);
}

```


## Stepper Motor Driver

{{< postimg url = "stepper_motor.JPG" alt ="stepper motor modular" imgw ="80%" >}}

I made this modular with [**TMC2208**](https://www.trinamic.com/fileadmin/assets/Products/ICs_Documents/TMC220x_TMC2224_datasheet_Rev1.09.pdf). The circuit of the board looked a little weird beacuse I soldered the pins of the TMC2208 in the worng direction 😂

I followed this [**video and his example code**](https://www.youtube.com/watch?v=fHAO7SW-SZI) to control the stepper motor. 


##### Chaning direction

{{< youtube KLsdv1boVEw "16:9" "70">}}

```c
#define DIR_PIN          5
#define STEP_PIN         2
#define ENABLE_PIN       8

void setup() {
  Serial.begin(9600);
  pinMode(3, INPUT);//button pin
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if(digitalRead(3)){ //when pressing the button
    left();
  }
  else{ //when release the button
    right();
  }
}

void left() {
  digitalWrite(DIR_PIN, LOW);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(80);
}

void right() {
  digitalWrite(DIR_PIN, HIGH);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(80);
}

void stop_move() {
  digitalWrite(STEP_PIN, LOW);
}
```



##### Chaning speed

{{< youtube Sx_UtsUa34Y "16:9" "70">}}

```c
// interval = 10 is too fast

#define DIR_PIN          2
#define STEP_PIN         1
#define ENABLE_PIN       6

int interval = 1000; //default is a slower speed

void setup() {
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(3, INPUT_PULLUP); //button
}

//a funtion to rotate the motor
void simpleMove(int steps) {
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(interval);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void loop() {
  if (!digitalRead(3)) {
    //digitalWrite(DIR_PIN, LOW);
    interval =80;
  }
  else{
    interval =1000;
  }

  simpleMove(1);

}
```


##### Multiple Outputs

I also tried to control the DC motor and the stepper motors at the same time with cat-paw board, because connecting multipul devices is one of the reason of making a mainboard.

{{< youtube FTpVSIRdfKc "16:9" "70">}}

```c
// interval = 10 is too fast

#define DIR_PIN          2
#define STEP_PIN         1
#define ENABLE_PIN       6
#define M1               8
#define M2               9

int interval = 1000;


void setup() {
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(M1,    OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(3, INPUT_PULLUP); //button
}

void simpleMove(int steps) {
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(interval);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void loop() {
  if (!digitalRead(3)) { //when pressing the button
    interval = 80; //stepper motor rotates faster
    digitalWrite(M1, HIGH);
    digitalWrite(M2, LOW);
  }
  else { //when releasing the button
    interval = 1000; //stepper motor rotates slower

    //DC motor change the rotating direction
    digitalWrite(M2, HIGH);
    digitalWrite(M1, LOW);
  }
  
  simpleMove(1);

}
```

## Files

- [KiCad project of cat-paw board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/output_devices/ATtiny1614_flexible4)
- [KiCad project of power converter board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/output_devices/DCmotor_driver)
- [KiCad project of DC motor driver modular](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/output_devices/power_convert)

Because the circuit of the stepper motor board is opposite so I didn't upload to here, I'll put the right version in the later week.

( 0614 Update : the file of [**the latest version of the stepper motor board**](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/stepperMotor_412_reg_LM2940) )

{{< content >}}
