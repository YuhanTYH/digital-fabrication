---
title: "Debug Log"
foldername: "debug_log"
image: "heroImg.jpeg"
date: 2022-01-24
author: "Yuhan Tseng"
tags: ["debug"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

Here is a place to record all the debugging processes and unsolve problems.

## Unsolved Problems

- What is the concept of branch?
- How to use parameter when writing CSS in shortcodes?


## Visibility of project

  **What is the problem?**
  
  Links of my repository and published website are not availible to other people. I didn't fond this problem untile other people tried to open the link because I only used my own computer and account to open these links.
  
  **Summary**

  1. My project is in a private group so it's unavailable to other people
  2. Somehow I connect the git to my whole computer driver, and this seems to cause problems connecting the exiting local folder to another GitLab repository.
  3. TBC....

  **Solving Process**
  
  I tried to change the visibility setting of my project.
  
  In my GitLab project -> Settings -> General -> Visibility, project features, permissiions. But found that my "Public" option was unselectable.
  {{< postimg url = "publicError.png" alt ="visibility setting" imgw = "70%">}}
  
  Also I went to my account Profile setting about Private profile. But that one was off.
  {{< postimg url = "publicError2.png" alt ="private profile setting" imgw = "70%" >}}
  
  We found somehow I created a private group and the project is in this group. So I tried to make the group public by the group settings but it didn't work, the project was still unavailable to others.
  
  I deleted the whole group and created a new blank project. And then tried to connect the repository to my local folder.
  
  Then I got a problem when adding git remote ```git remote add origin + git URL```. I can't conect my new project to my local folder.
  {{< postimg url ="publicError3.png" alt = "error screenshot" imgw = "80%">}}
  
  ```git remote set-url origin+ git URL``` seems to work!
  
  Then we tried " git pull" and looked like there're some problems about the branch.
  {{< postimg url ="publicError4.png" alt = "error screenshot" imgw = "80%">}}
  
  We found that I had two branches: master & main so trying to merge them from the GitLab project setting. But cannot do that because of some conflication(?) I deleted the "master" branch on the GitLab project setting. According to this [discussion](https://stackoverflow.com/questions/64249491/difference-between-main-branch-and-master-branch-in-github), I guessed the reason that I had a "master branch" is because I tried to use GitHub long time ago.
  
  And used ```git branch -M main``` to make my branch to "main".
  {{< postimg url ="publicError5.png" alt = "terminal screenshot" imgw = "80%">}}
  
  Then I got error when "git push". It seems because I accidentally connect the git repository to my whole local user folder. So I deleted the ".git" folder in my local user folder and also one in my folder for digital fabrication. (make sure that the files are also deleted in trash folder).
  {{< postimg url ="publicError7.png" alt = "terminal screenshot" imgw = "80%">}}
  
  Reconnect the digital fabrication folder to GitLab project:
  
  ```shell
  git init
  git branch -M main
  git remote add origin + git URL
  ```
  
  {{< postimg url ="publicError8.png" alt = "terminal screenshot" imgw = "80%">}}
  Then I had problem in pushing files. Seems to be caused by my privious setting about connencting git to my computer? We tried the following steps to solve:
  
- remove ".gitlab-ci.yml" by ```git rm +file's name```
- **create a ".gitignore"** (the files' name that recoded in this file will be ignored by git)
- ```git pull origin main --allow-unrelated-histories```
- ```git push --set-upstream origin main```
  
  and at this stage, everything looked nice...but I counld not push modified file. It seems to because I added a new ".gitlab-ci.yml" at my GitLab project page, which created a file that didn't exist in my local folder.
  {{< postimg url ="publicError9.png" alt = "terminal screenshot" imgw = "80%">}}
  
  then tried ```git pull```
  
  Everything is good now!
