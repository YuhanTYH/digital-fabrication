---
title: "Interface and Application Programming"
foldername: "interface"
image: "hero_img.jpg"
date: 2022-05-12
author: "Yuhan Tseng"
tags: ["stepper motor","interface","TouchDesigner"]
categories: ["NOTE"]
draft: false
---
{{< content >}}

This week we chose a software (or platform) to make a control interface. Because I plan to project some images on my final project which will be made by **TouchDesigner**, I think that using TouchDesigner to make a control interface for stepper motors will be more convenient. I can control the mechanical parts and process the images in the same software.

## Goal

:point_right:  Write an application that interfaces a user with an input and/or output device that you made.

Because the interface of the final project is more like a tool for me to set up the installation instead of letting the audiences to play around. The more concrete goals for the user interface are:

- Control the speed and direction of each stepper motor. ✅​
- Set the starting position of each stepper motor. ❌
- Use the data from a sensor. ❌

This week I only finished the first goal so I'll keep working on the other goals.


## TouchDesigner & Interface

To get clearer what I was going to do, I divided a functional interface into two parts: **communication part** and **visual part**. In TouchDesigner, there're some very useful and convenient operators that can be used for these two parts:

### Communication part

I searched for the tutorial about "TouchDesginer and Arduino", finding there were many related videos. Although the approaches and the codes they used were a little different, the main operation was through [**DAT Serial** operator](https://docs.derivative.ca/Serial_DAT). It works similarly with the Serial Monitor of Arduini IDE.

I got the basic idea how the communication work between TouchDesigner and Arduino from these two videos:

{{< youtube GyPovhaTO04 "16:9" "80">}}

{{< youtube 4bv5YiUE8_s "16:9" "80">}}


### Visual Part

The [**COMP** operator](https://docs.derivative.ca/Component) is different from other operators' families, it's more like different components that you can build a network or an interface, instead of the functional blocks.

The convenience of using TouchDesigner to build an interface is that you can adjust the properties of the operator or component directly by the setting panel, and see the outcome immediately.


## What I tried

After trying to use a single button of TouchDesigner to trun on and off of the LED on Arduino, I started to try with the stepper motor that drived by the board I remade last time. About the control command, I wanted to [follow the rule](https://yuhantyh.gitlab.io/digital-fabrication/assignments/network/#2-cat-paw-board--2-stepper-motors) I tried last week.

### A button controls a motor

I tried to use a button to turn on/off a stepper motor. It's similar to [**use the cat-paw board to control a stepper motor**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/network/#2-cat-paw-board--stepper-motor-board). I mainly followed the [first YouTube video](https://www.youtube.com/watch?v=GyPovhaTO04), now TouchDesigner did what the cat-paw board did:

{{< postimg url = "control_one_motor.png" alt ="use operators with some python code" imgw ="90%" >}}

And the code for the motor was almost the same as last time, the only difference was that I can send the command as byte directly from cat-paw board, but I don't know how to send data as byte from TouchDesigner so I sent the command as **decimal integer** from TouchDesigner

```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3

byte address, motor;
int test; // the incoming command is an integer

void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {
  if (Serial.available() > 0) {

    test = Serial.read();//save the incoming command

    Serial.print(test);

    //filter out address number
    address = test & 0b00000001;

    //filter out switch valure: 0(OFF) or 1(ON)
    motor = (test & 0b00000010) >> 1;

  }

  if (motor) {
    rotation(100);
  }
  else {
    stop_rotate();
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delay(1);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```

{{< youtube P6uzsrh097g "16:9" "80">}}




### An interface to control 2 motors


#### Communication part

Then I tried to control the speed and the rotating direction of 2 stepper motors. At first, I thought [the second YouTube video](https://www.youtube.com/watch?v=4bv5YiUE8_s) was what I need, but I found she used the character as a command that was easier for us to understand but need more proceed process in the microcontroller. Also, somehow I could not get the same result as the video when I followed what she did ​😅​

I think that the binary data is more efficient for ATtiny412 which I used for the stepper motor board, so the goal is to let ATtiny412 only need to process data as bytes, keeping the code as simple as possible in ATtiny412.

Because I'm not familiar with python, I spent lots of time figuring out how to combine three values (motor ID, direction, speed) into one byte (8 bits) format and send it as a decimal number to ATtiny412.

Here is how I combine three variables into binary format and finally send them as a decimal number. Although it worked fine somehow I felt there was an easier way to do 😂​ 😂​ 😂​

```python
def onValueChange(channel, sampleIndex, val, prev):
 
 # use a table operator to save variables
 # update the value here every time the "RUN" button is clicked
 id = op('table1')[0,1]
 dir = int(op('table1')[1,1])
 speed = int(op('table1')[2,1])

# when the "RUN" button is clicked
 if val > 0:
  
  # convert direction value from dicimal to bits
  # in the String format
  dir2 = decimalToBinary(dir) + '0000'
  speed2 = decimalToBinary(speed) + '00000'
  id2 = decimalToBinary(id)
  
  # convert String to decimal integer and add them up
  # the outcome will be the 8 bits command
  # but acctually is an decimal integer
  cmd = int(dir2) + int(id2) + int(speed2)
  
  # convert decimal integer to a String
  cmd2 = str(cmd)

  # update the text of interface
  op('container5/text1').par.text = 'Command:'+cmd2

  # convert a 8-bits String to a decimal number
  cmd3 = binaryToDecimal(cmd2)

  # send to ATtiny412 by serial communication
  op('serial1').sendBytes(cmd3)

 return
 
def decimalToBinary(n):
    # converting decimal to binary
    # and removing the prefix(0b)
    return bin(n).replace("0b", "")
    
def binaryToDecimal(val): 
    return int(val, 2)
```

About the code for the stepper motor board, it's almost same as [the code I used last time](https://yuhantyh.gitlab.io/digital-fabrication/assignments/network/#2-cat-paw-board--2-stepper-motors), but I added some code to chage the rotation speed with different speed mode.

```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3
#define ID 10

byte id, m_speed, m_dir;//save incoming command
byte m_speed2, m_dir2;//save for this motor
int test; //the incoming command is an integer

void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available() > 0) {
    test = Serial.read();
    //print out the input command
    Serial.print(test);

    //filter the ID number: 0 ~ 15
    id = test << 4;
    id = id >> 4;

    //filter the direction: 1 or 0
    m_dir = (test & B00010000) >> 4;

    //filter the rotation speed: 0 ~ 7
    m_speed = test >> 5;
  }

  //if the command ID is same as this motor ID
  if ((int)id == ID) {
    //save the setting of speed and direction
    m_speed2 = m_speed;
    m_dir2 = m_dir;

    if (m_dir2) {
      digitalWrite(DIR_PIN, HIGH);
    }
    else {
      digitalWrite(DIR_PIN, LOW);
    }
  }

  //set the different speed based on the speed mode value
  switch ((int)m_speed2) {
    case 0:
      stop_rotate();
      break;
    case 1:
      rotation(800);
      break;
    case 2:
      rotation(700);
      break;
    case 3:
      rotation(600);
      break;
    case 4:
      rotation(500);
      break;
    case 5:
      rotation(400);
      break;
    case 6:
      rotation(300);
      break;
    case 7:
      rotation(200);
      break;
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(s);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```

</br>

#### Visual part

{{< postimg url = "interface.png" alt ="an interface to control 2 motors" imgw ="60%" >}}

About the components of interface, I mainly used **button, slider, and container**:

{{< postimg url = "comp_operator.png" alt ="the componets I used to build the interface" imgw ="80%" >}}

I learned some basic functions of these componets, but I have not explored much about the appeareance setting of these components yet.

Using a button for one motor. When the button is press down, the id will be set as the motor's ID. I'm not sure how will this part be if I have 16 motors for the final project ​😂​ maybe II'll try to use keyboard input?

{{< postimg url = "idbutton.png" alt ="a button for each motor" imgw ="90%" >}}

Using a slider to change the speed. Because there are 3-bit are for setting the speed, which can be 0-7, the output of the slider is set to the integer 0-7.

{{< postimg url = "slider.png" alt ="a slider to change the rotation speed" imgw ="90%" >}}

Using a button for rotation direction. Because only two direction, I can used the state (value) of the button (0 or 1) directly.

{{< postimg url = "dirbutton.png" alt ="a button to change the rotation direction" imgw ="90%" >}}

After setting each component and its funciton, put them together in a container operator.

{{< postimg url = "interface2.png" alt ="integrate all the stuff to a container" imgw ="90%" >}}


#### Testing

{{< youtube 1T9AvZT1pwk "16:9" "80">}}


