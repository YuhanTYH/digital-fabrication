---
title: "Networking and Communications"
foldername: "network"
image: "hero_img.jpg"
date: 2022-05-04
author: "Yuhan Tseng"
tags: ["PCB","circuit"]
categories: ["NOTE"]
draft: false
---
{{< content >}}

## Goal

:point_right: Design, build, and connect wired or wireless node(s) with network or bus addresses.

:point_right: Send a message between two or more projects.


## How Devices Communictate?

I got lost because there are so many different ways of communication between different hardware 😵 So before doing some test with the boards and components, I tried to understand the differences among these protocols.

I found this video that introduces different serial communication protocols. I got some basic idea about how to transmit data between different devices.

{{< youtube IyGwvGzrqp8 "16:9" "80">}}

### Serial communication protocol

This time we mainly focused on serial communication, which is a way to transmit digital data that all the information is showed by **0 & 1**.

- Parallel communication: send all the bit daya at the same time, one bit needs one wire.
- Serial communication: send multiple bits data **in a sequence**, only needs **one or two wire (send or receive data) wires **, but slower than parallel communication.


### Serial port: UART

- Universal Asynchronous Receiver/Transmitter
- Sending and receiving serial data.
- For **low-speed, low-throughput** communication.
- Useing **TX & RX pins** and GND, so it only needs **two wires** for the communication, or you can only use **one wire** if the device only need to receive or transmit data.
- It is **asynchronous**, which means that two devices don't share a common clock signal. So they need to:

    - transmit the data **at the same speed**, like we set the **baund rate** in Arduino IDE.
    - have a **start & stop** signal data (START & STOP bits).
    - set the same **data length**.

- Simple, low-cost, easy to use.


### I2C (SDA/SCL)

- Inter-Intergrated Circuit
- Simplex
- It is **synchronous** so it needs to set the clock which using **SCL pin**, and **SDA pin** is for sanding data.
- So it uses **two wires** and GND for the communication.
- It is faster than UART because it uses frequency to know when each bit end, so it dosen't need to counte the time.
- You can use one mater connect to multiple slaves, using address to control specific slave.


### SPI

- Serial Peripheral Interface
- Duplex
- It needs **four wires** and GND to communicate:
    - SCLK: clock
    - MOSI: master output --> slave input
    - MISO: slave output --> master input
    - SS: selection pin


From the vedio's intuductionn, I felt that I2C is better for my project because I want to control multiple stepper motors (I hope I can contol 16 stepper motors 😂) at the same time with different directions and speeds. But also Kris reminded that we still need UART to debug (checking the data on our computer), which means that I need use both I2C and UART at the same time. That's a little complicated for me now.

Although in this video, it said UART is for the communication only between two devices, I wonder can I also use address to control multiple motors by UART because Kris used this way in the lecture, too.

After clarifing my goal:
- Control more than one stepper motors at the same time.
- Less and simpler wire connection.
- All using ATtiny412 ( I just somehow feel it would be cool if I can do this for my final proect 😆 )

I decided to focus on **UART, which using RX/TX pins** this week.

## Round 1: Two Boards

### 1. Arduino UNO & Cat-paw board

At first, I tried to communicate an Arduino UNO and my cat-paw board (ATtiny1614).

{{< postimg url = "round1.JPG" alt ="the conmmunicate between UNO, cat-paw board, and computer" imgw ="90%" >}}

**Code for sender (Cat-paw board)**

```c
///use the first bit as address: 0 or 1
//use the second bit as switch: 0->OFF, 1->ON

byte test = 0; //save the command

void setup() {
  Serial.begin(9600);
  pinMode(3,INPUT_PULLUP); //pin for button
}

void loop() {
  if(!digitalRead(3)){ // when press the button
    test = 0b00000011; //turn on LED
  }
  else{// when release the button
    test = 0b00000001; //turn off LED
  }
  Serial.write(test);
}
```

**Code for receiver (Arduino UNO)**

```c
byte test, address, led;

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);//pin13 for LED
  digitalWrite(13, LOW);//turn off LED at the beginning
}

void loop() {
  //when something send in serial port
  if (Serial.available() > 0) {
    
    test = Serial.read();//read the first byte
    //save the command in variable "test"
    
    Serial.print("Incoming:");
    Serial.print(test, BIN);
    
    Serial.print(", Address:");
    //use "and" to filter out the address value
    //because only 0 or 1
    address = test & 0b00000001;
    Serial.print(address, BIN);
    
    Serial.print(", LED state:");
    //use "and" to filter out the LED state value
    led = (test & 0b00000010) >> 1;
    Serial.println(led, BIN);

    if (led) {
      digitalWrite(13, HIGH);//turn on LED
    }
    else {
      digitalWrite(13, LOW);//turn off LED
    }
  }
}
```
{{< youtube _K6KscvXhYs "16:9" "80">}}

Although the LED on Arduino UNO acted correctly, it responded very slowly! Then I remembered that "print" function is very time-consuming when I use Processing, so I **commanded (removed) all the "Serial.print()" in the code,** and the LED responded immediately now!

{{< youtube dJX3Q3Okccc "16:9" "80">}}

Also, because the cat-paw board only sent data and the Arduino only received data, I can just connnect the blue wire without purple wire.

### 2. Cat-paw board & Stepper motor board

Using the same concept of turning ON/OFF the LED, I tried to run/stop a stepper motor. I use cat-paw board to send command, and a stepper motor board with ATtiny412 will rotate a stepper motor or stop it.

{{< postimg url = "round1-2.JPG" alt ="the conmmunicate between cat-paw board and stepper motor board" imgw ="90%" >}}

**Code for sender (Cat-paw board)** (same as the previous one)
```c
///use the first bit as address: 0 or 1
//use the second bit as switch: 0->OFF, 1->ON

byte test = 0; //save the command

void setup() {
  Serial.begin(9600);
  pinMode(3,INPUT_PULLUP); //pin for button
}

void loop() {
  if(!digitalRead(3)){ // when press the button
    test = 0b00000011; //turn on motor
  }
  else{// when release the button
    test = 0b00000001; //turn off moto
  }
  Serial.write(test);
}
```

**Code for receiver (stepper motor board)**
```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3

byte test, address, motor;


void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

//after using print funtion to check the data
//I commanded all of them to make the program operate faster
void loop() {
  if (Serial.available() > 0) {
    test = Serial.read();//save the incoming command

    //Serial.print(test, BIN);
    //Serial.print(",");

    //filter out address number
    address = test & 0b00000001;
    //Serial.print(address, BIN);
    //Serial.print(",");

    //filter out switch valure: 0(OFF) or 1(ON)
    motor = (test & 0b00000010) >> 1;
    //Serial.println(motor, BIN);

    if (motor) {
      rotation(100);
    }
    else {
      stop_rotate();
    }
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delay(1);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```
{{< youtube rSo6_2gkO0g "16:9" "80">}}


### 3. Serial monitor & Stepper motor board

Then I tought of the graphic that Kris draw in the lecture. I just realized that I can also send the command from computer to the stepper motor board directly. If this can be done, I think it would be useful for the next week assignment.

{{< postimg url = "connect_pic.png" alt ="the illustration of devices connection wiht TX/RX (draw by Kris)" imgw ="90%" >}}

I connected the stepper motor board to the computer with FTDI serial board. After trying some input and print out the data, I found that:

- The data type of input from the Serial monitor of Arduino IDE is alway **Char**, and 1 char = 1 byte.

- If I want to compare the input charater in the program, I need to **convert the charater to deccimal value** by [**ASCll code**](https://www.ascii-code.com/).

- I can use different characters to represent different commands. So I set the rule of the command:
    - The first byte is the direction of rotation: L or R.
    - The second byte is the speed of rotation: S (slower speed) or F (faster speed).
    - If the second byte is any character except S or F, then stop the motor.

{{< youtube DEvgXw9i4E4 "16:9" "80">}}

About the code, I basically followed [the video](https://www.youtube.com/watch?v=9HivniieLvI) that Kris put on MyCourse, and combined it to the code of controlling stepper motor.

**Code for stepper motor board**
```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3

char test[5];//an array to save the input command


void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available() > 0) {
    Serial.readBytes(test, 2);//read two bytes of the command

    //print out the input command
    Serial.print("Command:");
    Serial.println(test);
  }

  if (test[0] == 76) { // "L" turn left
    digitalWrite(DIR_PIN, LOW);
  }
  else if (test[0] == 82) { // "R" turn right
    digitalWrite(DIR_PIN, HIGH);
  }


  if (test[1] == 70) { // "F" faster speed
    rotation(100);
  }
  else if (test[1] == 83) { // "S" slower speed
    rotation(800);
  }
  else { // any other char => stop rotation
    stop_rotate();
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(s);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```

## Round 2: Three boards

After I got some basic idea of communicating two boards, I tried to control two stepper motos at the same time.

### 1. Serial monitor & 2 stepper motors

I first tried to use serial monitor of Arduino IDE to control two stepper motors. I used the same rule of the previous test, and added one more byte in the command so the first command was the ID of the stepper motor.

{{< youtube 2grd3q0jDYk "16:9" "80">}}

I think the key point of the code is that you need to check the ID first, and then decided if this motor will act or not. After some trying and fail, I came up with using another array to save the command for the motor I want to control.

**Code for stepper motor board**
```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3
#define ID 7 // the ID of the motor
//change this number according to the motor IDD


char test[5];//an array to save the input command
char command[3];//an array to save the command for this motor


void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available() > 0) {
    Serial.readBytes(test, 3);//read two bytes of the command

    //print out the input command
    Serial.print("Command:");
    Serial.println(test);
  }

  //check if the command is for this motor
  if ((test[0] - '0') == ID) {
    for (int i = 0; i < 3; i++) {
      command[i] = test[i];
    }
  }

  if (command[1] == 76) { // "L" turn left
    digitalWrite(DIR_PIN, LOW);
  }
  else if (command[1] == 82) { // "R" turn right
    digitalWrite(DIR_PIN, HIGH);
  }

  if (command[2] == 70) { // "F" faster speed
    rotation(100);
  }
  else if (command[2] == 83) { // "S" slower speed
    rotation(800);
  }
  else { // any other char => stop rotation
    stop_rotate();
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(s);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```


### 2. Cat-paw board & 2 stepper motors

I think the program's speed of dealing with byte is faster than dealing with character, because in this test I only used **1 byte** and I needed **3 chracters (= 3 bytes)** in the previous test.  (not sure about this because I didn't know how to measure the operation time of the program). However, I didn't know how to send byte data from serial monitor. To try with the command in the form of byte, I used cat-paw board to send the command and control two stepper motors.

I set the rule of the command like this:

{{< postimg url = "rule_command.png" alt ="the command to control a stepper motor" imgw ="70%" >}}

Then I connected three buttons to cat-paw board as input.
- Button 1: turn on motor-3, turn left in slower speed.
- Button 2: turn on motor-7, turn right in faster speed.
- Button 3: turn on both motors.

{{< postimg url = "serial_2motor.JPG" alt ="connect cat-paw board to two stepper motor boards" imgw ="90%" >}}

**Code for the sender (cat-paw board)**
```c
#define btn1 10
#define btn2 9
#define btn3 8
#define btn4 3

byte cmd_1 = B00100011;//speed:1, dir:0, id:3
byte cmd_2 = B00000011; //speed:0, dir:0, id:3 (stop)
byte cmd_3 = B01110111;//speed:3, dir:1, id:7
byte cmd_4 = B00000111; //speed:0, dir:1, id:7 (stop)


void setup() {
  Serial.begin(9600);
  pinMode(btn1, INPUT_PULLUP);
  pinMode(btn2, INPUT_PULLUP);
  pinMode(btn3, INPUT_PULLUP);
  pinMode(btn4, INPUT_PULLUP);
}

void loop() {

  if (digitalRead(btn1) == LOW) {//when press button1
    Serial.write(cmd_1);//send command 1: turn on motor-3
  }

  else if (digitalRead(btn2) == LOW) {//when press button2
    Serial.write(cmd_3);//send command 2: turn on motor-7
  }

  else if (digitalRead(btn3) == LOW) {//when press button3

    //turn on both motors
    Serial.write(cmd_1);
    Serial.write(cmd_3);
  }

  //when release bottons, turn off both motors
  else {
    Serial.write(cmd_2);
    Serial.write(cmd_4);
  }
}
```

**Code for the receiver (stepper motor board)**
```c
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3
#define ID 7

byte test, id, m_speed, m_dir;//save incoming command
byte m_speed2, m_dir2;//save for this motor

void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available() > 0) {
    test = Serial.read();
    //print out the input command
    //Serial.print("Command:");
    //Serial.print(test, BIN);

    //filter the ID number: 0 ~ 15
    id = test << 4;
    id = id >> 4;
    //Serial.print(",id:");
    //Serial.println((int)id);

    //filter the direction: 1 or 0
    m_dir = (test & B00010000) >> 4;
    //Serial.print(",dir:");
    //Serial.print(m_dir, BIN);

    //filter the rotation speed: 0 ~ 7
    m_speed = test >> 5;
    //Serial.print(",speed:");
    //Serial.println(m_speed, DEC);
  }

  //if the command ID is same as this motor ID
  if ((int)id == ID) {
    //Serial.println("YES");

    //save the setting of speed and direction
    m_speed2 = m_speed;
    m_dir2 = m_dir;

    if (m_dir2) {
      digitalWrite(DIR_PIN, HIGH);
    }
    else {
      digitalWrite(DIR_PIN, LOW);
    }

    if (!m_speed2) {
      stop_rotate();
    }
    else if ((int)m_speed2 == 1) {//slower speed
      rotation(800);
    }
    else if ((int)m_speed2 == 3) {//faster speed
      rotation(100);
    }
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(s);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(s);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```

{{< youtube 4D12BrwOKns "16:9" "80">}}

