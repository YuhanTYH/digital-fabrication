---
title: "3D Scanning and Printing"
foldername: "3d_scan_print"
image: "heroImg.JPG"
date: 2022-03-02
author: "Yuhan Tseng"
tags: ["3D model"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we tried 3D scanner and 3D printer in Aalto FabLab.

## Goal

:point_right: Understand how the 3D scanner works and how to process the scanned file.

:point_right: Understand how the FDM 3D printing works and how to use.

## 3D Scanning: Artec Leo

{{< postimg url = "3d_scanner.jpg" alt ="Artec Leo 3D scanner" imgw ="80%" >}}
There are several 3D scanners in Aalto FabLab and we mainly use **Artec Leo 3D scanner** for this time. It features:

- fully mobile and wireless
- not only scan the object's shape but also capture its texture
- higher level processor to get higher quality and accuracy
- easy to operate
- easy to process the scanning result with its software (**Artec Studio 15**)

## How It Works?

Artec 3D scanner applies the **structured-light method** to get the object's model. This method projects multiple light stripes on the objects, and calculate the object's shape and distance by the reflecting light stripes. The reflecting light stripe will curve in different ways If the object's surface is not flat, and also the width of the light stripes will be different in the different distances.
{{< youtube mSsnf5tqXnA "16:9" "80">}}

Because the scanner needs the reflected light from the object's surface, there are some materials that are hard to be scanned because they will affect the light reflection. From the Artec's website, there're some [suggestions about this issues](http://docs.artec-group.com/as/15/en/scan.html#selecting-and-preparing-objects-for-scanning) and [how to prepare the object](http://docs.artec-group.com/leo/scan.html#prepare-object-and-surroundings)

## How to Use?

### Scanning

[Online manual of Leo](http://docs.artec-group.com/leo/index.html)

1. Put the object on a clear background, you can make some mark (e.g. red X )on the background.
2. Press the power button to turn on the scanner. Sometimes it needs few minutes to finish this process.
3. You can see the previous projects on the screen. Press **NEW PROJECT** to create a new scanning project.

{{< postimg url = "use_scanner-1.jpg" alt ="power button and the user interface of Leo" imgw ="80%" >}}

4. There're some setting for the scanning, here are [the explanation](http://docs.artec-group.com/leo/scan.html#configure-scanning) of each setting from Arctec's website.

{{< postimg url = "use_scanner-2.jpg" alt ="setting for the scanning" imgw ="60%" >}}

5. Press **START** or press the button on the holder of the scanner and start to scan the object. Here are some tips for scanning:
    - Start to point to the base (ground).
    - When the object displays in **green color**, which mean the distance is good, can get the better result.
    - Don't move too fast.

6. Press **STOP** or press the button on the holder again to stop scanning. You can press **START** to continue scanning that will save the outcome in the same take, or you can press **Projects** on the left-top corner to finish this take.

7. You can have multiple takes for an object. Scanning the object from different angles and positions can help the computer to calculate a more complete and accurate model (but also the file's size will be bigger and you need more time to upload it).

### Processing the scanned file

[Online manual of Artec Studio 15](http://docs.artec-group.com/as/15/en/index.html)

1. Open the software --> Import from Leo --> Connected by IP
{{< postimg url = "artecStudio.jpeg" alt ="open the software" imgw ="60%" >}}

2. Make sure Leo scanner is open the network page. If the scanner is not opening this page, the software can't find the device:
    - SETTINGS (left-bottom corner) --> NETWORK --> press the ">" of the wi-fi --> find the IP address
    {{< video src = "scanner-network.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

    - Make sure the scanner connecting the same internet network of the computer which is running the software.
    - Enter the IP address into the software window --> Connect
    - When connecting is successful, you can choose your project to import.

3. Process the scanning takes. Here are some basic process and tips:
    - Modify each takes first.
    - Remove the extra scanning part: Editor --> Eraser --> Lasso selection --> Ctrl + select the area you don't want ( Use "Orthogonal view" can make this process easier).
    - Tools --> Outline removal
    - Align different takes: Align --> **Auto-alignment** --> Apply
    - Combine all takes into one model: Tools --> **Fast fusion**
    - To fix the holes on the fusion model: (1)Tools --> **Hole filling** (2)**Fix holes**
    - Help to work the model in other 3D software: Tools --> **Mesh simplification**
    - [Here](http://docs.artec-group.com/as/15/en/process.html#) are the explaination of other settings

4. Get the texture of the object: Texture --> Select the textures you want to apply
    - Check **Export** & **Enable texture normalization**
    - Press **Apply** and you can adjust the property of the texture --> **Apply**

5. Export the model: File --> Export --> **Meshes**
    - Create a folder because  there will have two exported files (3D object and the texture)
    - Select **".obj"** to export the 3D object and texture files.
    - Select **".png"** for the texture file.

### Charging the Scanner

There is a power cable can connect the scanner and the wall socket. The battery is at the bottom of the scanner, you can check the state of battery by taking out the side cover of the battery.
{{< postimg url = "charge_scanner.jpg" alt ="charge the scanner" imgw ="80%" >}}

## What I tried

I tried to scan a game controller and a AirPod cast. I found that it's harder to get a nice result than I thought. But it's may because both the objects I used were mainly black and too small.

### Game Controller

I first tried the game controller. I took about 4 takes for it and upload to the software.
{{< postimg url = "game-controller.JPG" alt ="scan a game controller" imgw ="50%" >}}
{{< video src = "game-controller2.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

After erasing the extra parts, I found the auto-alignment didn't work and there'er many parts didn't be scanned. The scanned file could not align properly:
{{< video src = "game_controller3.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Maybe because:

- I didn't notice the red marks on the base (the background). I put the object on different positions randomly for each take so it's hard for scanner to fix the position.
- The main color of the object is black and I didn't adjust the scanning setting to enhance the performance.

### AirPod Cast

I also tried to scan my AirPods Cast.
{{< postimg url = "airpod-cast.jpg" alt ="scan a AirPod cast" imgw ="60%" >}}

And I did some adjustment of the scanning setting:

- Consider the red marks on the base when I changed the position of the object.
- Increase the TEXTURE BRIGHTNESS.
- Make sure I'm using HD RECORDING (but this will make the file's size much bigger and need more time to upload).
- Change the RANGE to 0.5m.
- Change to a smaller base because the original base is a big table that made me hard to go around the object.

The scanned file aligned more accurately but still need to edit it manually:
{{< video src = "airpod-cast2.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Scanned the bottom inn seperate take to fix the hole:
{{< postimg url = "airpot-cast3.JPG" alt ="change to a smaller base and scan the bottom of AirPod cast" imgw ="80%" >}}

Applied texture to the scanned model:
{{< video src = "airpod-cast4.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Final 3D model file opened in Blender:
{{< video src = "airpod-cast5.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

## 3D Printing

3D printing is an **additive manufacturing technology**, which creates a 3D solid model by adding material layer by layer. Here is the common process of creating a 3D object with 3D printer:

{{< postimg url = "3dprint_workflow.jpg" alt ="basic process to create a solid object by 3D printer" imgw ="100%" >}}

There are mainly three kinds of 3D printers in Aalto FabLab:

- Ultimaker 3D Printer (S3 & 2+extended)
- LulzBot 3D Printer (Mini & TAZ 6)
- Formlabs Form SLA 3D printer (2 & 3)

The first and second kinds apply [FDM 3D printing technology](https://en.wikipedia.org/wiki/Fused_filament_fabrication#Fused_deposition_modeling), and the third one uses [SLA 3D printing](https://en.wikipedia.org/wiki/Stereolithography).

## Ultimaker 3D Printer

There is a [**Ultimaker S3**](https://ultimaker.com/3d-printers/ultimaker-s3) and a **Ultimaker 2 Extended+**. The operation of these two printers are almost same. The slicer software is **Ultimaker Cura**.

{{< postimg url = "ultimaker.jpg" alt ="Ultimaker 3D printers in FabLab" imgw ="100%" >}}
{{< postimg url = "ultimaker_uii.jpg" alt ="Ultimaker 3D printer's user interface" imgw ="80%" >}}

### Prepare a 3D model

Inspired by the [snow duck maker](https://odditymall.com/duck-shaped-snowball-maker) and [this model](https://www.thingiverse.com/thing:4741656), I tried to make a **snow poo maker** by Ultimaker 2 Extended+.
{{< postimg url = "duck-maker.jpg" alt ="snow duck makers found on the Internet" imgw ="90%" >}}

I made the model by Fusion 360. At this time, I learned how to make some more organic shape and also the structure of hinge. I exported the model as .stl file.
{{< video src = "poo-maker1.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

<div width="80%" style="display:flex; justify-content:center">
    <iframe src="https://aalto254.autodesk360.com/shares/public/SH35dfcQT936092f0e43dc1dbcd803c567a1?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
</div>

### How to print?

1. Open Ultimaker Cura
    - Select the printer you are going to use.
    - Select the material (PLA for this time) and the nozzle size (now is 0.4 mm)
2. Open file (or the folder sign button) --> choose the 3d model file.
3. Adjust and arrange the 3D model by the tools on the left bar.
4. Set the printing setting. [**Here**](https://support.ultimaker.com/hc/en-us/sections/360003548619-Print-settings) are the explainations for each setting.
5. Click **Slice** at the left-bottom corner to process the 3D model.
6. Click **Preview** to check the slicing result. You can see the structure of each layer also how the printer will run in each layer.
{{< video src = "poo-maker2.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

7. If you are good with the slicing result, click **Save to Disk** or **Save to Removable Drive** to get the .gcode file. Save the file to the SD card.
8. Put the SD card into the printer, select **PRINT** --> select the file and the printer will start to print.
{{< video src = "poo-maker.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

### Outcome and some problems

The printing outcome was nice but the hinge part broken when I tried to assemble the models 😂😂😂
{{< postimg url = "poo-maker8.jpg" alt ="finished printing: the hinge structure was broken" imgw ="100%" >}}

I think the possible reason is because:

- The gaps to connect two parts are too small.
- The structure of hinge part are too thin and weak, so it could not handle when I put some force to combine two parts.
- I forgot to leave some space between the rotation part and the friction are too big to rotate smoothly.
{{< postimg url = "poo-maker3.jpg" alt ="the structure of hinge has some problems" imgw ="80%" >}}

I tried to fix it by sanding and gluing.

Fortunately it didn't affect the function of the snow maker, so I can still use it to make snow poos as many as I want.

{{< video src = "poo-maker5.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

{{< postimg url = "poo-maker4.jpg" alt ="make some snow poos in the campus" imgw ="80%" >}}

## LulzBot 3D Printer

There is a **LulzBot TAZ 6** and several **LulzBot Mini** in FabLab.
{{< postimg url = "lulzbot-09.jpg" alt ="LulzBot 3D printers in FabLab" imgw ="90%" >}}

TBC......

## Formlabs Form SLA 3D Printer

There is a **Forlabs Form 2** and a **Forlabs Form 3** in FabLab.
{{< postimg url = "slaprinter-08.jpg" alt ="Forlabs Form 3D printers in FabLab" imgw ="90%" >}}

TBC......
