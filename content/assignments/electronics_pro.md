---
title: "Electronics Production"
foldername: "electronics_pro"
image: "heroImg.jpg"
date: 2022-02-24
author: "Yuhan Tseng"
tags: ["PCB","Milling machine", "CopperCAM"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we learn how to use milling machine to make PCB and how to solder the little components on it.

## Goals

:point_right: Characterize the design rules for the PCB production process at the Aalto Fablab.

:point_right: Make an in-circuit programmer. Create tool paths for the milling machine, mill it and stuff (solder components) the board.

:point_right: Test the board and debug if needed.

## What is PCB?

I heard PCB many times before but don't fully understand what it is, so here are some notes about PCB and what we are making in this assignment.

PCB is a [**Printed circuit board**](https://en.wikipedia.org/wiki/Printed_circuit_board) which provides a good connection for electronics components with a designed circuit. We can attach the electronics components to it by soldiering. Using PCB can make the product and design more tidy without messy wires, also make the electronic function more stable.

## What are we making?

The PCB we are making is **Programmer**. I haven't really understand this part so take some note about what I understand so far:

- There are many programmers from Fablab Academy, you can find them [here](http://pub.fabcloud.io/programmers/summary/).

- Each programmer has a microcontroller on it, which is like the brain of that board and decides what the main function of the board.

- We need upload the program to microcontroller and make the board do what we want it do.

- Some microcontrollers can upload the program directly, and some need an additional board to upload the program.

- For example, the board we made in this assignment is **UPDI D11C** which can be "an additional board" to help us program other boards in the future.

## Milling Machine

There are two milling machine at the Aalto Fablab and have a little different steps to make a PCB.

### SRM-20 Milling Machine

{{< postimg url = "srm-20_machine.JPG" alt ="SRM-20" imgw ="60%" >}}

One milling machine is [Roland monoFab SRM-20](https://www.rolanddg.eu/en/products/3d/srm-20-small-milling-machine).
I failed twice and succeed at the third time 😂 Here is the process about how to use it to make a PCB and some notes:

**1.** Get PCB layout file: We can find the file from [Programmers Sumarry page](http://pub.fabcloud.io/programmers/summary/), and there are three files for the PCB's layout. We need the **front copper layer**.

{{< video src = "downloadFile.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

{{< postimg url = "PCBfiles.jpg" alt ="three files for different layers of a PCB" imgw ="100%" >}}

**2.** Get PCB milling file: Open [**CopperCAM**](<https://www.galaad.net/coppercam-eng.html>), which is a software to make PCB CNC milling file.

**(1)** File >> Close layer (to close the previous file)

**(2)** File >> Open >> New circit >> choose the front copper layer file: Programmer-UPDI-D11C-F_cu.gbr

**(3)** Confirm the hightlighted tracks (need some note about this)

**(4)** {{< horigrid src = "dimentionSet.jpg" alt ="dimention setting" imgw = "100%" >}}
    File >> Dimensions >> set the margin and Z thickness.</br></br>
    Z thickness is usually a little bigger than the thisckness of the board..</br></br>
    
    💡 Set the z thickness to 2.1 if you use <b>double-side board</b>, because the board is thicker.
    {{< /horigrid >}}

{{< postimg url = "fistFail.jpg" alt ="faild try: the cutting layer didn't cut through the board" imgw ="100%" >}}

**(5)** {{< horigrid src = "originSet.jpg" alt ="origin setting" imgw = "100%" >}}

    File >> Origin >> set the X and Y all as <span class="hightlight">0 mm</span>.</br></br>
    The origin is on the left-bottom of the board (a white cross mark).
    {{< /horigrid >}}


**(6)** Check the tools are set with the right value:

- Parameters >> **Tool library**
- The tool number is the order of the bits put in the toolkit box.
- We use No.2 and No.3 (or No.4, they are the same) for this time.
- Check the setting of the bits are the right value.
- Engraving bit: only move in **horizontal direction**.
- Drilling bit: only move in **vertical direction**.

{{< postimg url = "toolibrary.jpg" alt ="tool library: set different bits" imgw ="100%" >}}

**(7)** Choose the bits that we will use for this time:

- Parameters >> **Select tools**
- We use **Engraving tool** and **Cutting tool** for this time.

{{< postimg url = "activeTool.jpg" alt ="choose the bits we want to use" imgw ="100%" >}}

**(8)** Create tool path:

After creating tool path, we fond that there's a part which two lines are connected. This will cause the problem of cirtuit so we make the line thinner to seperate them.

- right press on the line >> Edit all identical tracks.
- decrease the width.

{{< video src = "makeToolpath.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

**(9)** Export the milling file:

- Click **Mill** button on the tool bar.
- We use **Engraving layer** and **Cutting layer** for this time.
- Set the XY origin as **White croll**.
- Set the Z origin as **Corcut surface**.
- Click **OK** and save the files.

{{< postimg url = "exportMilling.jpg" alt ="before exploring the file" imgw ="100%" >}}
{{< postimg url = "millingFiles.JPG" alt ="get two milling files" imgw ="80%" >}}

Now I had the file for the milling machine 🎉​ 🎉​ 🎉​
</br></br>

**3.** Open **Vpanel for SRM-20** software on the desktop of the computer. It's for setting the machine and sending the file to the machine.

{{< postimg url = "vpanel_ui.jpg" alt ="the interface of Vpanel" imgw ="90%" >}}

**4.** Upload the tool to the machine:

- Use the **hex key** that attached on the machine by magnet.
- There are two holes on the tool holder, but only one is available for the hex key.
- The first layer is **engraving layer** so we use tool No.2.
- Be careful to take and upload the bit because it's very easy to break its head point.

{{< postimg url = "uploadTool.jpg" alt ="upload the No.2 bit on the machine" imgw ="100%" >}}

**5. Put the copper board** {{< horigrid src = "putMaterial.JPG" alt ="past the copper board on the working platform" imgw = "100%" >}}

    Use double-side tap to fix the copper board on the working platform.</br></br>
    💡 It's better to use the tap on <span class="hightlight">the whole area</span> of the board than just partcial area, because it's easy to <span class="hightlight">cause the different height</span>.
    {{< /horigrid >}}

</br>

{{< postimg url = "secondFail.JPG" alt ="I think that's why I failed at the second try" imgw ="80%" >}}

**6. Set Z-origin** {{< horigrid src = "setZorigin.JPG" alt ="set the z-origin of the machine" imgw = "100%" >}}

    <li>Move the tool close to the board as close as posible by the Vpanel. But <span class="hightlight">don't touch the board at this moment.<span></li>

    <li>Use the hex key to move down the tool till it touches the board slightly.

    <li>Because the working process will move the tool lower than 0 z-aix, make sure ther's space for moving the tool downer than now.</li>

    <li>Press the Z button at the "To Origin" on the Vpanel.

    {{< /horigrid >}}

**7. Set the XY-origin**

- Move up a little bit of the tool.
- Use Vpanel to move the tool to the starting point on the material.
- Press the XY button at the "To Origin" on the Vpanel.

**8. Send the milling file to the machine and start to cut**

- Close the cover of the machine.
- Press **Cut** button on the Vpanel.
- **Delete All** previous files first >> **Add**: choose choose the file which is using tool No.2.

💡 If you can't see your file in the folder, try to change the file type to **All**.

- Press **Output**, then the machine will start to work!

{{< video src = "engraving.mp4" type = "video/webm" preload = "auto" videow = "60%" >}}

**9.** After finishing engraving layer, clean the dust on the board and change the tool to **tool No.3.**

💡 Press **View** button on the Vpanel can move the working platform forward automatically.

**10.** Repeat the step 6. to step 8. to run the cutting layer. But remember

- Skip the step 7. **Do not reset the XY-origin**.
- Choose the file that using tool No.3.

**11. Complete! and don't forget**

- Put the bit back to the toolkit box.
- Put the hex key back on the magnet.
- Take out the double-site tap on the left material.
- Clean up the machine and the environment.

{{< postimg url = "firstPCB.JPG" alt ="My first PCB" imgw ="60%" >}}

### MDX-40 Milling Machine

(TBC.....)

## Soldering

I have some experience of soldering before but I haven't soldered so tiny components. In the beginning, it was hard for me to put the components on the right position of the board but I feel I'm getting used to it😁

{{< postimg url = "solderStation.jpg" alt ="soldering place in FabLab" imgw ="100%" >}}

### How to solder components on PCB?

**1. Clean the board before soldering** 
    {{< horigrid src = "cleanPCB.JPG" alt ="use this to clean up PCB board" imgw = "50%" >}}
    Use this alcohol (it's usaully in the icebox) to clean up PCB board before soldering any components.</br></br>
    Also check the lines of circuit are seperated correctly. If there are some parts are connected, you can use the knife to take out the copper layer.
    {{< /horigrid >}}

**2.** Make the sponge wet and open the exhaust fan.

**3.** Start soldering! Here are some tips:

👍 Use a clip to hold the components.

👍 Put some solder on the board first, and then put the component on the board while heating the soldering point up.

👍 Start from the smaller components.

👍 Start from the components which is in the middle of the board.

👍 Use magnifier with the light while soldering.

**4. Complete!**

{{< postimg url = "finsihSolder.JPG" alt ="fist completed PCB" imgw ="70%" >}}

## Testing PCB

TBC......

### Circuit Test

TBC......

### Program Test

TBC......
