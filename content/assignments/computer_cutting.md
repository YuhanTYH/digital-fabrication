---
title: "Computer-Controlled Cutting"
foldername: "computer_cutting"
image: "heroImg.jpg"
date: 2022-02-16
author: "Yuhan Tseng"
tags: ["Laser cutting","2D design", "Vinly cutter"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we learned how to use laser cutter and vinyl cutter in Fablab.

## Goal

:point_right: Be familiar with the laser cutter and try different setting. Find the kerf and know how to apply the kerf into my design.

:point_right: Be familiar with the vinyl cutter and try different setting.

:point_right: Make something that is related to the final project by these machines.

## Laser Cutter

{{< postimg url = "laser_machine.jpeg" alt ="laser cutter" imgw ="60%" >}}

We use [Epilog Fusion Pro 48 Laser Machine](https://www.epiloglaser.com/laser-machines/fusion-pro-laser-series.htm).

- Drawing software: [Adobe Illustrator](https://www.adobe.com/fi/products/illustrator.html)
- Laser cutting software: [Epilog Laser Job Manager](https://www.epiloglaser.com/laser-machines/epilog-job-manager.htm)

{{< postimg url = "info_table.jpg" alt ="laser cutter basic information" imgw ="90%" >}}

### How to use

**1.** Scan the card (which is next to the computer) to the detector to turn on it.

    People detector will make beep sound and need someone to turn off the sound by pressing button :white_check_mark:. The purpose is to make sure that someone is watching the machine whien it's runninng because sometimes the material causes fire.

**2.** Rotate the key to turn on the machine.

{{< postimg url = "laser_process1.jpg" alt ="laser cutting step 1 & 2" imgw ="100%" >}}

**3.** Put the material into the machine. Put down the cover slowly and carefully.

**4.** Open Illustrator to make and check the images.

- File color mode: RGB
- Cutting images or lines need to be **stroke 0.01 pt** and **no fill**.
- Engraving images need to be **no stroke** and **filled with color**.
- You can use **different colors** so the different parts of an image can apply different settings for cutting or engraving.

**5.** File >> Print >> Print

- Printer: Epilog Engraver
- Media Size: Custom
- uncheck "Auto-Rotate"

**6.** Then the software for laser cutter will open:

There is a camera view of the material. Move the image to where you want to cut on the material.

{{< horigrid src = "laser_setting.jpeg" alt ="laser setting" imgw = "100%" >}}

    <li>At the default, the images <span class="hightlight">only have a stroke and no fill will be set as cutting</span>, and the images <span class="hightlight">only have fill without stroke will be set as engraving</span>.</li>
    <li>Auto Focus: <b>Plunger</b> (detect the thickness of the material automatically.)</li>
    <li>Import the material setting by clicking the right-top folder sign.</li>
    <li> <b>Slower speed</b> or <b>stronger power</b> can cause <b>deeper cut</b>.</li>
    <li> <b>Frequency</b> is how many laser pulses be emitted in one second. For example, you need higher temperature to get a smooth edge of acrylic, then you need higher frequency.</li>

{{< /horigrid >}}

</br>

💡 **How to find the better setting?**

   - Import the material setting from the sofware.
   - There is a manual of the machine, you can find the recommanded setting of different materials on it.
   - Take a look for the [**priivious laser cutter setting**](https://wiki.aalto.fi/display/AF/Laser+Cutter+Settings) in FabLab. (For the previous machine, the 100% frequency is 5000 so for example when frequency = 500 then the value in new machine is **10%**.)    
   - Use the settings above as the start point and adjust them.

💡 There is a range with pink dotted line on the camera view (the material outside this range is covered by white dots). Make sure the cutting image is inside this range or the image will not be process.
   {{< postimg url = "cutting_range.jpg" alt ="be careful to the cutting range" imgw ="90%" >}}

💡 **Arrange the order of cutting**: (see the video below)
  - When you use different color in Illustrator, you can make different parts with different setting. Click <span class="hightlight">Split by: Color</span>, then the seperated tasks will be created.
  - The cutting order is <span class="hightlight">from the top to the bottom</span> of the process list, drag the task to arrange the cutting order.

   {{< video src = "cutting_order.mp4" type = "video/webm" preload = "auto" videow = "40%" >}}

**7.** Click the right-bottom button to send the file to the Job list. You can still edit and save the setting in Job list. >> Right-top button **Quick Print**. The cutting file will be sent to the machine.

**8.** Select the file on the screen of the machine >> Press **▶❙❙** button to start cutting.

**9.** Wait a while after the cutting is finished and open the cover to take out the material.

**10.** Turn off the laser cutter: Rotate the machine's key to "0" and press :x: button of the people detector.

### Testing

- The default setting for 3mm plywood seems to be too strong so the scorch marks are obvious. I tried to increase the speed and decrease the power also decreased the frequency.

- Using **Plunger** to get the focus distance automatically is better than inputing the thickness manually.

{{< postimg url = "lasertesting.jpg" alt ="test different settings with 3mm plywood" imgw ="90%" >}}

- Use different colors of the strokes: red line is cutting but didn't cut through the plywood, black line is the normal cutting.

{{< postimg url = "lasertesting2.jpg" alt ="test different settings with 3mm plywood" imgw ="90%" >}}

- Try with 3mm acrylic. Use different colors to seperate the proccess. The kerf of different materials with different setting may be different.

{{< postimg url = "lasertesting3.jpg" alt ="test different settings with 3mm acrylic" imgw ="80%" >}}

## Vinyl Cutter

{{< postimg url = "vinylcutter.jpeg" alt ="vinyl cutter" imgw ="60%" >}}

We use [Roland GX-24 Vinyl Cutter](https://www.rolanddga.com/support/products/cutting/camm-1-gx-24-24-vinyl-cutter).

- Drawing software: Adobe Illustrator
- Vinyl cutting software: Extension in Illustrator

**1.** Adjust the position of two wheels. The wheels need to be at the range of **white mark**.

**2.** Put in the material. The material needs to cover the light sensors.

{{< postimg url = "lightsensor.jpg" alt ="light sensors on the vinyl cuttor" imgw ="80%" >}}

**3.** There are 3 modes:

- ROLL: When the material is very big and you input the material from the back side.
- PIECE: When the material is smaller. The machine measures the width and height of the material.
- EDGE: When you only need a little part of a longer piece of the material. The machine only measures the width of the material.

    Choose the second one to get the size of the artboard.

     {{< video src = "measure_material.mp4" type = "video/webm" preload = "auto" videow = "40%" >}}

**4.** Open Illustrator to make and check the images.

- Set the size of the artboard based on the measuring result of the step 3.
- Use **Roland CutStudio** to adjust where you want to cut on the material.
- You can open **Roland CutStudio** panel from: Window >> Extension >> Roland CutStudio
- The cutting area need to have outlines.
- Model: GX-24
- Uncheck **Move to Origin** to decide the cutting position freely or the image will always be cut from the left-bottom (the default origin) of the material.

{{< postimg url = "artboard.jpg" alt ="set the cutting position" imgw ="80%" >}}

**5.** Click the printing button next to the model >> Cut

**6.** After finishing cutting, click **Menu** >> **unset up** >> enter to move the cutting head away from the material. Take out the material.

**7.** You can use transfer tap to make the sticker easier to tap.

{{< postimg url = "sticker.jpg" alt ="use the transfer tap to stick the material" imgw ="100%" >}}

## Application: Testing for the final project

With the [Strandbeest's leg](https://yuhantyh.gitlab.io/digital-fabrication/assignments/computer_aided_design/#what-i-tried) that I tried with 3D model in the software last week, I want to make a 4-legs version with laser cutter to understand how it works. Also, I'm interested in the flexible circuit that made with copper sticker and vinyl cutter so I plan to make a simple circuit on the transparent acrylic.

{{< postimg url = "skatch.jpg" alt ="idea about the testing structure" imgw ="60%" >}}

### Legs and body

I made a 3D model in Fusion 360 first and convered the pieces to .dxf files.

{{< video src = "4legs_model.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}
{{< postimg url = "2d_file.jpg" alt ="inport .dxf files into illustrator" imgw ="80%" >}}

I learned how to make the gear in Fusion 360 by this video:

{{< youtube B8A_11o7QZ0 "16:9" "70">}}

💡 **Simplify the path** 

When I fist time sent the file from illustrator to the machine, it took very long time even stopped the application. It's because the path of the gears **was combined with lots of dots** that made the file very big and the programme complicated.

Use **Simplify** function to make the dots into lines
{{< video src = "simplyPath.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

</br>

#### Outcome

{{< postimg url = "cuttingPart.jpeg" alt ="the parts of the legs and the body" imgw ="60%" >}}
{{< postimg url = "twoLegs.jpeg" alt ="assemble them with the bamboo sticks" imgw ="60%" >}}
{{< postimg url = "legs_body.jpeg" alt ="whole body and legs with motor" imgw ="60%" >}}

### Cat's head

Trying to stick the copper circuit on a transparent acrylic. I used [this website](http://www.falstad.com/circuit/) to simulate a simple LED circuit with the switch.

{{< postimg url = "catHead.jpg" alt ="draw the cutting shape based on the circuit and also the head part for acrylic" imgw ="70%" >}}

At the beginning, I cut the copper sticker directly and found that the knife dragged out the copper layer while cutting.
{{< video src = "copperfail.mp4" type = "video/webm" preload = "auto" videow = "50%" >}}

It's because the sticky part of the copper sticker is not strong enough. Due to the incresing of the material's thickness, I need to:

- Transfer the copper sticker to the 3M transparent transfer tap (not sure the name) first.
- Increase the cutting force (80 -> 100).
- Reduce the cutting speed (4cm/s -> 1cm/s).

{{< postimg url = "transcopper.jpeg" alt ="better result: stick the copper on the transfer tap first and then cut it by the machine" imgw ="70%" >}}

{{< postimg url = "circuitHead.jpg" alt ="past the circuit on the acrylic and solder the LEDs and resistor on the circuit" imgw ="70%" >}}

{{< video src = "ledTesting.mp4" type = "video/webm" preload = "auto" videow = "30%" >}}

</br>

## Final Outcome

Assemble all the parts together. The moving structure is failed because it can not walk on the ground :joy::joy::joy:

{{< youtube ZjvGOK4Gh-4 "16:9" "80">}}

{{< youtube Hobuq0o21iE "16:9" "80">}}

I think there're some possible reasons:

- The total weight is too heavy and the motor is not strong enough. Maybe I can try 2:1 ratio for the gears to reduce the rotation speed and increace the force?

- Stucture with four legs is not stable enough? The difference between the height of two sides is too big.
