---
title: "Computer-Aided Design"
foldername: "computer_aided_design"
image: "heroImg.jpg"
date: 2022-02-09
author: "Yuhan Tseng"
tags: ["2D design", "3D model"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we tried different 2D & 3D softwares and to explore how they can help my final project.

## Goal

👉 Explore one or more 2D vector graphics tools.

👉 Explore one or more 2D raster graphics tools.

👉 Explore one or more 3D modelling tools.

I have some experience about photoshop and illustrator for raster graphics and vector graphics, so I want to learn and explore more about 3D software. [Part of my final project](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note1/) is about moving structure, I hope to make some **simulations of the movement** in 3D software so the making process may be more efficient and also waste fewer materials to test.

## Autodesk Fusion 360

I knew Fusion 360 in a course in the first period and made some simple boxs by it. I realized there're some great fearures of it:

- You can set **parameters** in your model.
- It can recode your making history and easily go back to specific step to modify your design.
- It's not only for 3D models but also can export 2D files for some cutting process.

### Basic Operation

Try to get familiar again with the operating environment with this tutorial (part1~part3):

{{< youtube A5bc9c3S12g "16:9" "60">}}

**Viewpoint Control (Mac)**

- Zoom in/out: wheel of the mouse.
- Move the viewpoint: press the wheel and move the mouse.
- Rotate the viewpoint: Shift + press the wheel and move the mouse.

And I found I am always confused with the layers, components, and bodies in the Fusion 360, also I knew that there're a joint funciton in Fusion 360 from this tutorial and wanted to learn more about that. Here are the main concepts I learn from this time:

### Component v.s. Body

- Component and body are **two different things**, body is only about the 3D object; component can include sketches, bodies, and other elements, **also has its own origin**, it is more like a folder.

{{< postimg url = "com_body.jpg" alt ="the differences between component and body" imgw ="70%" >}}

- A body (3D) usually start from a sketch (2D).
- Multiple components put into one component => Assembly component.
- Here is a [article](https://www.autodesk.com/products/fusion-360/blog/components-vs-bodies-tips-fusion-360/) and video talk about this topic:

{{< youtube TzG2deElWqI "16:9" "60">}}

### Assemble and Joint Function

- Joint function can **only be used on the components**.
- Joint function can help you align different components to a specific face or point.
- **AS-BUILT-JOINT** function can help you connect different components with different types of joints.
- I learn a lot about from this video:
{{< youtube CzzCNVCzXK0 "16:9" "60">}}

{{< postimg url = "jointFunction.jpg" alt ="Joint function on the tool bar" imgw ="90%" >}}

### What I tried

Accoding to the [refernces](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note1/) I found for my final project, I tried to make a spider robot leg and a strandbeest's leg.

#### Spider robot leg

The sctucture is more complicated than I thought😂 so I just tried to make a [similar structure](https://www.thingiverse.com/thing:1009659) I found on the Internet. I found the [servo model](https://grabcad.com/library/useful-robotics-parts-1) (there're also other components' model which are very helpful) from the Internet, too.

{{< postimg url = "leg_rotate.gif" alt ="one robot spider leg" imgw ="70%" >}}

Here's the making process (speed up 4x), I think I can reduce some steps🤔
{{< video src = "spiderLegProcess.mp4" type = "video/webm" preload = "auto" >}}

The most confused part for me is that you need to make a kind of rotatin aix piece for the servo body:

{{< postimg url = "rotation_axis.jpg" alt ="a additional connection part for servo or motor" imgw ="80%" >}}

I tried to set some parameters so I can adjust the length of the leg very conveniently, but it's not working so I think some of my steps didn't connect together? 🤔
{{< video src = "parameterError.mp4" type = "video/webm" preload = "auto" >}}

And also each part that is combined with the servo is a little different although I thought there is just the same structure at the beginning.

{{< postimg url = "leg_parts.gif" alt ="Different parts in one leg" imgw ="70%" >}}

The most exited part for me is appling joint function to get more sense about how the leg moves!
How to set the movement:

- Ground a based component which can't be moved anymore.
- Connect each component with **Joint function** (Shortcut: j).
- Use **AS-BUILT-JOINT function** to set the type of the connection and see the simulated animation by this function.

{{< postimg url = "leg_joint_move.gif" alt ="Different parts in one leg" imgw ="70%" >}}

For the last part of the leg, it is my first time to use Fusion 360 to make some organic shape. I found that I can use **Fit Point Splines** in sketch to make it. (though I havn't catched the point how to use it and think it's hard to draw a shape🙈)

{{< postimg url = "FitPointSplines.jpg" alt ="fit point splines to draw curve line" imgw ="80%" >}}

#### Strandbeest's Leg

I'm very like the movement of this kind of linkage stucture. I basically followed [**this ratio**](https://yuhantyh.gitlab.io/digital-fabrication/img/final-project/Jansen-linkage-structure.gif) of the each stick.

Here is the making process (speed up 4x):
{{< video src = "linkageMake.mp4" type = "video/webm" preload = "auto" >}}

I found that I need to be careful about the different layers (height) of each stick if I make by like laser cutting.
{{< postimg url = "extralayer.jpg" alt ="In 3D view" imgw ="80%" >}}

And I tried the joint function to assemble these sticks.When I rotate the pink one, it looked like:
{{< postimg url = "linkage_run.gif" alt ="try to run a leg in Fusion 360" imgw ="80%" >}}

## Reflection

I spent more time than I thougt to learn Fusion 360. The start was hard but now I feel that I start realizing the convinence of Fusion 360 and how powerful it is. I actually just made some structures that othre's have done before so I will start trying to make some my own structure.

There some problems I may have:

- I think I'll make a model with bigger size and also bigger motors (or servers), then the structure will be different.
- I can silulate the movement, but how about the weight and force?
- How to set parameters more properly so my procee can be more efficient?

Also I am interested in FreeCAD and Blender, so I will keep exploring different ways to make 3D models in the future. 👾👾👾👾👾👾
