---
title: "Wildcard Week"
foldername: "wildcard"
image: "heroImg.jpg"
date: 2022-05-18
author: "Yuhan Tseng"
tags: ["EDM","metal cutting"]
categories: ["NOTE"]
draft: false
---
{{< content >}}

This week everyone could choose one bigger machine that is not in Aalto FabLab. We had options of water cutter, Zund cutter, wire EDM machine, and vacuum forming machine. I had no idea which one could be more helpful to my final project in the beginning, and Kris recommended that I could try with **wire EDM machine**, which is able to cut some very accurate pieces with metal.

## Goal

:point_right:  Choose one of the proposed digital manufacturing technologies.

:point_right:  Create a digital design to be manufactured with the technology/machine of your choice.

:point_right:  Participate in one of the introduction sessions.

:point_right:  Work together with the workshop master and document the process.


## About EDM Machine

{{< postimg url = "EDMmachine.jpg" alt ="the wire EDM machine I used this week" imgw ="80%" >}}

**Electrical discharge macining (EDM)** is also called spark machining, spark keroding, die sinking, wire buring, or wire erosion, because it cuts the material by **electric spark without touching the material**.

### How It Works

I didn't know about EDM machine before so this was so new and interesting thing for me. I tried to find some introduction video to understand how the machine works because it's hard to understand it only by texts. This video gave me a basic concept about how EDM machine works:

[**The introduction from Wiki**](https://en.wikipedia.org/wiki/Electrical_discharge_machining)

{{< youtube kh4DSOtef4k "16:9" "80">}}

The difference of the machine I used and this video is that, I used **wire EDM** which uses a very thin wire as tool and moves the wire to cut the workpiece.

{{< video src = "edm_cut.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

### ROBOFIL 440

There are two wire EDM machines in Aalto's workshop: [**ROBOFIL 4400**](http://www.cfsusa.net/detail/shop/Shop-Capacity/CNC/ROBOFIL%20X40%20CC%20US.pdf) and [**Brother HS-70A**](https://machinetool.global.brother/en-ap/hs70a/index.aspx), I used the former one in this week. We can not operate it by ourselves because the machine is related complicated and big, but there is a master who can help us and give us a better solution about what we want to do with the material and the machine.

#### Automatic Threading

I'm not sure if every EDM machine has this function, and one special feature of these two EDM machines for me is **automatic threading**. The metal wire sometimes needs to start somewhene in the middel of the material, so we can make a little hole on the material in advance and the EDM machine can rethread the wire from that hole, cutting from the middle of the material.

Because the piece I need is small so the hole for the wire is also small, which caused this threading process harder 😅​ The automatic threading failed many times during this week. However, we can always thread the wire manually.

This is a video that which the machine trying to thread the wire by itself. The water jet helps the wire go straight. You can see a very thin wire came out from the top and didn't go through the hole:

{{< video src = "auto_threading.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

</br>

Threading the wire mannually:

{{< video src = "manual_threading.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

#### Various Cutting Angles

ROBOFIL EDM machine can cut the workpiece between -45 degrees to 45 degrees, and the maximum height is 20 inches (= 50.8 cm). So you can make an irregular 3D shape with this machine.

{{< postimg url = "example-02.jpg" alt ="some example pieces cut by EDM machine, the right one was cut with an angle so the puzzle will not fall down" imgw ="80%" >}}


### Pro and Con

Although EDM machines can proceed with some harder materials with very accurate cutting, there are still some limits to this technique.

#### Pro:
    
- The losing material after cutting is very small.
- The material will not vibrate or be pushed by the tool during the cutting because the tool (wire) doesn't touch the material.
- It can produce a complicated shape that is hard to make by the conventional machining process.
- It can create a good finished surface.

#### Con:

- The material needs to be **metallic or conductive**.
- The process consumes lots of power.
- The cutting wire is disposable so it also consumes lots of wire.
- The process is very very slow. 😂​ 😂​ 😂​



## What I made

Last week I tried out a [**moving mechanism**](http://localhost:1313/digital-fabrication/final-project/final-project-note5/) that seemed to work fine, but still has some issues that make the movement not really stable and smooth. So maybe using metal and making it with a more precise size would be helpful.

At first, I was thinking of making the moving stick, spinning plate, and the front frame (which has the track of the stick) by EDM. I discussed it with the master and Kris and found that it was a little tricky about the part of the track.

{{< postimg url = "cuttingidea-02.jpg" alt ="discussing how to make the pieces" imgw ="100%" >}}

Then the master came out a clever idea that using less pieces and the making process was also easier! I tried to make a new model in Fusion360 and tested it with the 3D printing first.

{{< postimg url = "newidea-03.jpg" alt ="a cool idea from the master" imgw ="100%" >}}

It looked promising. The 3D printing was a little too soft and made the movement not really stable, and I think it could be improved by the metal material.

{{< video src = "newidea_test.mp4" type = "video/webm" preload = "auto" videow = "30%" >}}


### Testing Piece

After the first visit of the workshop and tested the new machnism with 3D printing, I went there again and started to cut the testing pieces.

Because my pieces is 2D shape so I can use **.dxf file**, however the master told me that the dxf file sometime have issue about the continuity of the outline, so it's better alway has a **.step file** that can be used for 2D and 3D design.

These parts were what I planed to make by EDM:

Actually I have not fully understand the making process, and here was the making process that I could remember:

1. Import the file into the toolpath software of the EDM machine.

{{< postimg url = "cutting_piece.jpg" alt ="the parts I planned to make by EDM" imgw ="80%" >}}

2. The master makes sure the cutting lines are continuous and put the shape in the right center.
3. Adding the starting hole for cutting the parts that are in the middle of the material, and remember the position of the hole.
4. Mark the range of the whole piece and the position of the holes on the material, drilling the hole by the drilling machine first.

{{< postimg url = "mark_material.jpg" alt ="draw some marks on the material and try to use the material efficiently" imgw ="60%" >}}

5. Because I will use screw at the upper hole of the stick to connect the surface of my final project, the master help me to make a thread inside the hole.

{{< video src = "making_thread.mp4" type = "video/webm" preload = "auto" videow = "60%" >}}

6. After finishing the preparation of the material, the master starts to prepare the cutting program, which is written in **G-code**. The master first decides the cutting direction and some setting by the software's interface and generate the program. Normally each line is a command to the machine, for example:

- G0 G40 + the position of X & Y means moving the machine to this positions as the starting point.
- G92 + the position of X & Y means threading the wire at this position.
- S501 is the first time cutting which is for roughing.

...and so on.

So the master can design how the machine work with the material by arraging these G-code. One complete toolpath will be saved as one file, and we can apply multiple files on one material by setting on the machine.

The file is save on a floppy disk and also the machine read the file from the floppy disk which is so cool for me ​😆​

{{< postimg url = "g_code.jpg" alt ="the master adjusts the g-code to design the cutting process" imgw ="70%" >}}

7. Fix the material in the machine, measuring the original point.

{{< postimg url = "put_material.jpg" alt ="fix the material in the working area" imgw ="60%" >}}

8. After importing the G-code and doing some setting on the machine, we can start to cut!

Cutting out process:

{{< video src = "startCut.mp4" type = "video/webm" preload = "auto" videow = "60%" >}}

The cutting process was very slow, and because the holes were too small for automatic threading, the master needed to thread the wire everytime before the machine started to a new cutting process.

{{< postimg url = "finish-1.jpg" alt ="the first part finished" imgw ="60%" >}}

And then we did the similar process for the second part. Finally we finished the two parts and tried on the mechnism. After some adjustments on the 3D printing stucture, it looked nice:

{{< youtube CNaW3u80_ac "16:9" "80">}}


### Cut for my final project

Now I knew what parts I wanted to make by EDM. Because I plan to make the same stucture with 16 motors, I need 16 pieces for each part. The master said we could make a stack of material so we only need one cutting process.

#### Prepare the material

First the master cut the big material into little pieces. For the T-shaped part, we planned to cut two pieces in one rectangle, so we needed 8 rectangular material.

{{< postimg url = "materialPrepare-1.jpg" alt ="cut the big material into little pieces to make a stack" imgw ="70%" >}}

After checking the position of the holes and the original point, the master drilled the holes by the drilling machine. It's the same process as we made testing pieces but we made with a stack of the material for this time.

{{< postimg url = "materialPrepare-2.jpg" alt ="make holes and thread in a stack of the material" imgw ="60%" >}}

About making the spinning plate, it was a little tricky because we didn't find much material. After checking the minimal size for each piece, the master cut the material into a small ones to make a stack.

{{< postimg url = "materialPrepare-3.jpg" alt ="mark the center area on each piece so every center hole inside this area can get enough space for the spinning plate" imgw ="60%" >}}

{{< postimg url = "materialPrepare-4.jpg" alt ="make the center hole and other holes for cutting the middle part" imgw ="60%" >}}



#### Start cutting

**Cut T-shape part**

{{< postimg url = "cut_Tpart.jpg" alt ="set the material in the machine" imgw ="80%" >}}

{{< postimg url = "cut_Tpart-2.jpg" alt ="cutting in progress" imgw ="80%" >}}

Finished!!
{{< postimg url = "cut_Tpart-3.jpg" alt ="T-shape part finished" imgw ="80%" >}}

**Cut spinning plate**

{{< postimg url = "cut_plate.jpg" alt ="spinning plate finished" imgw ="80%" >}}

{{< postimg url = "cut_plate2.jpg" alt ="insert the spinning plates on the motors" imgw ="80%" >}}



