---
title: "Computer-Controlled Machine"
foldername: "cnc"
image: "scooter_finish.jpg"
date: 2022-03-16
author: "Yuhan Tseng"
tags: ["2D","Milling"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we learned how to use a huge CNC machine in FabLab. This is my first time using such a big machine and making something in almost meter scale, so I was excited but also felt so nervous when I worked with the machine 😂

## Goal

:point_right: Participate one of the introduction sessions or watch instruction videos.

:point_right: Pay attention to what runout, alignment, feedrate and tool paths are.

:point_right: Design, CNC mill and assemble something big (meter scale). You will get a 1200x1200x18 mm sheet of plywood for your assignment. Make use of it.

## What is CNC?

CNC is the abbreviation of **Computer Numerical Control** which means you can use digital data to control, automate, and monitor how the tool of the machine works. For example, laser cutter, milling maching, 3D printer, and so on, all are CNC machine.

### Data File for CNC

Data file contains commands and settings to tell machine how to run the tool.

##### G-code

G-code is a bunch of position commands of tool path to tell a machine where (**X, Y, Z position**) to go at every unit time.

{{< postimg url = "running_toolpath.jpg" alt ="left side is a running G-code" imgw ="90%" >}}

##### Feedrate

Feedrate is **feed rate** of the tool, which means how much(mm) the tool engages in the material every minute. It is a important setting of CNC machine, here is how to calculate it:

<p style="font-size:2rem"><b>Feedrate = {{< hightlight color = "#FFFF00">}} N x C x S{{< /hightlight >}}</b></p>

- **N** = Number of flutes. Flute is the part of tool that takes out the material. Different flute number can be used for differnt type of materials and purposed.
- **C** = Chip load(mm), how much material will be taken out by each flute per revolution
- **S** =Spindle speed(rpm)

{{< postimg url = "flute.jpg" alt ="tools with different numbers of flutes" imgw ="90%" >}}

[**Here**](https://www.sorotec.de/webshop/Datenblaetter/fraeser/schnittwerte_en.pdf) is the parameter setting guide from the manufacturer.
{{< postimg url = "chipload.jpg" alt ="chip load table" imgw ="80%" >}}

{{< postimg url = "spindle_speed.jpg" alt ="you usually can find the max spindle speed from the machine's description" imgw ="90%" >}}

## Recontech 1312

{{< postimg url = "cnc_machie.JPG" alt ="Recontech 1312 in Aalto Fablab" imgw ="60%" >}}

There is a Recontecn 1312 in Aalto FabLab, which is a big and one of the most dangeour machine in FabLab. It can only be used for **wood or other softer material**.

## How To Use?

Here is the basic workflow of using CNC machine. There are various softwares can be used for each steps and here are the softwares I used for this assignment.

{{< postimg url = "basic_workflow.jpg" alt ="the workflow of using CNC milling machine" imgw ="90%" >}}

### Making a 3D Model

I used Fusion 360 to make a 3D model for my design and export the sketch into {{< hightlight color = "#FFFF00">}}.dxf file{{< /hightlight >}}. For the first time, my goal was to be familiar with the process of using Recontech 1312 and find some size setting for the cutting pieces, so I made some testing pieces to cut.

{{< postimg url = "testing.jpg" alt ="the 3D model for testing" imgw ="80%" >}}

{{< postimg url = "testing2.jpg" alt ="use some parameter settings to make the model adjustable" imgw ="80%" >}}

### Creating Tool Path

In the beginning, I tried to generate a tool path by Fusion 360 but got confused with some settings, so I changed to use [**VCarve**](https://www.vectric.com/products/vcarve-pro).

**1.** Open VCarve
{{< horigrid src = "job_setup.JPG" alt ="job setting" imgw = "50%" >}}
    <li>Create a new file.</li>
    <li>Set up the job:</li>
        <ul>
            <li>Job Type: Single Sided, we only used one side cutting and milling for this time.</li>
            <li>Job Size: The size of material.
            <li>Z Zero Position: Where you set the Z-origin. For the testing pieces, I could fix the material only by vaccuma table, so I set the Z-origin on <b>Machine Bed</b>.
            <li>XY Datum Position: Where you set the XY-origin. For the basic operation, don't use offset.</li>
        </ul>

{{< /horigrid >}}

**2.** Create vector images of working pieces with the tool on the left panel, or you can import a vector file. I imported the .def file I export from fusion 360.

{{< postimg url = "import_sketch.JPG" alt ="import vector images I created in advance" imgw ="80%" >}}

**3.** Deal with little corners
{{< horigrid src = "dogbone-02.jpg" alt ="set dogbone filite" imgw = "100%" >}}
Because the smallest working part depends on the tool's size, some corners can not be processed completed. Sometimes this will affect your design, like the joints of the pieces can't combine well together.</br>

<li>We can use <span style="background-color:#FFFF00">Create Fillets</span> to do some little extra cutting to avoid this problem.</li>

<li>Set the Fillet/ Tool Radius: <b>Radius of the tool + 0.1</b> is safer.</li>

<li>There are different types of fillets.</li>
{{< /horigrid >}}

**4.** Set toolpaths
{{< horigrid src = "setToolpath.jpg" alt ="tool path setting" imgw = "100%" >}}

<li>Open the Toolpaths pannel on the top-right side of the interface.</li>
<li>Select the area in the sketch that you want to apply the tool path.</li>
<li>For this time, we mainly use <b>2D Profile Toolpath</b> and <b>Pocket Toolpath</b>.
{{< /horigrid >}}
{{< horigrid src = "profile_toolpath.JPG" alt ="profile toolpath setting" imgw = "60%" >}}
<b>2D Profile Toolpath</b></br></br>
    <li>2D profile toolpath is for cutting piece.</li>
    <li>Select tool: I used 6 mm tool for this time.</li>
        <ul>
            <li>Start Depth is always 0.0mm</li>
            <li>Cut Depth: material thickness + 0.2(or more, because sometimes the material's surface is not completely flat or the material is bended.)
            <li>End Mill (6mm)</li>
            <li>Pass Depth: diameter/2 = 3mm</li>
            <li>Stepover: 40%</li>
            <li>Spindle Speed: usually don't use the max speed so we used 16000 here</li>
            <li>Feed Rate: 2 x 0.06 x 16000 = 1920</li>
            <li>Plunge Rate: feedrate/4 = 480</li>
        </ul>
    <li>Machine Vectors:
        </br></br>
        <ul>
            <li>Outside/ Right: Cut outside the shape.</li>
            <li>Inside/ Left: Cut inside shape. </li>
            <li>Climb: Take away less material.</li>
            <li>Conventinal: Take away more material.</li>
        </ul>
    </li>
    <li>Remember to <span style="background-color:#FFFF00"><b>add tabs to toolpath</b></span>  to avoid the pieces moving when milling.</li>
{{< /horigrid >}}
{{< tipblock >}}
    Be careful about the Outside and Inside setting. If you use outside for the hole, the hole's size will be added by the tool's size that is much bigger than the original design. So I seperated the holes and the outline cutting in different toolpath job.
{{< / tipblock>}}
{{< horigrid src = "pocket_toolpath.JPG" alt ="pocket toolpath setting" imgw = "60%" >}}
    <b>Pocket Toolpath</b></br></br>
        <li>Start Depth is always 0.0mm</li>
        <li>Cut Depth is like how deep you want to engrave.</li>
        <li>I used the same tool as profile toolpath.</li>
{{< /horigrid >}}

**5.** Export toolpath file

After calculting and creating the toolpaths, you can preview how the tool will work for each toolpath. If everything is fine:
    
{{< video src = "preview.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}
    
- I chose all the toolpaths jobs because I used the same tool.
- **Save Toolpath**
- Check **output all visable toolpaths to one file** to combine all the toolpaths together.
- Post Processor: Mach2/3 Arcs(mm)(*.txt)

Then you can get the toolpath file!

### Using Recontech 1312

**1.** Open Recontech 1312 (main power -> green turn on button) and control software **Mach3**.

    {{< postimg url = "openmachine.JPG" alt ="operating buttons" imgw ="70%" >}}

**2.** In the Mach3:

- **Reset** the application to start a new setting.
- **REF ALL HOME** to move the tool back to the default origin. Be careful the moving tool.
- Make sure turn on **Soft Limits** (there is a green outline if you turn on).
- Now we can control the tool movement by the keyboard. **Make sure there's NO other people outside the room and touch the keyboard of the laptop because it will move the tool too.**

{{< postimg url = "mach3.jpg" alt ="setting on Mach3" imgw ="70%" >}}

**3.** Install the tool
{{< horigrid src = "install_tool1.jpg" alt ="step 1" imgw = "100%" >}}

<li>Move the red protection cover to the left side.</li>

<li>Put the big sponge cube under the tool holder to protect the tool.</li>

<li>Take out the nut of the tool holder.</li>

{{< /horigrid >}}
{{< horigrid src = "install_tool2.JPG" alt ="step 2" imgw = "100%" >}}

<li>Choose a tool and pick the collet with right size.</li>

<li>There is a tool table on the wall, you can check information about the tool you picked.</li>

<li>Install the tool by the two wrench. The deep of inserting tool is about 1.5 cm</li>
</br>

{{< postimg url = "install_tool3-04.jpg" alt ="install tool" imgw ="100%" >}}

{{< /horigrid >}}

{{< tipblock >}}
    Although Kris tells us don't screw the tool too tight, the tool fell down during the running process happened twice for me and I broke a tool 😢 💔 I guess is because my strength is too small so I always remind myself to screw the tool tigher after that.
    </br>
    </br>
    {{< postimg url = "broken_tool-05.jpg" alt ="broken tool" imgw ="60%" >}}
{{< / tipblock>}}

**4.** Set up the spoil board and vaccum bed:

- The main concept of the vacuum bed is using air to absorb and fix the material on the working area, so the material won't move when the tool is running.
- There are two side of the vacuum bed, make sure you turn on the right side.
- Make sure you take out the cap of the air hole.
- Using rubber band to limit the air area that can make the force more concentrate. The arrangement of rubber bands affects a lot! Limit the air in the area of the materialm, not the spoil board.
- The spoil board can protect vaccum bed to be demaged by the tool. To make the air go through the spoil board, we usually use something like MDF board.
- Checking: Move the spoil board by hand and check if the air pressure is in green area.
- Put on earpotector because the sound of vaccum bed is very loud.

{{< postimg url = "vaccum_bed-03.jpg" alt ="setting the vaccum bed" imgw ="90%" >}}

**5.** Set the Z-origin

- Put the sensor on the spoil board.
- Move the tool on the sensor but keep some space between the tool and sensor.
- Press "Terän mittaus" and the tool will go down to set the z-origin automatically.

{{< postimg url = "z-oring-06.jpg" alt ="setting the z-origin" imgw ="90%" >}}

**6.** Put on the material
 - Turn on the vaccum bed to make sure it attaches on the spoil board well.
 - If the material is movable, repeate the step of setting vaccum bed.
 - You also can use screws and clamps to fix the material on the working bed, and **make sure the running area of the tool will not hit the screws and clamps**.
 - If you use the screws or clamps to fix the material, you can set the z-origin on the materail, and {{< hightlight color = "#FFFF00">}}make sure seting of <a href="https://yuhantyh.gitlab.io/digital-fabrication/assignments/cnc/cnc/#creating-tool-path">Z Zero Position</a> of the toolpath file is set to "Material Surface".{{< /hightlight>}}

**7.** Set the XY-origin

- Move the tool to the origin point and press "X" and "Y" on the Mach3 interface.
- Put the red protection cover back to the origin position.
- Activate the vaccum bed and leave the room.

**8.** Load the toolpath file and start!
- Load G-code and select the file that export from VCarve.
- You can check the file and working area on the preview window on the top-right interface.
- Press the **green button** on the wall to start the machine.
- Make sure you close the door. The machine will stop or can not start if you open the door.

{{< postimg url = "start_cut.JPG" alt ="VCarve interface" imgw ="80%" >}}

{{< postimg url = "machine_btn.jpg" alt ="START and STOP buttons" imgw ="80%" >}}

**9.** Take out the tool and cleaning

After the working proccess is finished, wait a while to let the tool stop completely before you go into the toom.

The work after finishing cutting is also important:

- Turn off the vaccum bed and take away the material.
- Take out the tool carefully. Remember to put the sponge cube under the tool. Put the tools back to the origin position.
- Clean the dust on the working bed and the ground.
- Turn off the machine:
    1. Press the **red button**
    2. See the number doing down and finally hear a "click" sound.
    3. Turn off the main power.

{{< postimg url = "testing_cut.JPG" alt ="Testing pieces done" imgw ="80%" >}}

{{< postimg url = "testing_piece2-08.jpg" alt ="Try to find the better size for the bearing and sticks" imgw ="80%" >}}

### Post Processing

After finishing CNC milling process, there're still some works to make the pieces more completed and can be used:

{{< horigrid src = "remove_tab.jpg" alt ="hammer and chisel" imgw = "40%" >}}

<b>Hammer and Chisel</b></br></br>
Use them together to cut the tabs and remove the pieces from the material.

{{< /horigrid >}}

{{< horigrid src = "router.jpg" alt ="router" imgw = "60%" >}}

<b>Wood Router</b></br></br>

<li>Use it to take out the tabs and make the edge of the piece is flat and smooth.</li>
<li>Connect the router to the vaccum cleaner.</li>
<li>Use the clamps to fix the pieces on the table.</li>

{{< /horigrid >}}
</br></br>
**Sanding**

Also we can sand the surface of the pieces by sand paper or sanding machine to polish the pieces.

## Make something bigger: A Scooter

{{< postimg url = "scooter_finish.jpg" alt ="a little scooter" imgw ="60%" >}}

### 3D model

I wanted to make something that we can play with so I planned to make a little scooter. After warching some examples, I made a 3D model by Fusion 360:

I set some parameters for the scooter's stucture, but somehow it didn't work. So I still needed to adjust manually some part in the form of .dxf file 😂

{{< postimg url = "scooter_par.jpg" alt ="set some parameters for the design" imgw ="100%" >}}

<div width="100%" style="display:flex; justify-content:center">
    <iframe src="https://aalto254.autodesk360.com/shares/public/SH35dfcQT936092f0e436cb8906a26aa922c?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
</div>

### Toolpath and CNC

I exported the sketch as .dxf file and import into VCarve. There're some points needed to notice:

- Because the material was too big, the vaccum bed could not hole it. Kris helped me use srews and clamps to fix the material on the vaccum bed.

{{< postimg url = "fixMaterial.JPG" alt ="use clamps and screws" imgw ="80%" >}}

- Set the **Z Zero Position** as {{< hightlight color = "#FFFF00">}} Material Surface {{< /hightlight >}}, also increase the **Rapid Z Gaps abouve Material, Home / Start Position**.

- Make sure the running path will not hit the clamps and screws.

{{< video src = "scooter_preview.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

### Pulse the running machine

In the middle of the milling process, the tool looked very possible to hit the clamp, so we wanted to stop the machine to remove the clamp and rerun the machine again.

{{< postimg url = "remove_clamp-09.jpg" alt ="remove the clamp before the tool hit it" imgw ="90%" >}}

I learn how to pulse the process and restart it:

1. Press **Feed Hold** to make the tool stop at the current step.
2. Press **Spindle CW F5** to stop the tool rotating.
3. Go inside the room to adjust the clamps.
4. Go outside the room and close the door.
5. Press **Spindle CW F5** again to make the tool start rotating.
6. Press **green button** on the wall to continue the process.

{{< postimg url = "pulse.JPG" alt ="VCarve interface" imgw ="80%" >}}

### Soft Limits

At the first time that I started the machine, there's a waring popped up. It's because some of the parts were out of the range of the machine. So I rearranged the parts on the material and exported a new toolpath file.

{{< postimg url = "soft_limit.JPG" alt ="warning from Mach3" imgw ="90%" >}}

### Post-process and Assembly

##### Post-process

{{< postimg url = "scooter_cut_done.JPG" alt ="milling finished" imgw ="70%" >}}

- I used hammer and chisel to cut the tabs and used wood router to remove the tabs.

{{< postimg url = "post_process-1.JPG" alt ="remove the tabs" imgw ="50%" >}}

- And then I found **I forgot to use fillets** to deal with the corner of the joints 😭 so it's hard to connect different parts. Fortunately, I found a tool that allows me to sand the corner into a straight angle.

{{< postimg url = "post_process-2.JPG" alt ="a tool to sand the corner" imgw ="70%" >}}

- Finally, I used sandpaper to refine the surface of each part.

##### Assembly

I realized that I forgot to consider the order of assmbling process when I design the scooter 😂 because some parts couldn't combine together when the other part was already there. I also found some problems that I didn't notice when I designed:

- The friction of the rotation part was too big so it was hard to turn around or change the direction.
- The connection of the front pard and the body of the scooter was too weak.
- The stick of the wheel and the bears were too small, they broken when I tried to ride it. 😂
- The plate for the feet to stand on was too big so the scooter was not easy to get balance.

### Try it

There's some problems about the scooter's stucture so it could only go straight 😂 😂 😂

{{< video src = "ride_scooter.mp4" type = "video/webm" preload = "auto" videow = "30%" >}}


## Files

- [Fusion360 file of the scooter](https://a360.co/3JmGtnN)
- [.dxf file of the scooter](https://gitlab.com/YuhanTYH/digital-fabrication/-/blob/main/static/files/cnc/scooter_final.dxf)
