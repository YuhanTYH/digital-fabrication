---
title: "Embedded Programming"
foldername: "programming"
image: "ledMatrix_hero.gif"
date: 2022-03-23
author: "Yuhan Tseng"
tags: ["PCB","LED matrix","flashing"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we learned how to actually use the PCB we made before, which means we programmed the board and controled something with the board.

I tried to programme the PCBs during the electronic design and production weeks. Although I could control the LEDs that I put on the boards, I'm still confused with the process and the principle about the chip (microcontroller) we used. So the goal of this week is trying to organize what I did and what I learned about the microcontroller.

## Goal

:point_right: Compare the performance and development workflows for other architectures. Try to program two different microcontroller boards.

:point_right: Read the datasheet for your microcontroller and identify information that could be useful for your project.

:point_right: Use your programmer to program your board to do something.

## Overall about Programming a PCB

We learned to how to program our board so we can control different componenets and get the data from different senesors in the futures. Based on the [FabLab's programmer page](http://pub.fabcloud.io/programmers/summary/), there are three types of board we are using so far, like different types are different roles, and here are what I underestand about them:

- **Programmer** (e.g. UPDI programmer, SWD programmer) boards are used to program other board, which means we need these boards to talk to other boards. (still not sure about this part)
  - **SWD (Serial Wire Debug)**: A 2-pin interface (SWDIO/SWCLK)
  - **UPDI (Unified Program and Debug Interface)**: A 1-pin interface that combines TX and RX into one pin.
  - We can use SWD programmer to flash most of the other boards that we have learned so far.
  - We can use UPDI programmer to program ATtiny series boards.

{{< postimg url = "programmer.JPG" alt ="left: Kris's SWD programmer, middle: I tried to make a SWD programmer with type C connector, right: my UPDI programmer" imgw ="90%" >}}

- **Hello Boards** are like the simple version of the board that we need for our final project. Most of the hello boards have LEDs to let you test your code. They are similar with a little arduino, we can develop them into more complecated boards and connect the sensors and components we need. Here are some boards I made:

{{< postimg url = "helloboard.JPG" alt ="left: ATtiny412 Hello Cat board, middle: D11C Hello board, right: ATtiny1614 LED Matrix" imgw ="90%" >}}

- **Bridges** are the board used to communicate to the computer from Hello boards. For example, we can write some code in hello board and let it send messages to the computer. In this case, we need to connect the hello board to the bridge, and then connect the bridge to the computer.

{{< postimg url = "bridge.JPG" alt ="Serial D11c to let hello boards to talk to the computer" imgw ="90%" >}}

## Start Programming

### Round 1: Make my UPDI programmer functional

At the Electronic Production week, I really wanted to know if the D11C programmer was successfully made. Keris helped me to flash my board with a SWD programmer:

**1.** Connect SWD programmer to the computer, use ``` edbg -l ``` to check if the board is detected by the computer:

  ```shell
    edbg -l
    Attached debuggers:
        0: 4344C80D - Alex Taradov Generic CMSIS-DAP Adapter
  ```

**2.** Connect SWD programmer and UPDI programmer, make sure the connecting direction of 4 pins are the same. Than inserted both in USB ports.

{{< postimg url = "swd_UPDI.JPG" alt ="connect SWD programmer and UPDI programmer" imgw ="50%" >}}

**3.** Download the binary for the bootloader (you can find in )[D11C programmer gitlab page](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c).

**4.** Upload the bootloader to UPDI programmer by SWD programmer:

  type this command:

  ```shell
    sudo edbg -ebpv -t samd11 -f [file path of the bootloader file]
  ```

  and got this:

  ```shell
    Debugger: Alex Taradov Generic CMSIS-DAP Adapter 4344C80D v0.1 (S)
    Clock frequency: 16.0 MHz
    Target: SAM D11C14A (Rev B)
    Erasing... done.
    Programming.... done.
    Verification.... done.  
  ```

it looked like flashing successfully! Now my D11C programmer should be detected by the computer.

**5.** Success!
{{< postimg url = "UPDI_port.png" alt ="UPDI is detected by the computer" imgw ="90%" >}}

**6.** The final step is uploading [USB-to-serial firmware](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino). Remember to choose the right board and the setting:

{{< postimg url = "upload_serial.png" alt ="setting for uploading firware" imgw ="50%" >}}



### Round 2: Light up LED of Hello D11C

According to the [D11C programmer gitlab page](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c), I need a SWD programmer to flash my board, so I borrow one from Kris.

At the Electronic Production week, I also made a Hello D11c. According to the [Hello D11C gitlab page](https://gitlab.fabcloud.org/pub/programmers/hello-d11c), I need a SWD programmer to flash my board. So I borrow one from Kris and tried to flash the board by myself.

Then I struggled at the very beginning, I could not install EDBG on my computer 😂 😂

##### Install EDBG (Mac)

After the help from Kris, I finally installed EDBG on my computer. Here is what we did:

**1.** Download the latest version of edbg file from [here](https://taradov.com/bin/edbg/). Then I got ```edbg-mac-r31``` file in my download folder.

**2.** Open terminal:

  - go to the folder of the file, in my case is Downloads folder ```cd Downloads```
  - list all the files that include "ed" to check the file name ```ls ed*```
  - some files need permission to execute in the computer so we need to make the file executable. **chmod** is the command to change the setting of file's permission: a = all group, x = execute
  
      ```chmod a+x edbg-mac-r31```
  
  - check if the file is running on the computer now ```./edbg-mac-r31 -h```
  - move the file to the topper layer of the computer so wholde computer can access the file: **mv + "file name" + "destination address & new file name"**
  
      ```sudo mv edbg-mac-r31 /usr/local/bin/edbg```
  
  - go back to the home directory ```cd```
  - check the programme is running again ```edbg -h```
  - if there are command options appeared, that's mean I installed EDBG successfully!

    {{< postimg url = "edbginstall.png" alt ="install edbg successfully" imgw ="90%" >}}

##### Flash Hello D11C by EDBG

Now I was supposed to flash bootloader on Hello D11C board by EDBG, but I got this:

``` Error: invalid response received ```

and I didn't know the reason. I found that Matti [got the same error and he used Arduino IDE to burn bootloader successfully!](https://fabacademy.org/2022/labs/aalto/students/matti-niinimaki/weekly-assignments/week04/#download-and-burn-the-bootloader-for-the-samd11c14a-chip) so I followed his step.

{{< postimg url = "flash_helloupdi.JPG" alt ="connect Hello D11c and SWD programmer" imgw ="70%" >}}

and it looked successful!

{{< video src = "helloUPDI_flash.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

##### Upload Code to Control LED

I copied the Blink.ino from gitlab page to test the LED:

```c
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(5, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

{{< video src = "helloUPDI_led.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

</br>

{{< video src = "helloUPDI_led2.mp4" type = "video/webm" preload = "auto" videow = "50%" >}}

### Round 3: Light up LEDs of Hello Cat ATtiny412

During Electronic Design week, I made a Hello Cat ATtiny412 and tried to program it. To program hello board, I need a **{{< hightlight color = "#FFFF00">}}UPDI programmer{{< /hightlight>}}**:

1. Upload [this code](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino) to UPDI programmer with [this setting](https://github.com/qbolsee/SAMD11C_serial), so the **UPDI programmer can as a bridge from Arduino to Hello Cat ATtiny412.** This step is important. If you don't upload this code to UPDI programmer, you will get the error like this ```UPDI initialisation failed``` (but this is not the only reason to get this error, I also got the same error somethimes after uploading this code).

<div width="70%" style="box-sizing:border-box; border-radius: 20px; background-color:#F9E79F;padding:20px 30px">
<b>USB Connection Issue?</b></br>

Sometime my computer (Mac) can't detect the board, and can't upload the code when I use USB hub. I also got <span style="background-color:#F17423; color:white"> UPDI initialisation failed </span>. But most of the time I succeed when I only use a single USB adaptor without using USB hub. So I guess is there some problem is related to Mac computer.

  <div width="100%" style="display:flex; flex-direction:column; ustify-content:center; align-items:center;">
    </br>
    {{< postimg url = "single_adaptor.JPG" alt ="using single adaptor" imgw ="60%" >}}
  </div>
</div>

</br>
2. Upload the code to Hello Cat ATtiny412. [Here is the proccess and outcome](https://yuhantyh.gitlab.io/digital-fabrication/assignments/electronic_design/electronic_design/#program-hello-pcb).

### Round 4: Communicate to the computer by Hello Cat

If I want to get some massages from Hello Cat ATtiny412, I need a **{{< hightlight color = "#FFFF00">}}Serial D11C board{{< /hightlight >}}**. I also made one during Electronic Design week. Following the [Serial D11C board page](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c):

**1.** I flashed the board by Kris's SWD programmer and Arduino IDE.

**2.** Upload [USB-to-serial firmware](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino). This step was same as what we did for UPDI programmer. And the purpose here is to **make Serial D11C board as a bridge from ATtiny Hello board to the computer.**

**3.** I uploaded this code by UPDI progrmmer:

```c
void setup() {
  Serial.begin(115200);
  pinMode(4,INPUT_PULLUP);
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);

}

void loop() {
  if(!digitalRead(4)){ //when press the button
    digitalWrite(2,HIGH); //light up 2 LEDs
    digitalWrite(3,HIGH);
    Serial.println("LED ON"); //print out some text
    
  }
  else{ //when release the button
    digitalWrite(2,LOW); //turn off 2 LEDs
    digitalWrite(3,LOW);
    Serial.println("LED OFF"); //print out some text
  }
}
```

**4.** Connect Serial D11C board and Hello Cat board and open Arduino Serial Monitor. Make sure the baud rate is the same as your code.

{{< video src = "hello_bridge.mp4" type = "video/webm" preload = "auto" videow = "90%" >}}

### Round 5: Make my SWD programmer functional (Failed)

Based on the experiences of these weeks, I found that SWD programmer is kind an important role to make everything start, so I decided to make my own one. Also thanks Kris for uploading USB type-c into the lab and I think maybe that can solve some connection problem of Mac computer.

I used the file from [Programmer SWD D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-swd-d11c) and changed the USB part to type-c connector. It's soooo hard to solder type-c connector part 😂 😂 😂

{{< postimg url = "swd_typec.png" alt ="changing the USB connector part" imgw ="80%" >}}

After finishing the board making, I followed the instruction on the page and tried to flash the board. Then I kept getting error message and didn't know the reason 😂 so I asked Matti for help. I learned:

- **Using EDBG by accessing a folder**
  - We dowloaded [EDBG repository from here](https://github.com/ataradov/edbg).
  - Open terminal: ```cd "path of the edbg folder"```
  - ```./edbg -h``` to check if we can use edbg. If there a bunch of commands show up, it means that we can use edbg now.
  
- **How to solve EDBG problem of Mac computer**
  - Go to the edbg folder and open a file called **"dbg_mac.c"**
  - Find this code ```static int report_size = 64; // TODO: read actual report size``` and **change "512" to "64"**. Save the file.
  - Now we should be able to use SWD programmer to flash other boards by EDBG on Mac computer.

- Also we found there're some problem of my soldering:
  {{< postimg url = "solder_problem.JPG" alt ="Dn and Dp pin were connected by the solder" imgw ="80%" >}}

##### Flash SWD programmer by EDBG

SWD programmer can be used to program another SWD programmer, too. SO I connected Kris's SWD programmer to my SWD programmer. Something that I need to pay attention is that the 4-pin connector, which is closer to the D11C chip, is only for flashing bootloader. So I connected the seconed 4-pin connector of Kris's SWD to the closer connector to my SWD.

{{< postimg url = "swd_flash.JPG" alt ="connect two SWD to flash bootloader" imgw ="80%" >}}

I donwloaded the **binary file** and put it in edbg folder, and ran these command:

```./edbg -ebpv -t samd11 -f free_dap_d11c_mini.bin```

and got:

```shell
Debugger: Alex Taradov Generic CMSIS-DAP Adapter B5A3422E v0.1 (S)
Clock frequency: 16.0 MHz
Target: SAM D11C14A (Rev B)
Erasing... done.
Programming...Error: invalid response during transfer (count = 6/12, status = 0)
```

##### Flash SWD programmer by OPENCD

From the gitlab page, there another way for MacOS to flash the board: OpenOCD, so I installed it and tried to use.
I got this:

```shell
openocd
Open On-Chip Debugger 0.11.0
Licensed under GNU GPL v2
For bug reports, read
 http://openocd.org/doc/doxygen/bugs.html
DEPRECATED! use 'adapter driver' not 'interface'
Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = v0.1
Info : CMSIS-DAP: Serial# = 4344C80D
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 0 TDO = 0 nTRST = 0 nRESET = 1
Info : CMSIS-DAP: Interface ready
Info : clock speed 400 kHz
Info : SWD DPIDR 0x0bc11477
Info : at91samd11c14a.cpu: hardware has 4 breakpoints, 2 watchpoints
Error: at91samd11c14a.cpu -- clearing lockup after double fault
Polling target at91samd11c14a.cpu failed, trying to reexamine
Info : at91samd11c14a.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for at91samd11c14a.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xe1000000 pc: 0xfffffffe msp: 0xfffffffc
target halted due to debug-request, current mode: Thread
xPSR: 0xe1000000 pc: 0xfffffffe msp: 0xfffffffc
** Programming Started **
Info : SAMD MCU: SAMD11C14A (16KB Flash, 4KB RAM)
Info : SWD DPIDR 0x0bc11477
Error: Failed to write memory at 0x000003c0
Error: samd_write: 889
Error: error writing to flash at address 0x00000000 at offset 0x00000000
embedded:startup.tcl:530: Error: ** Programming Failed **
in procedure 'script'
at file "embedded:startup.tcl", line 26
in procedure 'program' called at file "openocd.cfg", line 9
in procedure 'program_error' called at file "embedded:startup.tcl", line 595
at file "embedded:startup.tcl", line 530
Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections
Error: at91samd11c14a.cpu -- clearing lockup after double fault
Polling target at91samd11c14a.cpu failed, trying to reexamine
Info : at91samd11c14a.cpu: hardware has 4 breakpoints, 2 watchpoints
^Cshutdown command invoked
```

The flasghing process always got stock at this line ```Info : at91samd11c14a.cpu: hardware has 4 breakpoints, 2 watchpoints```

Somehow I felt the key point is this line:
```Error: at91samd11c14a.cpu -- clearing lockup after double fault```
So I googled about this error but haven't found something work 😂  Also I'm thinking about maybe the problem us about the cirtuit design?

### Round 6: Control a LED Matrix board (ATtiny1614)

I think I need to control multiple motors for the final project, and I though of that something about LED Matrix, which is kind of a way to control multipe LEDs. I found [this example](http://www.multiwingspan.co.uk/arduino.php?page=matrix) and wanted to try it. Maybe this can give me some direction about controlling multiple motors.

{{< video src = "ledMatrix.mp4" type = "video/webm" preload = "auto" videow = "90%" >}}

##### Code

```c
int row[] = {0, 1, 2, 3};// anodes
int col[] = {6, 7, 8, 10};// cathodes

int btn_count = 0;

void setup()
{
  Serial.begin(115200);
  pinMode(9, INPUT_PULLUP);
  for (int i = 0; i < 4; i++)
  {
    pinMode(row[i], OUTPUT);
    pinMode(col[i], OUTPUT);
  }
  allOff();
}

void loop()
{

  if (!digitalRead(9)) {
    btn_count++;
    delay(200);
  }

  if (btn_count == 1) {
    pattern1();
  }
  else if (btn_count == 2) {
    pattern2();
  }

  else if (btn_count > 2) {
    btn_count = 0;
  }

  delay(150);
  Serial.println(!digitalRead(9));
}

void allOff()
{
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(row[i], LOW);
    digitalWrite(col[i], HIGH);
  }
}

void pattern1() {
  for (int j = 0; j < 4; j++) {
    for (int i = 0; i < 4; i++)
    {
      digitalWrite(row[j], HIGH);
      digitalWrite(col[i], LOW);
    }
    delay(300);
  }
  allOff();
}

void pattern2() {
  for (int j = 0; j < 4; j++) {
    for (int i = 0; i < 4; i++) {
      digitalWrite(row[j], HIGH);
      digitalWrite(col[i], LOW);
      delay(100);
      allOff();
    }
  }
}
```

## Files

- Most of the boards are from [Fad Academy Programmer page](http://pub.fabcloud.io/programmers/summary/)

- [Kicad project of LED matirx board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/programming/ATtiny_led_matrix)
