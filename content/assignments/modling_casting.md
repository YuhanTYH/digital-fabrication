---
title: "Modling and Casting"
foldername: "modling_casting"
image: "hero_img.JPG"
date: 2022-03-30
author: "Yuhan Tseng"
tags: ["casting","3D milling"]
categories: ["NOTE"]
draft: false
---

This week we learned about making an object by modeling and casting. This was a very new filed for me and I realized that there're various ways to make a mode and do casting, but the main concept is the same.

## Goal

:point_right: Review the safety data sheets for at least two of labs molding and casting materials.

:point_right: Make and compare test casts with each of them.

:point_right: Design a 3D mold around the stock and tooling that you'll be using.

:point_right: Mill it (rough cut + (at least) three-axis finish cut).

:point_right: Use it to cast parts.

## The Overview of the Procedure

In the beginning, I was confused about why there were so many steps to make an object and what are the differences between them. Then I got some basic concepts from [this FabLab website](<http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/moldNcast/MNCtutorial/>).

In term of casting, the goal is getting an 3D object (final object):

1. Make a **wax mode** which is a **mode of mode** by a milling machine.
2. Make a **silicone mode** which is a **casting mode** by the mode of mode.
3. Make a **final object** by the casting mode.

There are various ways to make a mode of mode, but one of our goal of this week is learning 3D toolpath of the milling machine, so we make an 3D model for the mode of mode.

In term of making a 3D model, the goal is getting an 3D model of the wax mode and using milling machine to make it:

1. Make a 3D model of the **final object**.
2. Make a 3D model of the **casting mode** by using a block subtracts the final object.
3. Make a 3D model of the **mode of mode** by using a block subtracts the casting mode.

{{< postimg url = "overview.png" alt ="overview" imgw ="90%" >}}

## Making Mode of Mode

We used **MDX-40 milling machine** to make a mode of mode by wax. There are mainly **three phases** of the process:

**1. Flating the surface**

**2. Roughing**

**3. Finishing**

Here is an example process of making a mode of mode with foam material:

### Flating the surface

This step is to make sure the surface of the material is flat and the height of all the points of the surface are the same.

  **1.**  Measure the dimension of the material and install the material in the machine. **Don't put the material at the corner** because it may break the tool when the tool is processing the edge of the material.

  **2.** Install the tool
  {{< horigrid src = "install_tool.png" alt ="installing a tool" imgw = "80%" >}}
    <li>Install the collet with the right size.</li>
    {{< postimg url = "collets.JPG" alt ="collets with different size" imgw ="90%" >}}
    <li>Install the bit. For this phase we use butterfly drill bit.</li>
    <li>Use the spanners to tight the tool.</li>

  {{< /horigrid >}}

**3.** Use **VCarve** to make toolpath file

- Create a new file and set the jop size according to the material's size.
- Draw a rectangle to cover the whole surface of the material.
- Select the rectangel and create a **Pocket Toolpath**
  - Cut Depth: How deep your want to remove. Make sure it covers the lowest point of the surface.
  - Tool: End Mill(22mm). Need to pay attantion about **the maximine feed rate** of the machine. In this case, although the feed rate be calculated is 5600 mm/min (use MDF chip load as refernce), we need to reduce to under 3000 mm/min because [the maximum feed rate of MDX-40](https://www.camplex.com.au/roland-mdx40a-specifications/) is 50mm/sec.
  - Pocket Allowance: - 11m (minus the radius of the tool) => so the surface can be cleared up completed withoug margin.
  - Create this pocket toolpath and save it. Make sure the Post Processer is MDX-40.

**4.** Start milling the surface
  
- Open **VPanel**,  make sure choose **NC Code** in setup and **G54** for the coordinate system.
- Use sensor to set the z-origin.
- Use the ceter of the tool to align the left-bottom point of the material's surface, setting it as xy-origin.

{{< postimg url = "set_origin-02.jpg" alt ="left: set z-origin, right: set xy-origin" imgw ="80%" >}}

- **Override**: The value of here will replace the value in G-code. {{< hightlight color = "#FFFF00">}} Decrease the Cutting Speed (the same as feed rate in G-code)to 50% (decrease more if cut harder material){{< /hightlight >}}, so the tool will not move too fast.
- Cut >> Delete All >> Add the toolpath file >> Output
- If everything go well, we can increase the Cutting Speed gradually to 100%.

**5.** Depending on the material, we can also flat the surface of another side. Remember to make a new toolpath file with the new thickness.

### Roughing and Finishing

In Roughing phase, we remove most material that we don't want, and ignore the detailed part and the smoothy of the surface.

Create a new file in VCarve, and set the job size same as your material:

**1.** File >> Import >> Import Component/ 3D Model, and choose the 3D model file (.stl)

**2.** Set the Orientate 3D Model  

- Press "Center Model" to align the 3D model and the range of milling job.
- Adjust the zero plane position, make sure all the parts that we want to mill are above the zero plane. "Discard data below zero plane", then the parts that are under the zero plane will not proceed.

{{< postimg url = "zeroplane.png" alt ="zero plane of milling job" imgw ="80%" >}}

**3.** Set the Material

**4.** Select **Rough Machining Toolpath**

- Select tool: End Mill (6mm)
  - Path Depth: 2mm (harder material, smaller value)
  - Stepover: 40%
  - Spindle Speed: 14000 r.p.m.
  - Feed Rate: 2 x 0.07 x 14000 = 1960 mm/min
  - Plunge Rate: 490 mm/min

- Machining Allowance: 0.1mm (leave some material for the finishing phase)
- Calculate and create it.

**5.** Select **Finish Machining Toolpath**

- Select tool: Ball Nose 3mm Dia
  - Path Depth: 1mm (harder material, smaller value)
  - Stepover: 1mm
  - Spindle Speed: 14000 r.p.m.
  - Feed Rate: 2 x 0.04 x 14000 = 11200 mm/min
  - Plunge Rate: 490 mm/min

- Calculate and create it.

**6.** Save the toolpath files seperately

**7.** Start roughing:

- Change the tool to 6mm flate end tool.
- Set the z-origin (if you moved the material after facing, set the xy-origin, too)
- Decrease Cutting Speed to 50%
- Cut >> Delete All >> Add the roughing toolpath file >> Output
- If everything go well, we can increase the Cutting Speed gradually to 100%.

**8.** Start finishing:

- Change the collect and the tool to 3mm ball nose tool.
- Set the z-origin, **don't reset the xy origin**
- Decrease Cutting Speed to 50%
- Cut >> Delete All >> Add the roughing toolpath file >> Output
- If everything go well, we can increase the Cutting Speed gradually to 100%.

**9.** Cleaning

Milling process usually creates lots of dust and little chips, remember to clean up the working space and machine! There's a strogn vaccume cleaner in Aalto FabLab so we can clean the dust and little chips from the material easily. Also remember **remove the tools and put them back**.

## Making the Casting Mode

In this week, we used **silicone** to make casting modes. There are many various types of casting and modeling material with different property so we need to pay attention to what are the possible dangers from them. We can know that from the **safty datasheet** of the material. Here is an example from the material we used in this week:

{{< postimg url = "safty_datasheet.JPG" alt ="the possible danger of Smooth-Cast liquid plastic si" imgw ="60%" >}}

Here are the steps of making casting mode with silicone:

**1.** Wear the gloves and protective glasses.

**2.** Check how many silicone you need. We can pure water into the wax mode and know the volume we need.

**3.** Usually the silicone have two sub-material. Follow the combining ratio from the instruction and mix them together.

**4.** Add some **Silicone Thinner** to make it more like liquid and easier to use.

**5.** To avoid the bubbles inside the silicone which will make some hollows into the mode, we use **vaccum chameber** to remove the bubbles:

- Control the pressure by opening or closing the little hole on top of the cover.
- Don't open the hole too fase.
- Increase the pressure (by opeing the hole) when the pressure achieve 100.
- Repeat few times of this process.

  {{< postimg url = "vaccum_chamber.png" alt ="vaccum chamber" imgw ="80%" >}}

**6.** Pour the silicone into the wax mode from a higher position.

{{< tipblock >}}
    Be careful about the <b>Pot Life</b> of the material. We need to finish the process before this time.

    {{< postimg url = "pot_life.JPG" alt ="pot life of liquid plastic" imgw ="80%" >}}
{{< / tipblock>}}

## Cast an object

Cast a object by casting mode is almost the same process of making a casting mode. In this week, we used liquid plastic to cast objects.

## What I tried

I was thinking about the difference between 3D printing and casting to make an object, and I think the benefit of casting is that you can make multiple same objects in a shorter time than 3D printing. So I tried to make something like a little unit and I can combine them in different ways.

{{< postimg url = "final.JPG" alt ="little animal blocks" imgw ="80%" >}}

<div width="100%" style="display:flex; justify-content:center">
    <iframe src="https://aalto254.autodesk360.com/shares/public/SH35dfcQT936092f0e43d376a0d3d7da4169?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
</div>

### The first try with foam

I used foam to make a mode of mode as practice. From the review image of the VCarve, I found that the gaps between some parts of the model were too small and the tool couldn't go though. So I justed the model file and tried again.
{{< postimg url = "foam_try.JPG" alt ="the gap is too samll to mill" imgw ="70%" >}}

##### Facing

The first step is facing, making the surface of the material flat.

{{< postimg url = "foam_facing.JPG" alt ="after facing" imgw ="80%" >}}

##### Roughing

I used 6mm flat end tool for roughing.

{{< postimg url = "foam_Roughing.JPG" alt ="after roughing" imgw ="80%" >}}

##### Finishing

{{< postimg url = "foam_finishing1.JPG" alt ="after finishing by 3mm flat end tool" imgw ="80%" >}}

{{< postimg url = "foam_finishing2.JPG" alt ="after finishing by 3mm ball nose tool" imgw ="80%" >}}

After the finishing, I found that the space under the ears' part couldn't be processed because the tool can only operate vertiaclly. So I adjusted the angles of the ears.

{{< postimg url = "chaning_model-03.jpg" alt ="changing the angle of the ears" imgw ="80%" >}}

### Make a Wax Mode (mode of mode)

After trying with the foam, I used wax to make a real modling mode.

##### Roughing

{{< postimg url = "wax_roughing.JPG" alt ="after roughing" imgw ="80%" >}}

##### Finishing

{{< postimg url = "wax_finishing1.JPG" alt ="after finishing by 3mm flat end tool" imgw ="80%" >}}

{{< postimg url = "wax_finishing2.JPG" alt ="after finishing by 3mm ball nose tool" imgw ="80%" >}}

### Make a Silicone (casting mode)

I tried two different silicone, one was Mold Start which needs a few hours to dry; another was SORTA-Clear which can dry faster but the pot life is very short. I didn't how to use SORTS-Clear properly and it was easy to get lots of bubbles inside it.

{{< postimg url = "silicon_mode-04.jpg" alt ="silicon mode by Mold Start" imgw ="80%" >}}

{{< postimg url = "silicon_mode-05.jpg" alt ="silicon mode by SORTA-Clear" imgw ="80%" >}}

### Casting Objects

When I designed the mode, I thought I only need one hole for pouring the material. Then I found only one hole was not enough because **there're not space for the air to escape from the mode when I pouring the material inside the mode** 😂 😂 So what I got was a object with a big hole on the top.

{{< postimg url = "casting_object-06.jpg" alt ="first try of casting: hole on the top" imgw ="100%" >}}

After the first try, I tried to make another hole on the mode, this time I got a better result but still have some little bubbles happened in the ear's part.

{{< postimg url = "casting_object-07.jpg" alt ="bubble at the ear part" imgw ="80%" >}}


## Files

- [Fusion 360 model of the animal block](https://a360.co/3Ds650n)
- [Fusion 360 model of the casting mode](https://a360.co/3F9Jfvq)
- [Fusion 360 model of the mode of mode](https://a360.co/3OQjSTZ)

These files are not the perfect version, because:

- I designed to make these blocks connect together not only on the top and the button, but also front side and the back side, but some how the connection part was removed when I process the mode of mode.

- The ear's part is too small (or is the thickness of the mode too thin?), so that part is hard to cast and usually causes some holes.

- The four legs are too thin and small, they were broken when I tried to remove the silicone mode from the wax.

{{< content >}}
