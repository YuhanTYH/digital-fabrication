---
title: "Invention, IP and Income"
foldername: "invention"
image: "heroImg.gif"
date: 2022-06-13
author: "Yuhan Tseng"
tags: ["Source licences"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

## Goal

👉 Describe what you plan to do with your final project in the future.

👉 Explore and compare 3 different open source licences.

👉 Add copyright notice to your documentation website with a licence of your choice.

👉 Prepare a 1 minute long video presenting your final project (1080p, 25fps, mp4, <10MB) and add it to your final project page as well as the root of your website.

👉 Prepare a 1920x1080 px slide representing your final project and add it to your final project page as well as the root of your website.


## Future Plan

My final project is a **"Wave Machine"**, you can find the [**detailed documentation here**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project/). Actually, I haven't fully finished it and still have something want to try out, and here are to do list of the machine:

- Find a flexible material so the user can make it as a surface and attach on the top of all units.
- Contol by a sensor that people can interact with.
- A better apperance.
- Respond to the music.
- Wireless controlling (not sure about this haha)

The most recent goal is to finish the list above, find a concept that I want to discuss with people, and join the exhibition in autumn to share this project with people.

I consider it as a "interactive sculpture" so people can have some interaction and connection with it. It will not be a product in the future. However, because you can attach different material and objects to it, I hope people can use it to perform or display different ideas and concepts by it.


## Open Soure Licences

I heard about **CC licenses** for a long time but haven't understood what they mean before. Now I know it is a way to protect our creation and copyright when we want to share our project with the public. I knew [**this website: creative commons**](https://creativecommons.org/) from the lecture. I learned different types of CC (Createve Common) licesnses from [**here**](https://creativecommons.org/about/cclicenses/)


I hope people can be inspired by my project and also use my project to do more interesting things, so I chose **"CC BY-SA"** license for my final project, which means that "the reusers are allowed to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. If you remix, adapt, or build upon the material, you must license the modified material under identical terms." (from [createive commons' website](https://creativecommons.org/about/cclicenses/))

## 1-min Video & the Slide

You can see the short intro-video and the slide of this final project [**here**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project/).

