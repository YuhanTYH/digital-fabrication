---
title: "Project Management"
foldername: "project_manage"
image: "heroImg.jpg"
date: 2022-02-03
author: "Yuhan Tseng"
tags: ["GitLab"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

There were so many things were talked about on the topic of Project Management, so I took the note of this content in the separate parts:


## Goals

**Part 1: Git, GitLab**

👉 Know what is Git and GitLab, why we use it.

👉 Be familiar with the terminal.

👉 Start GitLab.

**Part 2: Make a Website**

👉 Connect the locoal folder to GitLab repository (repo).

👉 Be familiar with the basic git comment and the workflow of using GitLab.

👉 Publish a website and know how to update and howt to modify it.

**Part 3: Hugo & Static Website**

👉 Know what is Hugo, and why we use it.

👉 Build a Hugo website.

👉 Know how to apply a Hugo theme and how to modify it.



## Part 1: Git, GitLab

This part is about a comman and popular system of project management: Git.

### What and Why Git?

[Git](https://www.atlassian.com/git/tutorials/what-is-git) is a version control system which helps you manage your project conveniently and systematically. You can have a clear historical record of any change of the files in the project, it also reduces the messy conflicts that usually happen when many people collaborate on the same project. In addition, it can store the changes offline so you can still process your project without internet.

[GitLab](https://www.simplilearn.com/tutorials/git-tutorial/what-is-gitlab#what_is_gitlab) is a platform of software development which is based on Git.

Learning how to use Git system (and environment?) with terminal can make the working process more efficient in the future, and I think that help me understand more about my project stuctures.

Honestly I haven't catched the point of Git and GitLab. GitLab is like a cloud drive and free domain and server provider for me so far :joy:

### Make Friend with Terminal

Using terminal is a very convenient and efficient way to do any operation on your computer if you are familiar with its structure and logic. From now for DF course, the terminal will be a way that I can talk to my computer and also remote repo of GitLab directly.

To make myself don't give up too early, I try to

#### Make my terminal cooler

{{< postimg url = "terminal-05.png" alt ="make my terminal cooler" imgw ="100%">}}

- I use "agnoster" theme of [Oh My Zsh](https://ohmyz.sh/), and mainly followed this [article](https://www.freecodecamp.org/news/how-to-configure-your-macos-terminal-with-zsh-like-a-pro-c0ab3f3c1156/).

- I couldn't install or update [Homebrew](https://docs.brew.sh/FAQ#how-do-i-uninstall-homebrew) directly by commend, so I:

    1. unshallow (not sure what this mean) “homebrew-core” by ([ref](https://stackoverflow.com/questions/65160625/cant-update-or-upgrade-homebrew))
        ```git -C "/usr/local/Homebrew/Library/Taps/homebrew/homebrew-core" fetch --unshallow``` 
        
        (I wait for few minutes to start this process so don’t exit or close it too fast.)

    2. uninstalled Homebrew by
        ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)"```
    
    3. then install again.

- If the theme looked a little different whit the example, like this [question](https://stackoverflow.com/questions/37895501/display-issue-with-oh-my-zsh-agnoster-theme): install proper font and change the font of terminal.

- Can not install and use ITerm2? :point_right: Follow this [article](https://www.freecodecamp.org/news/jazz-up-your-zsh-terminal-in-seven-steps-a-visual-guide-e81a8fd59a38/). I use Iterm2 as my main terminal now and I can change the colors.

#### Some common commands

- **cd + folder path** : access to the spicific folder
- **cd ..** : go upper layer
- **pwd** : get the current position (path)
- **touch + file name** : create a file with that name
- **ls** : get all the files' name in the folder
- **ls -a** : get all the files' name in the folder including the hidden file
- **code .**: open the folder in Visual Studio Code
- using Tab to get hint of the comment or auto typting of the file's name

### Start GitLab

**1.** Create an account.

**2.** Set SHH key.

- SHH key can prove your right to access GitLab without inputing username and password every time.

- Each key is for one device to connect to GitLab.

- Follow [this](https://docs.gitlab.com/ee/ssh/) to generate a key.

- Put the key in the user folder instead of only the local project folder, so the whole computer can access the key.


- In GitLab website: click the profile on the top-right corner >> Preferences >> SSH keys (on the left side) >> Add your key

💡 When using GitLab with multiple computers, you can generate multiple SHH keys for each computer. Alternatively, you can only create one SSH key and copy the public & private key to the .ssh folder on all computers.

**3.** Create a blank project (set to public). There will be a README.md and a .yml in the project's already

🤘 The next part is about making a simple website 🤘


## Part 2: Make a Website

This part is about how to publish a website from GitLab.

### Basic Concepts

- A project of GitLab has a repo which contains all the files of this projec.

- For the documentation of Digitatl Fabrication course, I need to connect the local folder to the GitLab repo to:

    - Publish a website.
    - Update the website.
    - Manage all the files of my projects in this course.
    - Everything has the word "remote" related to GitLab part.

- The comment will start with "git" when the action is about git.

### Make a Basic Website

#### On the Local Computer

**1.** Install Git.

**2.** Make some global (= common) setting for Git:

```Shell
git config --global user.name “Your Name”
#Set the name that will be attached to your commits and tags.

git config --global user.email “you@example.com”
#Set the e-mail address that will be attached to your commits and tags

git config --global init.defaultBranch main
#running git init will produce a repository whose initial branch is main
#this line is because the defult branch in GitLab is "main" not "master"
#use "confiig --global init.defaultBranch" can see what is the current defult name
```

</br>
💡 THe code block in Markdown: add the programm language that your code use can show the syntax highlighting.

</br></br>
💡 I always got a branch named "mater" at the beginning, I'm not sure is it related to my previous setting about GitHub a long time ago. However, the default branch name of GitLab repo is "main" whichs made me keep encountering some errors about the conflict of the branch. I found a setting about defult branch name and tried it. It's look fine so far -> still need more experiment about this. [Ref](https://superuser.com/questions/1419613/change-git-init-default-branch-name)

</br></br>

**3.** Create a local folder for this project.

**4.** Connect this project folder (= local folder) to the GitLab project:

```shell
cd "local folder path"

git init
# Initialized empty Git repository in the local folder
# this means you create a loar repository
# there will be a .git file created in the local folder
git remote add origin "git repo url"
# you are adding the location of your remote repository where you wish to push/pull your files to/from
# "origin" is a alternative name for the remote repo
# so "origin" can be any other name
# git remote: get the alternitive name of the currennt remote repository

git pull <remote> <branch>
# remote = the alternitive name of the remote repo
# branch = the defulte brnach's name on the remote repo
# this command will create a new branch in the local folder and also merge to the remote branch
# this command syns the remote repo change to the local folder
```
</br>
💡 Using SSH url so I can use my SSH key automatically and don't need to input my username and the password every time.

{{< postimg url = "repo-url.png" alt ="get repo url" imgw ="80%">}}

Now there are the same files in the local folder and the remote repo.

**5.** Create:

- a "index.html" file in the local folder, and write something.
- a ".gitignore" file. The file name that be recorded in this file will be ignored by Git system.

**6.** Upload the local changes to the remote repo:

```shell
git status
# to check the local repo state now: are there some new changes?

git add +file name 
# stage the change of the spicific file
# or 
git add .
# stage all the change

git commit -m "some comment for this change"
# commit = the change for this time 

git push -u <remote> <branch>
# remote = the name of the remote repo
# branch = the name of the remote branch
# only need this line before the first push 
# this command let the local current branch track the remote branch

git push 
# sync the local changes to the remote repo
```

Now the index.html file should in the GitLab project's repo, too.

#### On GitLab Project Website

**1.** Delet ".gitlab-ci.yml".

**2.** Add a new .yml file, selecting "HTML" template.

{{< postimg url = "yml-file.png" alt ="add .yml file" imgw ="80%">}}

**3.** ```git pull``` in the terminal to sync the change.

**4.** From the GitLab project website >> Settings (on the left) >> Pages >> there is a address for your website.

🎉 Go to that address and see the website made by index.html so far! 🎉


### Basic Workflow

***When there are some changes in the local folder:***

```shell
git add + file name
# or git add .
git commit -m "xxxx"
git push
```

{{< postimg url = "workflow.png" alt ="how git push works" imgw ="50%">}}
{{< postimg url = "terminal-workflow.png" alt ="how git push works" imgw ="80%">}}

***When there are some changes in the remote repo:***

```git pull```

### Common Git Comments

- **git status**: check the local repo state now
- **git branch**: check all the branches' name
- **git remote -v**: check what the functions you can use for the remote repo
- **git config xxxx**: git setting or check the current setting, add "--global" before setting and this setting will affect all the git stuff you use on this computer
- **git log**: see the changing history.


## Part 3: Hugo & Static Website

This part is how to use Hugo to build a webite, also how to use Hugo theme to make the website look better.


### What and Why Hugo?

[Hugo](https://gohugo.io/) is a framework that helps you build a static website faster and easier. There's a ready-made structure and some files which are required for a website, so you can more focus on putting your personal content and information into that structure, and then show as a webstie.

{{< postimg url = "hugo.png" alt ="hugo" imgw ="70%">}}

(note: haven't fully understand the banifits of Hugo and what is static website.)

### Start Hugo

**1.** Install Hugo:

- [Here](https://github.com/gohugoio/hugo/releases/tag/v0.92.1) can find the latest version of Hugo.
- Donwload the file and unzip it. Move the "hugo" executable file to the computer user folder, so that everywhere in this computer can use Hugo.
- If you move it sucefully, then no metter where you use ```hugo version```, you can get the correct respond.
- ```which hugo``` can check where is the file.
- ```sudo mv +file name +path``` can move the file to thsi path.

**2.** Create a Hugo website:

- Go to the local project folder.  
- Move the previous website files into a temp folder in the local project foler.

    ```shell
    hugo new site ./ --force
    # ./ = create the website folder at the current path
    # --force : because the project folder is not an empty folder
    ```

- Then there're some folders and files created in the project folder.

**3.** ----- basic setting for Hugo structure----- TBC.....


#### On GitLab Project Website

**1.** Delet ".gitlab-ci.yml" if there is one in repo.

**2.** Add a new .yml file, selecting "HUGO" template.

- If you are using Hugo extended verion, change the first line

    ```image: registry.gitlab.com/pages/hugo:latest```

    to

    ```image: registry.gitlab.com/pages/hugo/hugo_extended:latest```



### Using Theme of Hugo

I use this [theme template](https://github.com/naro143/hugo-coder-portfolio).

(0501 Update: now I'm using [**this theme template**](https://github.com/gethugothemes/bookworm-light))

#### On Local Computer

**1.** Downoad the whole theme repo from github and unzip it.

**2.** Put this theme folder (here the folder name is "hugo-coder") into  **themes** folder.

**3.** Copy the whole content of config.toml that provided on the github, and chaged:

- theme name = the folder name

- baseurl = gitlab website address

**4.** Run ```hugo serve``` and goto the local host address to see the real-time website simulation. Now every change in the file will show on the website right away after saving the file.

**5.** Play around with the config.toml file to see each parameter corresponding to which element of the website.

#### Structue of the website pages

- All the pages put in **content** folder of the project folder.

- You can create multiple folders inside **content** folder to organize the pages.

- Each manu item has a folder.

- Different ways to organize pages:

    1. about: index.md and all the media files put in the same folder => access the media file by its file name directly. The page of about shows the content of index.html.

    2. assignments:
        - Connect to a list page first (havn't understand this part).
        - Each topic has a folder.
        - If that topic only have one page, name the page as "index.md" then it can access the media files by its file name directly.
        - If that topic has multiple pages, add **"../"** before the media file's name to get it.

    3. final-project
        - Connect to a list page first.
        - All the media files put into one folder "final-project-img".
        - There are multiple pages (.md file).
        - Access the media files by **"../folder name (here is final-project-img)/file name"** .

{{< postimg url = "file-structure.png" alt ="how files are organized in content folder" imgw ="80%">}}

💡 Different theme templates may have different ways to organize the files, also different ways to access the images or the videos on a page. So I usually spend some time trying out and finding the rule when I change a new theme template.

#### Note of customizing theme

- Menu: set the item's name, order (weight), and the path (url)

    {{< postimg url = "menuset-02.png" alt ="menu setting" imgw ="80%">}}

- Shortcodes: make some customized functions that can be used in markdown files. I tried to make some shortcodes to make my images and videos size more flexible in the page.

  - **postimg.html** => in the Markdown file, I can set the width of the image, add black border, make the image in the center.

    ```html
    <img class="post-img" src="{{.Get `url`}}" alt="{{.Get `alt`}}" width="{{.Get `imgw`}}" />
    <style>
    .post-img {
        display: block;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 4rem;
        margin-top: 2rem;
        border: 1px solid black;
    }
    </style>
    ```

    {{< postimg url = "../img/shortcodes-03.png" alt ="imgae shortcode" imgw ="80%">}}
  
  - **youtube.html** => in the Markdown file, I can set the ratio of the video's width and height, the width of the videoa and make it in the center.

    ```html
    {{ if or (eq (index .Params 1) "widescreen") (not (index .Params 1) ) }}
    {{ $.Scratch.Add "ratio" (mul 56.25 (int (index .Params 2)) 100.0)}}
    {{ else if eq (index .Params 1) "standard" }}
    {{ $.Scratch.Add "ratio" (mul 75 (int (index .Params 2)) 100.0)}}
    {{ else }}
    {{ $nums := split (index .Params 1) ":" }}
    {{ $.Scratch.Add "ratio" (mul (div (mul (int (index $nums 1)) 100.0) (int (index $nums 0))) (div (int (index .Params 2))100.0))}}
    {{ end }}
    <div class="youtube-div">
        <div
            style="position: relative; padding-bottom: {{ $.Scratch.Get `ratio` }}%; overflow: hidden; width: {{ .Get 2 }}%;">
            <iframe class="youtube-video" src="https://www.youtube.com/embed/{{ index .Params 0 }}" allowfullscreen frameborder="0">
            </iframe>
        </div>
    </div>
    <style>
        .youtube-div {
            width: 100%;
            display: flex;
            justify-content: center;
            margin-bottom: 5rem;}

        .youtube-video {
            position: absolute;
            width: 100%;
            height: 100%;
        }
    </style>
    ```

    {{< postimg url = "shortcodes-04.png" alt ="video shortcode" imgw ="80%">}}


