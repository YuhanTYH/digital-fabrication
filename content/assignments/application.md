---
title: "Applications and Implications"
foldername: "application"
image: "hero_img.jpg"
date: 2022-05-27
author: "Yuhan Tseng"
tags: ["proposal", "final project"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week's assignment is to let us think clearer and deeper about our final projects. I changed the idea of my final project many times, and even now I have not had a clear image about what I will make in the end 😂​ However, I got the direction and what I want to do during these few weeks and I think I'm on the right way now.

## Idea

I want to make a **kinetic sculpture** that can make some movements or patterns according to the different controlling ways or interactive inputs. One unit is an up-down structure and there're total 16 units that form a 4 x 4 matrix. The sculpture can create a wave when each unit moves to a different height.

Testing half of the matrix:


{{< youtube zT6aSf4VpNw "16:9" "70">}}

Though I think I don't have enough time to make it functional completely by next week, my final goal is make it as **a interactive work that can combine with sensors, music, or projection, and the user can attach different things on the top of each unit or a surface that connect to all units.**

## Project Checklist

My final project should inncoperate these skills and knowledge:

#### 2D and 3D design

I made 3D model in Fusion360 at the beginning for most of the parts of this project, and decided which parts would be made by the 3D printer or the laser cutter. Also, I tried EDM machine that I cut a 2D shape which needed a 3D model file.

#### Additive and subtractive fabrication processes

- **Additive fabbrication**: 3D printing
- **Subtractive fabrication**: laser cutting, milling machine, EDM machine 

#### Electronics design and production

There are three types of PCB I plan to make for this project:

- Stepper motor board: Each unit has a stepper motor board that uses ATtiny412 as a microcontroller to control a stepper motor.

- Power supply board: Because each unit needs to connect to a power source, I designed a board that has 4 outputs of the original high voltage and 1 output of 5v in case of connecting other sensors or boards in the future. Also, each power board can connect together to get the original power.

- Communication board: An FTDI serial board with a USB jack so I can connect the sculpture to the computer more easily. (Update: I found there're some issues about getting power from the type C connecter to the type A USB jack, so I used the type A USB connecter in the end.)

{{< postimg url = "PCBs-02.jpg" alt ="Left: stepper motor board, Middle: power supply, Right: FTDI board" imgw ="90%" >}}

#### Microcontroller interfacing and programming

The project uses Arduino IDE and TouchDesigner to program the boards. I tried the [**Interface and Application Programming week**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/interface/) with TouchDesigner, and this way looked promisiong so the main interfae will be made by TouchDesigner.

#### System integration and packaging

There are two parts about this project:

- Hardware Part: I plan to make each unit a modular and I can assemble the whole machine with screws and nuts, without glue or tape.

- Software Part: I hope to integrate all the controlling functions in TouchDesigner so it will be easier to work with sounds, images, and videos.


## Have I answered these questions?

Go through these questions to get clearer what I am doing:

#### What will it do?

It is an interactive and kinetic sculpture that can create different movements.

#### Who has done what beforehand?

Ther are some works have similar idea and stuctures, I put the note [**in this page**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note4/).

#### What will you design?

- An unit that has a up-down stucture and the rotation speed and direction can be controlled by programming.
- A machine that combines 16 units as a matrix and it's easy to connect to the computer and the power.
- An interface that can control this machine easily.

#### What materials and components will be used?

( 0609 update: component list)

I noted in the [**Final Project Component List**](https://docs.google.com/spreadsheets/d/1he1XG7m5g6ZXyFmROtBFitZXZmy-xyhiORy6xx5Xq28/edit?usp=sharing).

#### How much will they cost?

According to the [**Final Project Component List**](https://docs.google.com/spreadsheets/d/1he1XG7m5g6ZXyFmROtBFitZXZmy-xyhiORy6xx5Xq28/edit?usp=sharing), the total cost of the material is **546.4€**.

#### Where will they come from?

All the components and materials are from [**Aalto Fablab**](https://studios.aalto.fi/fablab/), only few screws with special size that I got from my own box.

#### What parts and systems will be made?

- A 4 x 4 kinetic matrix sculpture.
- A controlling interface.

#### What processes will be used?

- 3D model building
- 3D printing
- Laser cutting
- Circuit design
- PCB milling and soldering
- Programming
- Metal cutting by EDM machine

#### What questions need to be answered?

Can I visualize the digital signal and data (e.g. video, music, real-time data from a sensor... and so on) with a mechanical structure in a more natural and interesting way?

#### How will it be evaluated?

I think there're still lots of parts of this project can be developed so there're differnt thing can be evaluated at different developing phases. 

Here is a list that I hope I can achieve these goals before the Digital Fabrication III:
( 0609 update: which parts I finished)

- All units can work at the same time. ✅​
- I can control the rotation speed and the direction of each unit. ✅​
- It has a complete appearence. ✅​
- The movement can act as a continuous wave or respond to the music. ✅​ ( ❌ can't respond to the music)
- The user can attach different things on top of each unit. ✅

And here is a list that I hope I can complete during summer time:

- Find a flexible material so the user can make it as a surface and attach on the top of all units.
- Contol by a sensor that people can interact with.
- A better apperance.
- Respond to the music.
- Wireless controlling (not sure about this haha)


