---
title: 'Input Devices'
foldername: 'input_device'
image: 'hall_effect-1.png'
date: 2022-04-28
author: 'Yuhan Tseng'
tags: ['PCB', 'sensor']
categories: ['NOTE']
draft: false
---

This week we tried out different sensors and read the data from the sensors through the board we made. I didn't have a clear idea about using what kind of sensor in my final project, so I chose some sensors I haven't used before.

## Goal

:point_right: Design a microcontroller board with a sensor of your choice. Make sure the board has a working FTDI connection.

:point_right: Program your board to read values from the sensor and send them to your computer.

## Overview

The main task of this week was **communicate our board to the computer** so the [Serial Board](https://yuhantyh.gitlab.io/digital-fabrication/assignments/electronic_design/#d11-bridge-serial-board) that I made during Electronic Design week would be important. Because I usually used ATtiny series, the process of reading the data from sensors will be:

1. Connect the sensor to the main board which is using ATtiny series as microcontroller.
2. Upload the code to the main board by **UPDI programmer**.
3. Connect the main board to [**FTDI Serial Board**](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c).
4. Now I can send the data from sensor --> main board --> Serial Board --> computer.

The sensors I chose for this week were:

- Magnetic Field sensor [**TLE493D**](https://www.infineon.com/dgdl/Infineon-TLE493D-W2B6-DataSheet-v01_20-EN.pdf?fileId=5546d46261764359016189ec158943a4) : It can detect the magnetic field in X,Y,Z three aixs by **hall effect**.

- Joystick : This one was not new to me, I used it to test the basic connection of FTDI serial between my mainboard and the computer, and also used it to practice some data visualization.

- Capacitive sensor : I saw **step respone** at the Fab academy website first but I can not fully understand how it works. Then I knew capacitive sensor from [Nadieh's documentation](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-11/#choosing-the-sensors), which seems to easier to understand 😂 I'm not sure it's the same principle of step respone.

## TLE493D (XYZ Hall Effect)

{{< postimg url = "tle493d.JPG" alt ="XYZ hall effect sensor hello board" imgw ="70%" >}}

TLE493D is a sensor that can detect the magnetic field in X,Y,Z three differnt directions ( and from the datasheet, it seems to detect the temparature as well? 😳 ). One feature of it is using **I2C** interface so all the data can send out only by **2 pins (SDA & SCL)**.

### Circuit

I followed the example board from [Fab academy](http://academy.cba.mit.edu/classes/input_devices/mag/TLE493D/hello.TLE493D.t412.png) also the application circuit from the datasheet, redesigning a board with ATtiny412.

{{< postimg url = "tle493d_schematic.png" alt ="The schematic of the board" imgw ="80%" >}}
{{< postimg url = "tle493d-2.png" alt ="Circuit arrangement of the board" imgw ="70%" >}}

### Testing

Through I went through the video about how I2C works, I still have many parts need some time to understand 😅 I don't know how to write a code in Arduino IDE to get the data from this sensor by myself.

I the beginning, I tried to use the [arduino file from Fab academy website](http://academy.cba.mit.edu/classes/input_devices/mag/TLE493D/hello.TLE493D.t412.ino), I uploded the code successfully by UPDI programmer, connecting the board to computer by Serial board, and then there was nothing happen...... I spent few hours trying to find the reason and finally I found that

**TX & RX pins were connected to the wrong places!** 😂 😂 😂 😂 The reason was because the library of the FTDI connector had updated with the more simple pin order but I didn't notice that and still use the old way to connect them. So I made a quick fix by adding additional wire (actually I could just used some jump wires to connect this board to the FTDI Serial board).
However, this order of TX & RX pin is used when you use both FTDI pinheader and FTDI socket from fablab Kicad library.

{{< postimg url = "fix_rxtx-02.png" alt ="Try to fix the wrong connection" imgw ="80%" >}}

Now there were something show in the serial monitor! But It's obviously not the right data and it also didn't change when I put a magnent close to the sensor 😂

{{< video src = "xyz_hallEffect_fail.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

So I asked Matti for help 🙏 After the explaination about this code file and also other code files on the website, I realized that:

- This [Arduino code file](http://academy.cba.mit.edu/classes/input_devices/mag/TLE493D/hello.TLE493D.t412.ino) deals with the data in the form of byte, which is easier and lighter for the computer and programmer. However, it's hard for human to read. In addition, we need to use this file with python code file. Upload this code to the board. **Add a little delay at the end of the code.**

- This [python code file](http://academy.cba.mit.edu/classes/input_devices/mag/TLE493D/hello.TLE493D.t412.py) receives the data from Arduino code and creates the visual part. Its purpose is to visualize the data from the sensor. There're something need to pay attention to use this python code:
  - Make sure you have **Python 3** on your computer.
  - Make sure you have the Python libraries used in the code: [tkinter](https://docs.python.org/3/library/tkinter.html) & [serial](https://pyserial.readthedocs.io/en/latest/pyserial_api.html)
  - Make sure you connect the board to the computer with FTDI Serial board.
  - Open the terminal, go to where the python file is -> run `python3 [the python file name] [name of the serial port you use]`

Now the interface showed up as the example video! But still no responding circle or something show in the window 🥲 Fourturantly before I was thinking to make a new board, Yikun told me I could try to change a new sensor first because I could upload the code which meant that my basic circuit was fine.

And finally!!

{{< video src = "hallEffect_good.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Also tried to read the value in arduino IDE, but I haven't understand the rule of these value and how to filter process them.

{{< video src = "hallEffect_good2.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Here is the code:

```c

#include <Wire.h>

#define address 0x35

void setup() {
  //
  // start serial and I2C
  //
  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(400000);
  //
  // reset TLE493D
  //
  Wire.beginTransmission(address);
  Wire.write(0xFF);
  Wire.endTransmission();
  Wire.beginTransmission(address);
  Wire.write(0xFF);
  Wire.endTransmission();
  Wire.beginTransmission(address);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.beginTransmission(address);
  Wire.write(0x00);
  Wire.endTransmission();
  delayMicroseconds(50);
  //
  // configure TLE493D
  //
  Wire.beginTransmission(address);
  Wire.write(0x10);
  Wire.write(0x28);
  // config register 0x10
  // ADC trigger on read after register 0x05
  // short-range sensitivity
  Wire.write(0x15);
  // mode register 0x11
  // 1-byte read protocol
  // interrupt disabled
  // master controlled mode
  Wire.endTransmission();
}

void loop() {
  uint8_t v0, v1, v2, v3, v4, v5;
  //
  // read data
  //
  Wire.requestFrom(address, 6);
  v0 = Wire.read();
  v1 = Wire.read();
  v2 = Wire.read();
  v3 = Wire.read();
  v4 = Wire.read();
  v5 = Wire.read();

  int x = (v0 << 4) + ((v4 & 0xF0) >> 4);
  int y = (v1 << 4) + (v4 & 0x0F);
  int z = (v2 << 4) + (v5 & 0x0F);

  Serial.print(x);
  Serial.print(",");
  Serial.print(y);
  Serial.print(",");
  Serial.println(z);

  delay(100);
}
```

## Joystick

I made a ATtiny1614 mainboard during the Output Devices week. This board have 8 I/O pins so I can connect different sesnors to try. I found some Joystick modules at my room so I decided to try them out and also check if this mainboard is fine to connect with FTDI.

This type of joystick module is mainly combined by 2 potentiometers and 1 button, [this website](https://lastminuteengineers.com/joystick-interfacing-arduino-processing/) has a clear explanation and some example codes.

It can use a simple analogRead to get the value of X & Y:

```c
void setup() {
  Serial.begin(9600);
}

void loop() {
  int x = analogRead(9);
  int y = analogRead(8);
  Serial.print("x:");
  Serial.print(x);
  Serial.print(" y:");
  Serial.println(y);
}
```

{{< video src = "joystick_test1.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

And I changed a little in Arduino code and used with Proccessing to practice visualization.

```java

import processing.serial.*;

Serial myPort;  // Create object from Serial class
String value = null;  // Data received from the serial port
int index, start;
float x_v, y_v;
int x_step, y_step;
int speed = 15;

void setup()
{
  size(800, 800);
  noStroke();
  noFill();
  String portName = "/dev/cu.usbmodem143101";//name of the serial port

  myPort = new Serial (this, portName, 9600);
  myPort.bufferUntil('\n');

  //put tha red circle at the center of the canvas at the beginning
  x_step = width/2;
  y_step = height/2;

}

void draw()
{
  getInput();
  background(255);
  fill(255, 0, 0);
  moving();
  ellipse(x_step, y_step, 50, 50);
  delay(50);
}

//when get data from Arduino
void serialEvent (Serial input)
{
  value = input.readStringUntil('\n');
}

// analyze the data string
void getInput()
{
  if (value != null)
  {
    String value_2 = value;
    index = value.indexOf(",");
    if (index > 0)
    {
      x_v = float(value.substring(0, index));
      value_2 = value.substring(index+1);
      y_v = float(value_2);
      //println(x_v, y_v);
    }
  }
}

//move the red circle
//change the threshold can make the sensor more sensitive or less
void moving() {
  if (x_v > 900) {
    x_step += speed;
    if (x_step>width) {
      x_step= width;
    }
  } else if (x_v < 100) {
    x_step -= speed;
    if (x_step<0) {
      x_step= 0;
    }
  }

  if (y_v > 900) {
    y_step += speed;
    if (y_step>width) {
      y_step= width;
    }
  } else if (y_v < 100) {
    y_step -= speed;
    if (y_step<0) {
      y_step= 0;
    }
  }
}

```

{{< video src = "joystick_test2.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

## Capacitive Sensor

From the lecture and many documents, step response sensors and capacitive sensors look like having many applications and variations. I was interested in [this touched pad](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html) because it looked like something similar to the LED matrix, but I didn't have enough time to explore it. After searching more information, I knew about [**capacitive sesor**](https://playground.arduino.cc/Main/CapacitiveSensor/) and watched this [video](https://www.youtube.com/watch?v=bhspPkxOMxs). I felt it's not the same principle as step response sensor?

Check for the detatil about capacitive sensor 👉 [Arduino Tutorial: Touch and Capacitive sensing](https://dumblebots.com/2019/06/19/arduino-tutorial-touch-and-capacitive-sensing/)

Although the circuit is simple, I haven't fully understand the code which seems to be the key to get the right value.

I followed [Nadieh's documentation](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-11/#capacitive-touch) but kept getting the same value from the sensor 😓

Finally, I found the reasons and made the sensor act like a switch:

- The value of the resistor I used was too small. I didn't know why I mistooke the 1 ohm resistor which should be 1 megohm 😂 😂 😂
- Increase the delay time at the beginning of the code.

{{< video src = "capacitive_sensor.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

#### Note

- Computer may make some noise for the sensor if I test the sensor on my laptop.

{{< content >}}
