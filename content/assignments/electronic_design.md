---
title: "Electronic Design"
foldername: "electronic_design"
image: "pwmLed.gif"
date: 2022-03-10
author: "Yuhan Tseng"
tags: ["PCB","circuit design"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

This week we learned how to design a circuit with microcontroller by a software, and then made it in real by milling machine. Also we learned how to test the circuit by multimeter and oscilliscope.

## Goals

:point_right: Check operating voltage on the board with multimeter or voltmeter.

:point_right: Use oscilliscope to check noise of operating voltage and interpret a data signal.

:point_right: Redraw one of the echo hello-world boards or equivalent and add (at least) a button and a LED (with current limiting resistor) or equvivalent input and output

:point_right: Check the design rules, make it, test it.

## Design and Make a PCB

Based on [Hello ATtiny412 board](https://gitlab.fabcloud.org/pub/programmers/hello-attiny412), I added one more LED and one button. Then used **KiCad** to draw and design the circuit.

{{< postimg url = "helloCat_ATtiny412.JPG" alt ="helloCat_ATtiny412 board" imgw ="60%" >}}

### Electronic Components

Here are the components I used:

##### ATtiny412

ATtiny412 is a microcontroller. Actually I haven't fully understand what it is and always get lost in its [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf). I think it as the brain of the PCB board and decide the main function of that board.

##### Capacitor (1uF)

Capacitor can store electronic energy but uses the different way of the battery. Due to this property, it is also used to make the circuit stable and reduce the noise of the circuit. In this hello board, we used it before the ATtiny412 so the voltage and current are more stable when go through the microcontroller.
{{< postimg url = "capacitor_symble.jpg" alt ="we used unpolarized capacitors which don't have specific direction" imgw ="60%" >}}

##### LED

Its full name is **light-emitting diode** which is a type of diode. It only has **one direction** which means that you need to make sure you connect the right way of positive and negative of the LED, or you may break it. I used two different color of LEDs in my hello board and found that there are different ways to tell the direction of LEDs:

{{< postimg url = "leds.JPG" alt ="how to tell the direction of LEDs" imgw ="70%" >}}

##### Resistor (1K)

Resistor can be used to control current or voltage of a part of the circuit because it follows Ohm's law: ```V = I x R``` => ```R = V / I```

In this hello board, I used it to reduce the voltage that passes by the LEDs. Actually, I just followed FabLab's Hello Attiny412 component list,  instead of calculating the value by myself. I used 1KΩ for each LED. And then I realized that I can use smaller resistor to make LEDs lighter because:

**the value of resistor = (Power Voltage - LED's forward voltage) / LED's IF**

{{< postimg url = "led_datasheet.jpg" alt ="property of LED we use in FabLab" imgw ="60%" >}}

R = (5v - 1.8v) / 0.02A = 160 Ω (20mA = 0.02A)

And I'm curious why the hello board of FabLab used 1KΩ resistor 🤔 After asking, I realized that if the circuit provide the max voltage to LED, it would be very very light, but sometimes LEDs are used as indicators that don't need to be so bright. Also from [the explanation](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-6/#leds-and-diodes) from Nadieh's website, I know that if I use the max voltage is dangerous for LEDs.

##### Socket and PinHeader

Record these two components just because I mixed up them at the bigging and used the wrong one😂

{{< postimg url = "socket.JPG" alt ="socket and PinHeader" imgw ="60%" >}}

### Design Circuit: KiCad

We use [KiCad](https://www.kicad.org/) to design circuits and arrange the components on the PCB board. A KiCad project usually contains two files: {{< hightlight color = "#FFFF00">}}.kicad_pcb{{< /hightlight >}} & {{< hightlight color = "#FFFF00">}}.kicad_sch{{< /hightlight >}}, we start from .kicad_sch file.

Before starting to draw the circuit, we need to import FabLab's component and foodprint library:

**1.** Download this [repository](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) and put the folder in the proper location of your computer.

**2.** Open KiCad >> Preferences >> Manage Symbol Libraries >> add a new library path (select **.kicad_sym**)

**3.** Preferences >> Manage Footprint Libraries >> add a new library pathe (select **fab pretty** folder)

**4.** Preferences >> Configure Paths >> add the path of the folder of repository

So now we have the symble of the components we need!

##### Schematic Editor

Firstly, I started by drawing an electronic schematic. The electronic schematic is different from the final arrangement of the components, but you can check how the circuit works in a more tidy way. Here are some tips:

💡 Use a mouse, or you can go {{< hightlight color = "#00D2ED">}} KiCaD >> Preferences >> Mouse and Touchpad >> Uncheck "Center and warp cursor on zoom"{{< /hightlight >}}, now you can use laptop's touch pad to zoom in and out, also use two fingers to move left or right (not sure is only for Macbook)

💡 Choose a symble >> Right kick >> Show Datasheet, then you can see the detail about this component.

💡 **Shortcut**

- ```w``` : Add a wire. It will start the wire at the point where you press "w", duble click to end the wire.
- ```r``` : Rotate the components.
- ```a``` : Add a new symbol.
- ```Cmd + l```: Add a global label.
- ```q```: Add a un-connection flag.

I mainly follow the tutorial video and added one more LED and one button:

{{< postimg url = "kicad_2.jpg" alt ="the schematic graphic of the hello board" imgw ="80%" >}}

**5.** **Annotate schematic** to make each component have a number.

**6.** **Electrical rules checker** to check the circuit's basic logic. If the result is no error then I can go to the next step.

**7.** **Open PCB in board editor** and start to arrange components on the board.

{{< postimg url = "kicad_3.jpg" alt ="the toolbar of Schematic Editor" imgw ="80%" >}}

##### Board Editor

In board editor, we can arrange the components and design how the circuit goes. It spent more time than I thought.

{{< postimg url = "kicad_4.jpg" alt ="the schematic graphic of the hello board" imgw ="60%" >}}

Here are some note about using board editor:

- Use smaller grid is more easily to adjust the components' position.

{{< postimg url = "kicad_5.jpg" alt ="the grid setting of Board Editor" imgw ="60%" >}}

- I followed [this video](https://www.youtube.com/watch?v=VEf9hRK-8o0) and learned that I can use **black .dxf** file to import a customized shape as the board's outline:

    File >> Import >> Graphics >> select the file, make sure you import in **Edge Cuts** layer, adjust the size. (I don't know how to adjust the size after importing)

    {{< video src = "kicad_6.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

- Use **Update PCB from Schematic (a.)** to update the circuit and the components if there are some changing in the Schematic Editor; use **DRC Control (b.)** to check if there any problem about the board arrangement.

{{< postimg url = "kicad_7.jpg" alt ="the toolbar of board editor" imgw ="90%" >}}

- Export gerber files:
    1. Press **Plot** on the toolbar
    2. Select the file location.
    3. Select the layers you want to use. For this board, I only designed one side so I chose **F.Cu, F.Mask, and Edge.Cuts**.
    4. Check "Plot Edge.Cuts on all layers" >> Plot

    {{< postimg url = "kicad_8.jpg" alt ="plotting setting" imgw ="80%" >}}

##### ✏️ About Button #####

At the beginning, I added a [pull-down resistor](https://circuitdigest.com/tutorial/pull-up-and-pull-down-resistor) between the button and the ATtiny412, and I struggled with the wire arrangement 😂 After asking other people, I understand these things which made my circuit more simple:

- The button we used has interior connecting between pins (pin4 & pin3, pin2 & pin1) so I don't need to connect them (button & button, GND & GND) in Board Editor.
 {{< postimg url = "kicad_9-02.jpg" alt ="button we used" imgw ="80%" >}}

- ATtiny series have the internal pullup resistor so I don't need to add by myself. From [**Adrián's website**](https://fabacademy.org/2020/labs/leon/students/adrian-torres/week08.html), there're detailed and organinzed information about different ATtiny microcontrollers.

### Make a PCB Board

After getting the gerber files, I could do what we have learned from electronics production week to make my design into a real PCB board. I used [SRM-20 milling machine](http://localhost:1313/digital-fabrication/assignments/electronics_pro/electronics_pro/#srm-20-milling-machine) to make this board.

{{< postimg url = "cat_board.JPG" alt ="making hello board" imgw ="60%" >}}
And then soldered it!

### Program Hello PCB

Last time in electronics production week, I kind of tried to flash and program the board I made, but still didn't understand what I was doing 😂 In this time, I felt clearer about what each step is for (but still haven't fully undersatand yet).

##### Make UPDI D11C as Programmer

I made a [UPDI D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c) last time and Kris helped me flashing bootloader by SWD programmer. But it still have a **USB-to-serial firmware** to make it be able to programme ATtiny Hello board I made for this time.

Follow the [gitlab README page](https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c#loading-usb-to-serial-firmware), I got the sketch for UPDI D11C board [here](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino), following [this instruction](https://github.com/qbolsee/SAMD11C_serial) and uploaded the code to UPDI D11C board by Arduino IDE.
(screenshot?)

Now I can use this UPDI D11C to code my Cat-Hello board!

##### Programme Hello Board

Check out the ATtiny412 pinout to know what the pin number are used for button and LEDs.
{{< postimg url = "attiny412-pinout.jpeg" alt ="ATtiny412 pinout: know more about each pin of ATtiny412 " imgw ="80%" >}}

1. Connect hello board to UPDI D11C by UPDI.
    {{< postimg url = "code_helloboard.JPG" alt ="connect hello baord and programmer" imgw ="80%" >}}
2. Use the setting like this: (make sure the Port is selected to UPDI D11C board)
    {{< postimg url = "code_helloboard2.jpg" alt ="setting of Arduino IDE" imgw ="60%" >}}
3. I tried to set 3 modes of LEDs and change the mode by pressing button:
    - Mode 1: Turn on 2 LEDs
    - Mode 2: 2 LEDs take turns to blink
    - Mode 3: 2 LEDs blink together

```c++
#define LED_1 3
#define LED_2 2

int pressCount = 0;

void setup() {
  Serial.begin(115200);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(4, INPUT_PULLUP);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

}

void loop() {
  if (!digitalRead(4)) {
    pressCount ++;
  }

  if (pressCount == 1) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, HIGH);
  }
  else if (pressCount == 2) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, LOW);
    delay(100);
    digitalWrite(LED_2, HIGH);
    digitalWrite(LED_1, LOW);
    delay(100);
  }

  else if (pressCount == 3) {
    digitalWrite(LED_1, HIGH);
    digitalWrite(LED_2, HIGH);
    delay(500);
    digitalWrite(LED_1, LOW);
    digitalWrite(LED_2, LOW);
    delay(100);
  }

  else {
    digitalWrite(LED_1, LOW);
    digitalWrite(LED_2, LOW);
    pressCount = 0;
  }
  
  Serial.println(pressCount);

  delay(150);
}
```

4. Upload!

It looked worked! But actually there was a little problem about detecting button input because it supposed to change by pressing the button only once, and 2 LEDs would turn off after Mode 3.
{{< video src = "code_helloboard3.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

### D11 Bridge Serial Board

Kris mentioned bridge serial board can be used to connect ATtiny412 hello board and the computer, which means that I can send out some signals from the hello board and check the signals on my computer. The original [D11 bridge serial board](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c) from FabLab uses board itself as USB connector that we usually need to add some tapes on that part to make it thicker so it can be plugged in the USB port directly.

{{< postimg url = "serialD11c-3.JPG" alt ="D11 Bridge Serial Board" imgw ="60%" >}}

##### Schematic and Board arrangement

Fortunately that Kris added a new USB footprint into FabLab so we don't need tape anymore 🙌🙌🙌 To use this USB footprint, I adjusted the KiCad file of this board:

{{< postimg url = "serialD11c-1.jpg" alt ="displace Conn_USB_A_Plain to Conn_USB_A_Plug" imgw ="70%" >}}
{{< postimg url = "serialD11c-2.jpg" alt ="make sure the arrangement of USB footprint" imgw ="70%" >}}

### Double-side PCB

Because the holes for USB footprint, I needed to make a double-side PCB for this time. Different with making hello board, I also plotted backside file and drill file.

{{< postimg url = "double-side.jpg" alt ="also plot B.Cu and drill file" imgw ="70%" >}}

For the milling machine part, I needed to use drilling bit. {{< hightlight color = "#FFFF00">}}Drilling bit only can move vertically {{< /hightlight >}} or it would be broken. So if I have some size of the holes are not fit to the drilling bit I can use 1mm cutting bit to do it. Another important point is that **the shape of the board needs to be symmetry**

TBC......

## Testing PCB

### Multimeter

A common way to check if your PCB is fine is using [**multimeter**](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all), which can measure the voltage, current, resistance, and the continuity of the circuit.

##### Testing Voltage

I tested the voltage from the computer USB port, which was around 5,22v. It's a little too high to the ATtiny chip. I think that's why people recommand don't connect the board to the computer directly, and it's better to use a USB hubs.

{{< postimg url = "test_voltage.png" alt ="testing the voltage from USB port" imgw ="70%" >}}

##### Testing Continuity

Another useful measurement for debugging is **continuity**, you can know if your circuit connect wrong or miss some parts. When the two checking points is connecting, the multimeter will make a sound.

{{< postimg url = "multimeter.JPG" alt ="the symbol of testing continuity" imgw ="70%" >}}

{{< youtube B5ZvQquXtiU "16:9" "70">}}

### Oscilliscope

TBC...

##### PWM Signal

I tried to use analogWrite to create PWM signal and changed the brightness of LEDs.

```c++
#define LED_1 3
#define LED_2 2

int light = 0;

void setup() {
  Serial.begin(115200);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(4, INPUT_PULLUP);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

}

void loop() {
  light ++;
  if(light>255){
    light = 0;
  }
  analogWrite(LED_1,light);
  analogWrite(LED_2,light);
  Serial.println(light);
  delay(20);
}
```

{{< youtube Uej1sCKCigo "16:9" "80">}}

## Debugging Time

Cat Hello board became wierd while I was trying to test it by the oscilliscope. The code worked but only blue LED, and the red LED kept lighting when I connected the board to the D11 bridge serial board, no matter how many times I pressed the button.

{{< postimg url = "brokenLed-03.jpg" alt ="LEDs behave wierd during I tested the board" imgw ="80%" >}}

Because the board acted quite normal at the beginning, I didn't think that's the circuit's (soldering) problem. Instead, I thought it was caused by coding problem. So I tried to:

- Reflash all the boards: UPDI D11C & D11 Bridge Serial board.
- Reupload the code by Arduino IDE.
- Check the sequence of UPDI and FTDI connection were correct.

But that didn't work 🥲🥲🥲🥲 So I asked Matti for help. He started from checking **the continuity** of the circuit, especially from {{< hightlight color = "#FFFF00">}} VCC & GND parts {{< /hightlight>}}. Then we found that **the GND were not all connected.** After getting a closer check, there was a very little part of copper layer was broken!

{{< postimg url = "debug2-04.jpg" alt ="debugging process" imgw ="90%" >}}

{{< tipblock >}}
<b>Hot Air Rework Station</b></br>
Because the broken part was covered by the pinHeader of FIDI part, I needed to take out this part. Matti showed me how to use hot air gun to take out soldered component more easily and faster. The main priciple is that you hold the component and heat up all the pins together by the hot air gun, when the soldering parts are melted, the board will fall down automatically due to the gravity.
{{< / tipblock>}}

{{< postimg url = "hot_air_gun.JPG" alt ="Hot air rework station in FabLab" imgw ="60%" >}}

So I sodered that part and the cat recovered to two eyes again! 🎉🎉🎉🎉🎉
{{< postimg url = "debug3.JPG" alt ="debugging process" imgw ="80%" >}}
{{< postimg url = "debug5.jpg" alt ="debugging success" imgw ="60%" >}}

I guessed the reason was because I scratched the copper tap when I tried to use little hook to test the TX/RX signal...
{{< postimg url = "hook.JPG" alt ="suspect" imgw ="70%" >}}

## Files

- [KiCad project of Hello-cat board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/electronic_design/Hello-Cat-ATtiny412)
- [Testing code (.ino)](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/electronic_design/test_code)
