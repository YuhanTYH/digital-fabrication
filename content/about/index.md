---
foldername: "about"
---

# About

{{< postimg url = "cats.gif" alt ="HELLO" imgw = "30%">}}

Hi, I am Yuhan Tseng. I am studying New Media Design and Production at Aalto University in Finland currently. My background is Technology Education and I am interested in interactive design and installation.

This website is the documentation of my journey in [**Aalto FabLab**](https://studios.aalto.fi/fablab/). I was very lucky to take a 6-mouth course that is organized by the Aalto Fablab during 2021-2022. I learned how to use various machines and the techniques of digital fabrication.

You can see what I have learned and played around in the [**Assignments**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/). In the [**Final Project**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/), you can see the development of my final project: Wave Machine.

Have FUN!

My religion is Cat :)
