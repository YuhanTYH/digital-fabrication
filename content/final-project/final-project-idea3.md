---
title: "Final Project Idea-3: Artificial Creature"
foldername: "final-project"
image: "idea3_heroImg.jpg"
date: 2022-03-21
author: "Yuhan Tseng"
tags: ["Idea"]
categories: ["NOTE"]
draft: false
---

## Concept

Inspired by Enbodied Interaction course, I came out with another idea about interaction installation that I really want to try.

It is a dynamic sculpture with multiple tentacles. There has a projector projects different parts of face on the sculpture. When the audiences pick up and move one of the tentacles, the sculpture chaning its shape with organic movement.

{{< postimg url = "idea3_sketch-02.jpg" alt ="project sketch" imgw ="90%">}}

## References

This project mainly inspired by two great artists: [**Tony Oursler**](https://tonyoursler.com/) and [**Eevi Rutanen**](https://www.eevirutanen.com/).

### 3D Objcet Projection

Tony Oursler has many amazing works are made by projecting on different 2D and 3D objects, here are some my favorite works of him:

{{< youtube 5HjR1X0a4e4 "16:9" "70">}}
{{< postimg url = "tony_1.jpeg" alt ="Tony Oursler's work" imgw ="90%">}}
{{< postimg url = "tony_3.jpeg" alt ="Tony Oursler's work" imgw ="90%">}}

In Audio Visual and Embodied Interaction course we'll learn how to project and map the images on object with TouchDesigner.

### Creapy Creature

I knew Eevi Rutanen during the orientation week when I came to Aalto. She gave us a speech and I am fascinated with her creatures and color.

{{< vimeo 359789029 >}}

### Dynamic Sculpture

I watched many dynamic movement effect made by TouchDesigner, Processing, Blender, and so on, and I very like that kind of movement. I'm curious that can I make it in realistic object.

{{< youtube YXUDgc2J-yk "9:6" "60">}}

## How to Do?

### Main Body of Sculpture

To control the transformation, I am thinking about putting multiple expandable structures under a flexible material so when each structure goes to a different height, the sculpture will become different shapes.

{{< postimg url = "idea3_3-04.jpg" alt ="an idea about dynamic transfoming" imgw ="80%">}}

Then I did a simple prototype to see if it has some posibility to do. The testing result was not too bad(?) I used DC motors in this testing, and I think I can make the changing more smoothly by using **step motors**. Also, the step motors might be quieter.😂 😂

{{< youtube nGwoYD4zr9k "16:9" "60">}}
{{< youtube 125Cxu-JUJc "16:9" "60">}}

I could not find a suitable flexible material so I used a plastic bag to do a rapid testing.😂
{{< youtube rDOSFPKDVQY "16:9" "60">}}
{{< youtube 1bpbAc60Rd0 "16:9" "60">}}

Matti told me I can try using **silicone** and also we'll learn about model and casting this week. I think this is a good oppotunity to explore the possible material for the scuplture's body.

### Tentacles

About tentacle, I'm thinking about putting some sensors inside it so it can detect how does audience move it. The movement of tentacle will affect the transforming of the sculpure's body.

{{< postimg url = "idea3_2-03.jpg" alt ="how to interact with it" imgw ="90%">}}

### Projection

I'll learn how to map the projection on the 3D object in the next few weeks by TouchDesigner.


{{< content >}}
