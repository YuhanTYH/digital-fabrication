---
title: "Note: 3D Mapping Projection"
foldername: "final-project"
image: "mapping_heroImg.png"
date: 2022-04-04
author: "Yuhan Tseng"
tags: ["3D","Lasser Cutting"]
categories: ["NOTE"]
draft: false
---

I [changed my final project](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-idea3/) last time. To get start, I divided it into few parts to explore:

- Mechanism
- 3D projection
- UI and UX design
- Combine all the parts together

Because I am taking two courses that teach us about interaction design by [**TouchDesigner**](https://derivative.ca/), also I have another project plans to project something on a 3D object, I spend some time to explore the projecting mapping during these two weeks. This page may be more about my another project but I learned a lot about 3D mapping during this process, and I hope I can apply it to my final project of FabLab.

## Goal

The goal of 3D mapping part is to project the visual stuff perfectly on the surface of an 3D object. Because in my FabLab final project, I hope to do some movement of the object's surface, the surface will be like organic shape and keep changing.

In this project, I tried to make a sculpture in an irregular shape. I collected some recycled bottles and plastic bags to make it and actually I didn't have any idea what the outcome should be looked like 😂

{{< postimg url = "sculpture_test-02.jpg" alt ="making process" imgw ="80%">}}

## 3D Mapping in TouchDesigner

I haven't used this software before untill take the course. I feel it's a powerful software when I start to learn it. I learned some mapping tools in TouchDesigner during the class, and I think these two may be sutible for my project:

### CamSchnappr

[CamSchnappr](https://docs.derivative.ca/Palette:camSchnappr) is a mapping application, especially for 3D structures. It works with **the virtual 3D model** of the physical structure that you want to project on, so when you align multiple points from the 3D model to the physical object, the application can calculate the exact position, size, and angle to project the image.

Because I need a 3D model of my sculpture, I used 3D scanner to get the model and texture. Compared to the experience of the assignment last time, I feel that the 3D scanner is more sutible to the big object.

{{< youtube U3gWMYJcIuI "16:9" "70">}}

Then I followed this tutorial and tried to apply it to my object, but somehow the computer kept crashing when I tried to connect the mesh to the CamSchnappr. Also here recorded something that haven't understand:

- Impot 3D object file with .obj and .fbx in TouchDesigner will get differnt operators and structure. And Artec Studio can't export .fbx file, so I tried to import the 3D model in Blender and then export as .fbx file.

- Can not apply the texture, which is created in Artec Studio, to Geometry model directly.

{{< youtube zzwzM-JbpM8 "16:9" "70">}}

{{< postimg url = "camschnappr.JPG" alt ="TouchDesigner got stuck whenever I tried to connect the mesh to Cam Schnapp" imgw ="60%">}}

### Kantan Mapper

[Kantan Mapper](https://docs.derivative.ca/Palette:kantanMapper) is a mapping tool for 2D shapes. By creating different 2D polygons and editing them with bezier tool, you can project images at a particular position in a special area. The operation of Kantan Mapper is very straightforward and easy to understand for me.

Try to limit the projecting area on the 3D object:
{{< youtube MA4bRX_2ybY "16:9" "70">}}

And also I found this tutorial that teach you how to use **Kantan UV Helper** to make the process run more efficiently:
{{< youtube ndC8ODwwQnw "16:9" "70">}}

The cook time are quite different between Kantan Mapper and GLSL
{{< video src = "low_cooktime.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

Testing the using process:
{{< youtube tqXVgB_rZPM "16:9" "70">}}

Testing different projected content:
{{< youtube -MvvwL5_bR0 "16:9" "70">}}

## Some Notes and What Next?

### About physical object

I realized that the surface of the sculpture I used is too irregular 😂 Although I can adjust the projected angle, shapes, or perspective by TouchDesigner, there're still some gaps that light can't reach, particularlly only one projector.

- The shape of object can be organic or irregular but need to pay attention to if there some big angles between faces. Also keep in mind that light always go straight.

- 3D scanner is good to scan big object so I can use it to create 3D model instead of making by myself.

- Next: Try to make the object for the final project. May use silicon or other flexible material.

### About mapping tool

- Keep trying CamSchnappr on TouchDesigner.
- Try to use different projectors: I used a little projector for this time. It seemed that it needed to be far from the object, so I wonder if different types of the projector can get closer to the projected object.
- Try different positions of projector.

### About mechanism

- For this week assignment, I'll try to make some stepper motor drivers and learn how to control multiple stepper motors.


{{< content >}}
