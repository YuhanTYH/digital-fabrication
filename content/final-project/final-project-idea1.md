---
title: "Final Project Idea-1: Moon Blocks"
foldername: "final-project"
image: "moonblock_heroImg.png"
date: 2022-01-24
author: "Yuhan Tseng"
tags: ["Idea"]
categories: ["NOTE"]
draft: false
---

## Concept

Moon blocks are common divination tool in traditional Chinese religion. People use it to ask questions to god, hoping god to give them some guidance with Yes or No answer. It is usually used with lottery poetry to get the more detailed instruction. This custom makes me think of people searching for answers on the Internet nowadays. I hope to make a digital one and make people on the Internet do God's job in this process.

{{< postimg url = "throwBlocks.gif" alt ="throwing moon blocks" imgw ="60%">}}
{{< postimg url = "moonblock_result.jpg" alt ="different results" imgw ="90%">}}

Also, I hope to learn and be familiar with the following skills:

- making a shell that can contains electronics components and circuits in a stable way.
- be familiar with accelerometer and gyroscope sensor.
- be familiar with connecting data from the Internet to microcontroller.
- making a personal PCB.
- be familiar with RFID technology.

## Some ideas about the structure

{{< postimg url = "moonblocks.jpg" alt ="project structure" imgw ="90%">}}

## How to use

1. Hold the moon blocks and move around the result source you choose.
2. Think about the question in your mind. Only one question which can be answered with Yes or No.
3. Hold the moon blocks on your hand, and throw out the moon blocks on the ground.
4. See the results and hear the detailed answer.
{{< postimg url = "steps.jpg" alt ="using steps" imgw ="90%">}}

## References

- [A Fortune Telling Bird](http://www.kimjinhee.com/A-Fortune-Telling-BIrd): I'm mainly inspired from this project.

- [Moon blocks introduction](https://en.wikipedia.org/wiki/Jiaobei) from wikipedia.
