---
title: "Note: Mechanism Part"
foldername: "final-project"
image: "mechanism_heroImg.jpg"
date: 2022-04-24
author: "Yuhan Tseng"
tags: ["Motor","Kinetic"]
categories: ["NOTE"]
draft: false
---

{{< content >}}

Recently I searched for some references about the mechanical part and found some interesting works that may be helpful.

## Goal

The goal of the mechanical part is to make the surface of the sculpture move dynamically and smoothly. To show various shapes and make the movement naturally and smoothly enough, I hope I can control the mechanism partially or unit by unit. Based on this goal, I found some cool works that used different structures to achieve similar outcomes.

Also, I learned how to control one stepper motor durinng the Ouput Device week, now my next step is trying to design a board to control multiple stepper motors at the same time.


## References

### Kinetic Wave Sculpture

{{< youtube _PFceuhw0NU "16:9" "70">}}

This is [Moath Momani's final project](http://fabacademy.org/2018/labs/fablabirbid/students/moath-momani/final_project.html) from 2018 Lrbid Fab Lab.

The most special part for me is that he only used **one stepper motor** to create this motion. Because the end of wire were attached on the circle's edge, the length of the wire changed when the circle spinned with the motor. I haven't fully undersatand how it worked Also, using little wood sticks to make a flexible chain (net) was clever.

I also found some parts I need to consider:
- This kind of design only have one type of movement.
- The direction of the sculpture is opposite to my final project, and this kind of design needs to be in this direction because it used gravity to pull up and down the chain.


### Point Cloud

{{< vimeo 42896836 >}}

</br>

This is [a kinetic sculpture](http://www.jamesleng.net/#/pointcloud/) which interact with a live feed of weather data. It's speed, direction of rotation, and smoothness will change with the data from current weather. It made by [James Leng](http://www.jamesleng.net/).

In this work, he used **servo motors** which seems to can be an option for me, too. The most impressive part for me is the sculpture is in a 3D shape and the whole structure looked very tidy and elegent.

He had another video which was about the different structures that he had tested. This video is very helpful to me and I plan to try out some stuctures from this video:

</br>
{{< vimeo 38959713 >}}
</br>
</br>


### Mechanical Duck Sculpture

{{< youtube QIZD-iSaPss "16:9" "70">}}

This sculpture was made by [David Cranmer](https://www.nervoussquirrel.com/index.html). The machnism part is a little similar with Point Cloud. And from the [documentation page](https://www.nervoussquirrel.com/duckmachinemakingof.html), he saied each motor used 2 pins so I guess he used **DC motors** in this project. But I don't know how he connect a potentiometer for each motor to get the position information 🤔


## Next Step

I think the mechanism of Point Cloud is closest to what I want to try because it has more possibility to develop into a 3D organic shape. Considering the accuracy of control and the force, I will try to use **stepper motor** first.

I designed a 4-stepper motor driver board with Attiny3216, using [TMC2208 motor driver chip](https://www.trinamic.com/fileadmin/assets/Products/ICs_Documents/TMC220x_TMC2224_datasheet_Rev1.09.pdf).

{{< postimg url = "multi_motor_board.png" alt ="Schematic of the motor board" imgw ="70%">}}
{{< postimg url = "multi_motor_board2.png" alt ="Circuit arrangement of the motor board" imgw ="70%">}}
{{< postimg url = "multi_motor_board3.JPG" alt ="I haven't finished the soldering part" imgw ="70%">}}

----
**2022.05.01 UPDATE**

I finished the stepper motor board, but somehow the 5V and GND were connected and I couldn't fine where they were connected 🥲 🥲 🥲 After lots of time to debug and removed some parts accidentally 😂 I gave up this one and decided to do another way: making many single stepper motor boards and make them communicated to each other.

{{< postimg url = "finished_multiMotors.JPG" alt ="I finished the soldering part but it didn't work 😂" imgw ="70%">}}

----
</br>
</br>

I also found there are few this kind of little stepper motors, which is much lighter and smaller than what we used during Machine Building week. So I will try with these motors this week.

{{< postimg url = "little_stepperMotor.JPG" alt ="Little stepper motor" imgw ="50%">}}




