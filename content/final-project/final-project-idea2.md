---
title: "Final Project Idea-2: Future Creature II"
foldername: "final-project"
image: "creature_heroImg.jpg"
date: 2022-01-27
author: "Yuhan Tseng"
tags: ["Idea"]
categories: ["NOTE"]
draft: false
---

## Concept

This idea came from my previous project ["Future Creature"](https://www.hackster.io/tseng2/future-creature-an-interactive-installation-5494ff), which imaged a future plant that grows with the artificial waste. So in this project, with the same concept and recycled materials, I want to make the creature more organic and more like an animals.

{{< youtube q5PHm4vBs8o "16:9" "60">}}

The main appearance is inspired from the robot spider which have done by many other people before. I am very interested in the mechanism of it and how it works, especially about the movement like the video below. I am also curious that if I can make this type of robot with natural or recycled materials.

{{< youtube HRv_WwoBy5Y "16:9" "60">}}

## Features and Sketch

- The creature can move freely in every 2D direction.
- When it detects some people close to it, it will move towards these people.
- There is a little material hang in each glass bottle, the little material will hit the bottle and make sound when the creature moves.
- There is a LED light in each glass bottle, it will light up when the little material hits the bottle.

{{< postimg url = "robot-spider1.jpg" alt ="robot spider sketch" imgw ="70%" >}}

{{< postimg url = "robot-spider2.jpg" alt ="robot spider sketch" imgw ="95%">}}

## Some parts haven't figure out

- How does it detect people: temperature, distance?
- How to move towards the objective (people)?
- The power source: rechargeable battery?
- How to light up the LED when the bottle is hit: vibration sensor, connect conductor?
- How to combine the part of bottles and the body?
