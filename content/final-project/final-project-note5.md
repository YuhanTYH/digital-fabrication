---
title: "Note: Mechanism Part2"
foldername: "final-project"
image: "up_down_stucture.jpg"
date: 2022-05-09
author: "Yuhan Tseng"
tags: ["Motor","Kinetic"]
categories: ["NOTE"]
draft: false
---
{{< content >}}


These two weeks I tried two things:
- The up-down structure with stepper motor.
- Control multiple stepper motors at the same time.


## Up-down Structure

I mainly followed [this mechnism](http://localhost:1313/digital-fabrication/final-project/final-project-note4/#point-cloud). I used Fusion 360 to make a model and tried to simulate the motion, but I found that although the structure moved perfectly in the software, there was still some bias when I make a real one.
I also tried to make by laser cutter and 3D printer, so far I found that the whole stucture made by 3D printer is more stable.

This the first version with laser cutter:

{{< video src = "v1_laserCut.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

It looked fine in the simulation but when I tried with the real material, it was just stuck when going up. I used screws and nuts to connect two moving sticks and I thought they might be one of the reasons and they also took up much unnecessary space.

{{< postimg url = "small_bearing.JPG" alt ="small bearing to save the space and make the movement more smoothly" imgw ="50%" >}}

Then I tought of that Kris said there're some small bearllings. I tried them and adjusted some stucture. This time I designed for 3D printer.

{{< video src = "v2_3dprint.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}

It looked nice in the simulation so I printed them out. It worked better them the first version but still not smooth enough 😂

{{< youtube rXxPrr-jxuY "16:9" "70">}}

I found the reason was that the track for the stick was too big, so then the stick went up, there was some space to make the stick tilt instead of going up straight. So I made the stick thicker and added a little piece to avoid the stick going out of the track. it looked nice this time 👍

{{< youtube EA3i7jFimSU "16:9" "70">}}

Then I was thinking can I use the laser cutter to make some parts? Because 3D printing needs more time and sometimes it's hard to adjust, especially in the testing phase. I changed the design of some parts to fit the laser cutter and designed the base for the stepper motor board.

The outcome looked not bad but it was not stable, sometimes the stick stuck at the track. I'm thinking is it because the plywood is too flexible or too weak, so it can not fix the stick in the track? 🤔 🤔

{{< youtube gXCL7eaajaU "16:9" "70">}}


## Contol multiple motors

Last time [I failed to make a board that used 4 stepper motors](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note4/#next-step). So I changed to make many single stepper motor boards and make them communicate with each other.

I managed to control two stepper motors at the same time in [the Networking and Communications week](https://yuhantyh.gitlab.io/digital-fabrication/assignments/network/#2-cat-paw-board--2-stepper-motors). But when I tried to connect 4 stepper motor boards and send out the command, I just smelled something burn and the motors didn't work 😂 😂 😂 😂 I have not found the reason, but I think maybe one way that I can try is **using multiple IDCs in one line** to reduce the separate connection.

{{< postimg url = "multi-boards.jpg" alt ="connect 4 stepper motors boards" imgw ="90%" >}}



### Another way?

- 4 motors use one driver?

{{< postimg url = "wave_machine.jpeg" alt ="wave machine" imgw ="40%" >}}
