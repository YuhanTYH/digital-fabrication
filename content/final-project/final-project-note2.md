---
title: "Note: Prototype of the moving part"
foldername: "final-project"
image: "prototype_heroImg.jpg"
date: 2022-02-27
author: "Yuhan Tseng"
tags: ["3D","Lasser Cutting"]
categories: ["NOTE"]
draft: false
---

To understand how the linkage structure works, I tried to make **a prototype of moving part** by laser cutting. The movement was triggered by a little gear motor and the four legs moved fine in the air. However, it **could not move forward when I put it on the ground**.
{{< youtube ZjvGOK4Gh-4 "16:9" "70">}}
{{< youtube Hobuq0o21iE "16:9" "70">}}

I came out with some potential reasons:

- [Torque](https://en.wikipedia.org/wiki/Torque): The torque is not big enough to drive the whole robot'body. Maybe the motor is not strong enough or the total weight is too heavy.

- The difference between the legs: the difference of the height of the legs between the two sides is too big and makes the body unbalanced.

## About Torque

I found the motor I used has a higher speed of rotation than the references. The rotating speed of the legs is the same as the motor because I used 1:1 ratio for the gears, and apparently that the torque wasn't enough.

{{< postimg url = "gears.jpeg" alt ="use 1:1 ratio for ghe gears" imgw ="60%">}}

Based on the video below, I can {{< hightlight color = "yellow" >}}decrease the gear teeth of the driver gear (the gear on the motor) or increase the gear teeth of the driven gear (the gear connects to the body's stick){{< /hightlight >}}, so I can increase the torque and decrease the speed.

{{< youtube txQs3x-UN34 "16:9" "65">}}

## About the Difference between the legs

I found that when the robot tried to walk on the ground, its body was easy to tilt to one side and then could not go back to balanced state again. At that moment, there are three legs were touched to the ground and only one leg raised up which theoretically had a plane to support the robot's body.

{{< postimg url = "walking.png" alt ="there're three points of legs on the ground but the body still tilts to the front" imgw ="70%">}}

The possible reasions:

- the weight distribution was not average, more weight was on the front part of the body.
{{< postimg url = "weight_distrubution.jpeg" alt ="the acrylic head and batteries are on the front part of the body" imgw ="60%">}}

- the rotating timing of the two sides was not appropriate. I used 180 degree as the difference between the two side. Maybe I can try to decrease the difference.
{{< postimg url = "two_sides.png" alt ="the positions of the legs at the same time" imgw ="80%">}}

## Multiple pairs of legs and gears

I think the problems I found from the prototype will be more obvious if I want to make a bigger version for my final project. According to the original strandbeest and many other people's robots, they have multiple pairs of legs which seems to help the body to keep balance during moving, so I plan to try a structure with multiple pairs of legs.

{{< youtube uVKkW7GO1nU "16:9" "70">}}
{{< youtube _er5PRGH8l4 "16:9" "70">}}

Also, I found that many versions connect two legs on different gears instead of one. It seems to be more stable and move more smoothly.
{{< postimg url = "two_gears.png" alt ="connect the legs on different gears and also the driver gear is smaller than the driven gear" imgw ="100%">}}


{{< content >}}
