---
title: "Final Mechanism"
foldername: "final-project"
image: "one_unit.jpg"
date: 2022-05-23
author: "Yuhan Tseng"
tags: ["Motor","Kinetic"]
categories: ["progress"]
draft: false
---
{{< content >}}

## Mechanism Part

After decideing the [**final mechanism design**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/wildcard/#what-i-made) druing wildcard week, I started to make the official parts for the final project. I need 16 units in total.

{{< postimg url = "final_design-02.jpg" alt ="the final design of the mechnism (one unit)" imgw ="80%" >}}

{{< video src = "moving_test.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

</br>

The frames of the motor and of the metal sticks are made by laser cutting with 3mm transparent acrylic.

{{< postimg url = "acrylic_frame.jpg" alt ="the frame of the moror and of the metal sticks" imgw ="80%" >}}

</br>

The base of a unit is made by 3D printing. (4 are prining now...)
{{< postimg url = "3dprint_base.jpg" alt ="the base of one unit" imgw ="80%" >}}

#### Progress so far: 7 / 16

{{< postimg url = "assemble.jpg" alt ="finished 7 units so far" imgw ="80%" >}}


## Electronic Part

Because each unit needs one stepper motor board to drive and control the stepper motor, I made 16 stepper motor board with ATtiny412.

{{< postimg url = "16motor_board.jpg" alt ="finished 16 stepper motor boards with ATtiny412" imgw ="80%" >}}


### Heating problem of motor boards and the motor

When I test the stepper motor with the motor board, some boards heated up very soon, especially at the regulator, ATtiny412, and the motor itself. I found there're two possible reasons:

#### Regulator

I found I used two different regulators for the stepper motor boards, one is [**NCP117**](https://www.onsemi.com/pdf/datasheet/ncp1117-d.pdf), and another is [**LM2940**](https://media.digikey.com/pdf/Data%20Sheets/Texas%20Instruments%20PDFs/LM2940(-N,C)_Sept-23-2013.pdf). I misconnect the power and ground because they have differnt pinouts, and that might be the reason of heating up.

{{< postimg url = "regulator_pin-03.jpg" alt ="pinout of regulators" imgw ="80%" >}}

#### Stepper motor current

During the [**Machine Building week**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/machine_building/#electronics-and-circuit), I adjusted the voltage that went through the G-shied by the potentiometer, so the current that went through the motor would not be too large. I didn't fully understand at that time and I got cleare conceptr at this time. Matti told me about how the motor works and [**this website, which is about Step-Stick**](https://learn.watterott.com/silentstepstick/).

The concept is that a motor is mainly driven by enough current, not enough voltage. So even with the enough voltage from the main power, if the current go through the motor isn't enough or too big, the motor can't run or may break. 

From the [**data sheet**](https://cdn.sparkfun.com/datasheets/Robotics/ST-PM35-15-11C.pdf) of this stepper motor, it needs 400mA (0.4A) current to operate. I followed the calculator from [**the website**](https://learn.watterott.com/silentstepstick/faq/#how-to-set-the-stepper-motor-current) and adjusted the output voltage by the potentiometer for each Step-Stick. I found I used too large voltage to the stepper motors before.

{{< postimg url = "stepstick-04.jpg" alt ="adjust the output voltage to the motor" imgw ="80%" >}}


