---
title: "Note: Moving Part"
foldername: "final-project"
image: "movepart_heroImg.png"
date: 2022-02-06
author: "Yuhan Tseng"
tags: ["Idea"]
categories: ["NOTE"]
draft: false
---

I plan to divide my final project into 3 parts to explore:

1. **Moving Part**
2. **Glass Bottle Part (upper body)**
3. **Interaction Part (detecting and following people)**

And this page is the note of Part 1.

## Where to Start?

I searched for similar projects on the Internet and also read the references that I got from the course to see if there is something that can give me some idea to start. I found there are 3 types of moving structure I am interested in and want to try:

## Type 1: Articulated robot

### 👀 What I found

- This type of robot has servo motor on its each joint, so the movement is created by controlling the angle of each servo motor.
- I think this type of movement is most creature-like and also most smoothly when tuning around.
- It can have different amounts of legs, but need to be even number (e.g. 4 legs, 6 legs, 8 legs). The more legs can make smoother movement.
- I'll start from one leg 3D (the video below👇) & 2D model (the laser cut version).

{{< youtube KnhH0ZwWoAQ "16:9" "60">}}

- And then try four leges with 12 servos.

##### References

- [Previous FabLab project](https://fabacademy.org/2021/labs/puebla/students/cuautli-garcia/finalproject.html#1)
- [Laser cut version](https://github.com/ArminJo/ServoEasing/tree/master/examples/QuadrupedControl)
- Other:

{{< youtube LB8VLnVpoPY "16:9" "60">}}
{{< youtube fvUhFBq7Z4g "4:3" "60" "👉 Project Detail 👈" "https://www.hackster.io/diyguyChris/arduino-spider-robot-quadruped-57b832">}}

## Type 2: Linkage robot

### 👀 What I found

- This type of robot applies linkage structure to move so it only needs **two 360-degree motors** (mostly are DC motors) to drive the robot.
- Changing the motor's rotating direction can make one side move forward or backward so using two sides of this structure can control robot's moving direction.
- There are different types of linkage structure:

    ***Klann linkage***
  - [Explaination](https://en.wikipedia.org/wiki/Klann_linkage)
  - Example 1: [Terra Spider: Autonomous Remediation Robot](https://www.instructables.com/Terra-Spider-Autonomous-Remediation-Robot/)
  - Example 2
  {{< youtube X3MNu4tOIUc "16:9" "60">}}

    ***Other linkage structure***
    - Example 1: Using 2 DC motors (the movement looks more like [solenoid](https://www.tlxtech.com/articles/solenoid-101-what-is-a-solenoid)?)and metal wires to move. [(Project detail)](https://www.hackster.io/bribro12/8-legged-spider-robot-194260)
    {{< youtube Hj8gsWjy_DY "4:3" "70">}}
    - Example 2: Useing gears and the rotatable connections between sticks to move. [(Project detail)](https://www.hackster.io/tartrobotics/a-diy-hexapod-robot-with-arduino-lego-and-3d-printed-parts-78415b)
    {{< youtube nUlNH8Ms1ko "16:9" "70">}}

## Type 3: Strandbeest

### 👀 What I found

- Created by artist [Theo Jansen](https://en.wikipedia.org/wiki/Theo_Jansen#The_strandbeest). It's a project about creating an artificial creature, and the coolest part for me is that [it is evoluted](https://www.strandbeest.com/evolution) by time! I like the movement of this kind of robot because it is beautiful.
 {{< youtube LewVEF2B_pM "16:9" "70">}}

- This type of robot also applies linkage structure but specifically using [Jansen's linkage structure](https://en.wikipedia.org/wiki/Jansen%27s_linkage).

{{< postimg url = "Jansen-linkage-structure.gif" alt ="How Jansen's linkage structure works in one leg" imgw ="70%" >}}

- It can have different pairs of legs but I haven't figure out that how to control moving direction with so many legs.
- I plan to start with one leg structure to understand more about how it works, and then use two DC motors with two pairs of legs to try moving and turning around.

##### References

- Example 1: Four Legs Walking Machine Model and detailed movement.
{{< youtube C9UvdlX1bhs "16:9" "70">}}
{{< youtube _2T4c21i-o0 "16:9" "70">}}

- Example 2: More legs. How to control moving direction?
{{< youtube WKOfy_YfRr4 "16:9" "70">}}

## Conclusion

- Though Type 1's movement seems more agile but I preffer Type 3's structure and also it can be drived by less motors.
- I assume that Type 1 is the easiest to move successfully among these structures because each leg is controled by independent servos. I plan to start with this structure to test and also use it to practice my Computer-Aided Design assignment.
- Because Type 2 and Type 3 are using linkage structure, I think that may be more efficinet if I can do motion simulation before I actually tast it with pysical materials. I found there some tutorials of motion simulation by **Fusion 360** and **FreeCAD** so I will start to explore these tools first and then try others.
- Hope I can try through all 3 types of structure 🤞🤞🤞🤞🤞


{{< content >}}
