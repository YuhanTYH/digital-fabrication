---
title: "Final Project: Wave Machine"
foldername: "final-project"
image: "final_heroimg.JPG"
date: 2022-06-03
author: "Yuhan Tseng"
tags: ["Motor","Kinetic"]
categories: ["NOTE"]
draft: false
---
{{< postimg url = "final_poster.jpg" alt ="poster of final project" imgw ="100%" >}}

[**Here is my final project video**](https://yuhantyh.gitlab.io/digital-fabrication/final_video.mp4)

{{< youtube nBlCu2ZVkhw "16:9" "80">}}

---

I have some pages about my final project in the "Final Project" menu, but they are more like the note of my thought and progress so I tried to organize more complete documentation of my final project on this page.

## Ideas

I kept changing the idea about the final project when I learned more about what I am able to make and what might be my limit. 😂​

I am interested in the mechanism and kinetic structure so my fist complete idea about the final project was [a robot with some linkage stucture](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note2/). But in the middle of the development, I felt a little bored because there are many people doing the similar stuff and I didn't come up with some new thing to try and to challenge. 

Then I found some kinetic sculptures on the Internet. At the same time, I was taking Embodied Interaction course and Audio Visulization course. I got some inspiration from these places and decided to make [an object (or surface) that can move organically](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-idea3/), combining to the projected image on this object.

At the Digital Fabrication III, I finally got a clearer image and idea of the final project: [a kinetic sculpture that consists of multiple kinetic units.](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note4/#references)



## What Is It?

Basically this project is a sculpture that can create different moving patterns. It is a matrix that consists of 16 up-down units, so the resolution of this pattern is 4 x 4.

## Mechanism Part

In the beginning, I took [**James Leng's work : Point Cloud**](http://www.jamesleng.net/#/pointcloud/) as a reference and did some adjustments so I could make with the material that we have in the FabLab. The final version looked well but actually, the stick didn't move smoothly all the time.

###### The benefits of this design:
- It's quiet.
- I can make it by myself with the material and machine in the FabLab.

###### The drawbacks of this design:
- Its size is a little big.
- The movement is not stable enough.

{{< youtube --KJJcN9FK8 "16:9" "70">}}

I didn't come up with other design at that time so I took this design during the **Wildcard Week** and discussed with the workshop's master and Kris. The workshop's master mentioned [**another type of mechanism**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/wildcard/#what-i-made) that seems to simplfy the stucture also decrease the size. 

I felt there were some mathematical ways that can help me to know what is the better length of each part, but I didn't find the related information at the time so I started with a size that I felt good by myself and kept adjusting from it. After some testing, this structure worked nice! Although it makes some sounds because of the metal parts, I like this sound that shows the property of the machine 😆​


Before using EDM machine to cut the metal parts, I used 3D printing to test the design:
{{< youtube 5-uHqn1FPgM "16:9" "70">}}

💡​ The workshop's master helped me make a thread in the hole on the rounded plate so I can use washers and nuts to make this part act like using a bearing.

💡​ The workshop's master helped me make a thread in the hole on the top of the stick so I can use screws to attach different stuff to it.

💡​ The central hole of the rounded plate was a little smaller than the diameter of the motor's shaft. Then I used the vice bench to press the motor's shaft into the plate's hole. However, this way made the plate not completely horizontal.

💡​ I added some **WD40** to make the movement more smoothly.

#### Files of the final version

- [Metal stick, cut by EDM machine](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/0516_stick.step)
- [Metal rounded plate, cut by EDM machine](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/0516_spin_Plate.step)
- [Transparent acrylic front frame, cut by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/0520-1_acrylic_3mm.ai)
- [Transparent acrylic motor base, cut by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/0520-2_acrylic_3mm.ai)
- [The base of the structure, made by 3D printing](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/topBase.stl)
- [The support sticks of the structure, made by 3D printing](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/supportStick.stl)


## Stepper Motor Controlling

To control the stepper motor, I use [**Slience-Step-Stick motor driver (TMC2208)**](https://learn.watterott.com/silentstepstick/). And the motor I used [**this one**](https://www.digikey.fi/en/products/detail/sparkfun-electronics/ROB-10551/5766908).

In the beginning, I wanted to control 4 stepper motors with one microcontroller. I tried to design a board with **ATtiny1614** but [got a short circuit](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-note4/#next-step) in the end and it was hard to debug.

{{< postimg url = "multi-motor_board-02.jpg" alt ="A board uses ATtiny1614 to control 4 stepper motors => Failed" imgw ="100%" >}}

Kris recommended me that I can make each stepper motor board with one microcontroller, and communicate all the boards together to control multiple motors at the same time. There are some benefits of this way:

- It is easier to debug because the range of examining is smaller than a big board.
- I can control more stepper motors without considering the amount of the microcontroller's I/O pins.
- There is a shortage of ATtiny1614 recently and we have much more ATtiny412 in the FabLab.

For the fisrt version of the little stepper motor board, I **connected TX/RX, GND, 12V, and 5V one board by one board**, which made the circuits unstable and I burned some boards in the end 😂​ 😂​ The reason might be there's some interrupt and other resistances were created when the electricity go through one board to another.

{{< postimg url = "motorBoard_v1-03.jpg" alt ="First version of the small stepper motor board => Failed" imgw ="100%" >}}

After this try, Kris told me that it's better to separate the power from each board, which means that each board has a independent power. So I redesigned the circuit: 

In this version, each board only connect **TX/RX and GND pin together**.

{{< postimg url = "motorBoard_v2-1.png" alt ="Second version of the small stepper motor board => Successed" imgw ="80%" >}}

During making this version, I found that [**there were two different regulators**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-progress/#heating-problem-of-motor-boards-and-the-motor) in the FabLab, and [**LM2940**](https://media.digikey.com/pdf/Data%20Sheets/Texas%20Instruments%20PDFs/LM2940(-N,C)_Sept-23-2013.pdf) is more sufficient. So most of the stepper boards of this project were made with LM2940.

One little problem I met was that we didn't have the recommended capacitors of LM2940 in the FanLab, which was 0.47 uF. However, we had 0.22 uF and 10000 pF (=0.01 uF), so I used **parallel circuit** to add up the value and got 0.47 uF in the end. Also, I found someone **solder capacitors as a stack** which can save the space of the PCB so I used the same way on my board.

{{< postimg url = "motorBoard_v2-04.jpg" alt ="Used multiple capacitors with parallel circuit to get the required value." imgw ="100%" >}}

💡 Actually I don't need **Enable Pin** of the Step-Stick, so I can connect this pin **to the GND directly on the board** and get one more pin to use for other purpose.

💡 During making these PCBs, I found that **the part between rivets and the copper wires** were very easy to disconnected. My solution were:
    
- Decrease the amount of the rivets.
- Add some extra solder to connect the rivets and the copper wires before soldering other components.

💡 Actually the capacitors of the regulator don't need to be the exact value as the datasheet, usually bigger than the recommanded value is ok.


##### The current of the motor

Another important thing needs to be careful is [**the current setting on Step-Stick**](https://yuhantyh.gitlab.io/digital-fabrication/final-project/final-project-progress/#heating-problem-of-motor-boards-and-the-motor).

The full current of the motor I used is 400mA (= 0.4A), and I followed [**this artical**](https://duet3d.dozuki.com/Wiki/Choosing_and_connecting_stepper_motors) that recommanded to set the current to around 85% of the rated (full) current of the stepper motor. So after claculating by the website's tool, I set around **0.52 V** on the Step-Stick.


#### Files of the final version

- [The KiCad project of the stepper motor board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/stepperMotor_412_reg_LM2940)

## Conmunication

I learned how to use [**Serial Communication with TX/RX pins**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/network/#2-cat-paw-board--2-stepper-motors) during **Networking and Communications week**, and designed a way to control a motor's speed and direction with 1 byte command. Also, I leared a clever way of using IDC connector which put the IDC in the middle of the ribbon so I could decrease the amount of the pin headers on the board.

{{< postimg url = "multi-board-connect.jpg" alt ="Use IDC and ribbon to connect TX/RX and GND pins of each boards" imgw ="80%" >}}

Because the main command will send from TouchDesigner, so I made a FTDI board to connect these stepper motor boards and the computer with USB cable.

{{< postimg url = "FTDI.jpg" alt ="Use a FTDI board as a bridge of these boards and the computer" imgw ="70%" >}}

The code for the stepper motor board:

( After testing and making sure I coyld get the right valure, I commanded all the print function to speed up the program. )

```c++
#define DIR_PIN          2
#define STEP_PIN         4
#define ENABLE_PIN       3
#define ID 0 //each board has a unique ID (0~15)

byte m_speed, m_dir;//save incoming command
byte id = 17;//set the default ID is not one of the board
byte m_speed2 = 0;
byte m_dir2 = 0; //save for this motor
int test;
boolean Start = false;

void setup() {
  Serial.begin(115200);
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available() > 0) {
    test = Serial.read();
    //print out the input command
    //Serial.print("Command:");
    //Serial.print(test);

    //filter the ID number: 0 ~ 15
    id = test << 4;
    id = id >> 4;
    //Serial.print(",id:");
    //Serial.print(id);

    //filter the direction: 1 or 0
    m_dir = (test & B00010000) >> 4;
    //Serial.print(",dir:");
    //Serial.print(m_dir, BIN);

    //filter the rotation speed: 0 ~ 7
    m_speed = test >> 5;
    //Serial.print(",speed:");
    //Serial.println(m_speed, DEC);
  }

  //if the command ID is same as this motor ID
  if ((int)id == ID) {
    //Serial.println("YES");

    //save the setting of speed and direction
    m_speed2 = m_speed;
    m_dir2 = m_dir;

    if (m_dir2) {
      digitalWrite(DIR_PIN, HIGH);
    }
    else {
      digitalWrite(DIR_PIN, LOW);
    }
  }

  //set the speed according to the command  
  switch ((int)m_speed2) {
    case 0:
      stop_rotate();
      break;
    case 1:
      rotation(8000);
      break;
    case 2:
      rotation(3500);
      break;
    case 3:
      rotation(3000);
      break;
    case 4:
      rotation(2500);
      break;
    case 5:
      rotation(2000);
      break;
    case 6:
      rotation(1500);
      break;
    case 7:
      rotation(1000);
      break;
  }
}

void rotation(int s) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(100);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(s);
}

void stop_rotate() {
  digitalWrite(STEP_PIN, LOW);
}
```

#### Files of this part

- [The KiCad project of FTDI board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/serial_D11C_usbA_2)


## Control Interface

I made an interface in TouchDesigner:

{{< postimg url = "interface.jpg" alt ="A user interface to set the movement" imgw ="70%" >}}

- Set the Starting Position:  I haven't figured out a way to set every unit to the same initial position automatically, so I make a button for each motor to rotate it manually. The motor will rotate when you press the button, and stop when you release the button.

- CHOOSE A MOTOR (0-15): You can set the speed and direction of a spicific motor. Input the motor's ID here.

- SET THE SPEED (0-7): Input the speed here. There are 1~7 different speeds from slow to fast. "0" means stop. If you press "Set all speed", the speed will be applied to all the motors.

- SET THE DIRECTION (0 or 1): Input the direction here. If you press "Set all dir", the direction will be applied to all the motors.

- SET THE STEPS: Input the integer number here. The steps can only be set to all the motors now. Press "Set all step" to set the steps.

- SET: Press this button to apply the setting to the motor you chose.

- START: Start to move all the units with the setting. Press again to stop.

[**Here is how to sent the commend from TouchDesigner to ATtiny412**](https://yuhantyh.gitlab.io/digital-fabrication/assignments/interface/#what-i-tried). And here is the main code to control a specific motor:

```python

motorID = 11; # each motor has a unique ID (0~15)

def onOffToOn(channel, sampleIndex, val, prev):
 return

# when the button is pressed
def whileOn(channel, sampleIndex, val, prev):
 
 op('table1')[0,1] = motorID
 op('table1')[1,2] = 'left'
 op('table1')[2,1] = 1
 
 sendCom()
 
 return

# when the button is released
def onOnToOff(channel, sampleIndex, val, prev):

 op('table1')[0,1] = motorID
 op('table1')[2,1] = 0
 
 sendCom()
 
 return

def whileOff(channel, sampleIndex, val, prev):
 
 return

def onValueChange(channel, sampleIndex, val, prev):
 
 return

# a function to convert the setting to a one-byte command
# and send to the ATtiny412 by serial port
def sendCom():
 id = op('table1')[0,1]
 dir = int(op('table1')[1,1])
 speed = int(op('table1')[2,1])

 dir2 = decimalToBinary(dir) + '0000'
 speed2 = decimalToBinary(speed) + '00000'
 id2 = decimalToBinary(id)
 #print(int(speed2))
 cmd = int(dir2) + int(id2) + int(speed2)
  
 cmd2 = str(cmd)
 #op('container5/text1').par.text = 'Command:'+cmd2
 cmd3 = binaryToDecimal(cmd2)
 op('serial1').sendBytes(cmd3)
 #print(cmd3)
  
def decimalToBinary(n):
    # converting decimal to binary
    # and removing the prefix(0b)
    return bin(n).replace("0b", "")
    
def binaryToDecimal(val): 
    return int(val, 2) 

```

I also made a table to save all the setting of each unit, so the system can send all the setting once the user press "START".

{{< postimg url = "setting_table.jpg" alt ="Each row is a setting for a motor (speed, direction, steps)" imgw ="70%" >}}

#### Files of this part

- [TouchDesigner project of the interface](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/interdace2.toe)


## Power Supply

About the power supply, I was confused at first because it was not the same as controlling one motor, I needed to control 16 motors at the same time. After asking Kris and Matti, I finally got a clearer concept about the power part:

- To drive a motor, the current is more important than the voltage. If I can provide a motor with enough current, the motor can still rotate with a voltage that is less than the rated voltage. The effect of less voltage is less torque.

- To provid enough current for 16 stepper motors, I need a power that can supply **16 x 0.34 = 5.44 A** current (I used 85% of the rated current). Also, from [**Step-Stick's website**](https://learn.watterott.com/silentstepstick/faq/#what-power-supply-do-i-need), they recommend use a power supply that **with a few times higher voltage than the motor's voltage**.  

- To find a suitable power supply, it is better that it can provide **an exact voltage and an equal or larger current** you need.

Accodinng to the conditions above and the power supply that I found in the FabLab, I decided to use **2 power supplies, each provides 20V voltage and maximan 4.5A current**. One power supply for 8 motors which need 8 x 0.34 = 2.72 A current.


{{< postimg url = "mainPower.jpg" alt ="The main power sources of the project. I changed the connect plug of it." imgw ="60%" >}}

To make the connection easier, I designed a power board that has 2 input of the power with high voltage and 4 outputs of the high voltage + 1 output of the 5V. So one power board can provide power to 4 motors and I made 4 power boards. Also, I changed the heads of the main power supplies to the power plugs that we have in the FabLab, so I can connect them to the power board easier.

{{< postimg url = "powerBoard-02.jpg" alt ="Two power boards connected together to use one power supply" imgw ="90%" >}}

{{< postimg url = "powerboards-03.jpg" alt ="4 power supply boards, 2 boards connected together." imgw ="100%" >}}



#### Files of this part

- [The KiCad project of Power board](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/powerSupply)



## Assemble & Appearence

For the assembling part, I hoped I can assemble it without any glue or tape. I designed each unit as a modular so I could make multiple same units easily.


##### A unit

{{< postimg url = "assemble-01.jpg" alt ="The main structure of a unit" imgw ="100%" >}}

(The files are [**here**])
- White part: 3mm transparent acrylic by laser cutting
- Red part: 2mm matel sheet by EDM machine
- Yellow part: 3D printing 
- a: M3 5mm screw
- b: M2 8mm screw
- c: M3 8mm screw
- d: M3 20mm screw
- M3 nut


##### The base of the machine

The stepper motor boards were attached on the base of the whole box.

{{< postimg url = "assemble-2-02.jpg" alt ="Assemble a unit and PCB to the base of the box" imgw ="100%" >}}

- Green plate is the stepper motor board.
- Red screws: M2.5 6 mm nylon screw
- Red nuts: M2.5 nylon nut
- Grey screws: M3 12mm screw

💡 To avoid the metal screws causing the short circuit on the PCBs, I usually use nylon screws to fix the PCBs part.

##### Appearance Design

About the whole style of the machine, I hope to show some of the mechanical parts but also don't want the whole machine full of the artificial style. So I made the main body of the box with plywood. The top cover was made of transparent black acrylic and made a little window on the front side, so people can also watch how the mechanism part works.

{{< postimg url = "assemble-3-04.jpg" alt ="The whole appearance of the machine" imgw ="100%" >}}

#### Files of the box

- [4mm plywood front side, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/front_4mm.ai)
- [4mm plywood left side, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/left_4mm.ai)
- [4mm plywood right side, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/rightside_4mm.ai)
- [4mm plywood back side, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/back_4mm.ai)
- [4mm MDF base, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/boxBase_4mm_MDF.ai)
- [2mm black transparent acrylic, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/frontWindow_2mm.ai)
- [3mm acrylic glossy black, made by laser cutting](https://gitlab.com/YuhanTYH/digital-fabrication/-/tree/main/static/files/final-project/top.ai)

##### Different Attachments

I tried to attach different things on top of each unit and made different visual effect.

**3D printing gestures**
{{< postimg url = "attachment-05.jpg" alt ="Use screws to attach different gestures models to each unit." imgw ="100%" >}}

**Laser cutting mirros**
{{< postimg url = "attachment-06.jpg" alt ="Multiple little mirrors create some chaotic effect." imgw ="100%" >}}

{{< video src = "mirrors.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}


**Flexible fabric**

I still wanted to create some organic movement so I tried to find a stretchable fabric. I designed a type of connector to connect the unit and the fabric with the screws. The result was not really good because the fabric was not flexible enough and limited the machine's movement. I also tried some projected images and videos on the fabric and the outcome was not bad 😆​

{{< postimg url = "attachment-07.jpg" alt ="Use 3D printied connectors to attach the fabric to each unit." imgw ="100%" >}}

{{< video src = "fabric_projection.mp4" type = "video/webm" preload = "auto" videow = "70%" >}}

## Component List

All the components that were used in this project are organized in [**this list**](https://docs.google.com/spreadsheets/d/1he1XG7m5g6ZXyFmROtBFitZXZmy-xyhiORy6xx5Xq28/edit?usp=sharing).You can also find the current price of these components.



{{< content >}}


